import React from 'react';
import {StyleSheet, Text, View} from "react-native";
import ColorDefinitions from "../../constants/ColorDefinitions";
import {Avatar, Divider} from "react-native-elements";
import {Ionicons} from "@expo/vector-icons";
import Ripple from "react-native-material-ripple";

export default (props) => (
    <View style={{marginBottom: 5, elevation: 5}}>
        <View style={{backgroundColor: ColorDefinitions.darkBackgroundColor.shade4, paddingHorizontal:10, paddingVertical: 10, flexDirection: 'row'}}>
            <Avatar
                rounded
                size='medium'
                overlayContainerStyle={{backgroundColor: ColorDefinitions.darkBackgroundColor.shade0}}
                titleStyle={{fontFamily: 'alpha-regular', color: ColorDefinitions.lightText.shade3}}
                title={props.user ? `${props.user.name.first.charAt(0)}${props.user.name.last.charAt(0)}` : 'G'}
                containerStyle={{marginVertical: 5, backgroundColor: 'transparent'}}/>
            <View style={{paddingHorizontal: 10}}>
                <Text style={{textTransform: 'uppercase', fontFamily: 'alpha-regular', marginVertical:5, color: ColorDefinitions.lightText.shade0, fontSize: 17}}>
                    {props.user ? `${props.user.name.title}. ${props.user.name.first} ${props.user.name.middle}  ${props.user.name.last}` : null}
                </Text>
                <Text style={{textTransform: 'uppercase', fontFamily: 'numeric-regular', marginVertical:2, color: ColorDefinitions.lightText.shade0, fontSize: 17}}>
                    { props.user ? props.user.login.membership_number : null}
                </Text>
            </View>
        </View>

        <Divider style={{padding:.1, backgroundColor: ColorDefinitions.lightBackgroundColor.shade0}}/>

        <Ripple rippleOpacity={.8} onPress={()=>props.navigation.navigate('Notifications')}>
            <View
                style={{paddingVertical: 10, paddingHorizontal: 10, flexDirection: 'row',justifyContent: 'space-between',backgroundColor: ColorDefinitions.darkBackgroundColor.shade4}}>
                <Ionicons name='ios-mail' size={24} color={ColorDefinitions.lightText.shade0}/>
                <Text style={{fontFamily: 'alpha-regular', paddingVertical: 4, paddingHorizontal: 10,color: ColorDefinitions.lightText.shade0}}>0 notifications, 0 unread</Text>
                <Ionicons name='ios-arrow-forward' size={24} color={ColorDefinitions.lightText.shade0}/>
            </View>
        </Ripple>

    </View>
);

const styles = StyleSheet.create({

});
