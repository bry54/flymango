import React from 'react';
import {StyleSheet} from "react-native";
import TypoDefinitions from "../../constants/TypoDefinitions";
import ColorDefinitions from "../../constants/ColorDefinitions";
import {Button} from "react-native-elements";

export default (props) => (
    <Button
        onPress={() => props.buttonAction()}
        containerStyle={{margin: 0, marginVertical: 20}}
        buttonStyle={{backgroundColor: ColorDefinitions.accentColor.shade0, borderRadius: 0}}
        titleStyle={{textTransform: 'uppercase', fontFamily: 'alpha-regular', marginVertical:5, color: ColorDefinitions.darkText.shade4}}
        title={props.buttonTitle}
        raised/>
);

const styles = StyleSheet.create({
    labelDefinition: {
        color: ColorDefinitions.lightText.shade0,
        fontSize: TypoDefinitions.smallFont,
        fontWeight: TypoDefinitions.normalText,
        textTransform: 'capitalize',
        fontFamily: 'alpha-regular'
    },
});
