import React from 'react';
import {FlatList, Modal, Text, View} from "react-native";
import {Ionicons, MaterialCommunityIcons} from '@expo/vector-icons';
import {Button, Input, ListItem} from "react-native-elements";
import TypoDefinitions from "../../constants/TypoDefinitions";
import ColorDefinitions from "../../constants/ColorDefinitions";
import Ripple from "react-native-material-ripple";

export default (props) => (
    <Modal
        animationType="slide"
        transparent={false}
        visible={props.modalVisible}
        onRequestClose={() => {props.updateItemVisibility( )}}>
        <View style={{flex: 1}}>
            <View style={{flex: 1}}>
                <View style={{flexDirection: 'row', justifyContent: 'space-between', backgroundColor: ColorDefinitions.colorPrimary, padding:10, elevation: 5}}>
                    <Text style={{paddingVertical: 20, color: ColorDefinitions.darkText.shade4, fontFamily: 'alpha-regular', fontSize: 16}}>Select Your Airport</Text>
                    <Button
                        onPress={() => {props.updateItemVisibility()}}
                        buttonStyle={{borderColor: ColorDefinitions.accentColor.shade0}}
                        titleStyle={{textTransform: 'capitalize', color: ColorDefinitions.lightText.shade0, fontFamily: 'alpha-regular', marginVertical:5}}
                        title="Cancel"
                        type='clear'/>
                </View>
                <Input
                    rightIcon={
                        props.airportSearch ? (
                            <Ripple onPress={()=>props.updateInputItem('airportSearch', '')}>
                                <MaterialCommunityIcons name={'close'} size={25} color={ColorDefinitions.accentColor.shade0}/>
                            </Ripple>
                        ) : (
                            <MaterialCommunityIcons name={'airport'} size={25} color={ColorDefinitions.accentColor.shade0}/>
                        )
                    }
                    onChangeText={ (text)=>props.updateInputItem('airportSearch', text)}
                    value={props.airportSearch}
                    containerStyle={{ elevation: 2, marginBottom: 2 }}
                    inputContainerStyle={{borderColor: ColorDefinitions.accentColor.shade0, borderBottomWidth:2}}
                    labelStyle={{fontFamily: 'alpha-regular', fontWeight: TypoDefinitions.lightText, fontSize: TypoDefinitions.xSmallFont, color: ColorDefinitions.darkText.shade4}}
                    inputStyle={{fontFamily: 'alpha-regular'}}
                    placeholder='Type to filter airports' />

                <FlatList
                    keyExtractor={(item) => JSON.stringify(item.id)}
                    data={props.airports}
                    renderItem={(item) => renderItem(item, props)}
                    extraData={props}/>
            </View>
        </View>
    </Modal>
);

const renderItem = ({ item }, props) => {
    const airPortKey = props.airportMode;
    return (
        <Ripple rippleOpacity={.8} onPress={ async () => {
            await props.updateItemVisibility();
            props.updateInputItem(airPortKey, item);
        }}>
            <ListItem
                title={item.province.name+', '+item.city.name}
                titleStyle={{fontFamily: 'alpha-regular', color: ColorDefinitions.darkText.shade4, paddingVertical: 3}}
                subtitle={item.airport_code+' - '+item.name}
                subtitleStyle={{fontFamily: 'alpha-regular'}}
                rightIcon={()=>(<Ionicons name="ios-star-outline" size={26} color={ColorDefinitions.accentColor.shade0} />)}
                bottomDivider
            />
        </Ripple>
    )
};
