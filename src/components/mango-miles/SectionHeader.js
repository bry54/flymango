import React from 'react';
import {StyleSheet, Text, View} from "react-native";
import ColorDefinitions from "../../constants/ColorDefinitions";

export default (props) => (
    <View style={{backgroundColor: ColorDefinitions.darkBackgroundColor.shade4, paddingHorizontal:10, paddingVertical: 10}}>
        <Text style={{textTransform: 'capitalize', fontFamily: 'alpha-regular', marginVertical:5, color: ColorDefinitions.lightText.shade0, fontSize: 17}}>
            {props.sectionTitle}
        </Text>

        <Text style={{ fontFamily: 'alpha-regular', marginVertical:5, color: '#f5f5f5', lineHeight:20}}>
            {props.sectionDescription}
        </Text>
    </View>
);

const styles = StyleSheet.create({

});
