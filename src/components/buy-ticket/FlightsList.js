import React, {Component} from "react";
import {Alert, Text, TouchableOpacity, View} from "react-native";
import {Accordion} from "native-base";
import {MaterialCommunityIcons, MaterialIcons} from '@expo/vector-icons';
import {Avatar, CheckBox, ListItem} from "react-native-elements";
import moment from "moment";
import ColorDefinitions from "../../constants/ColorDefinitions";
import Ripple from "react-native-material-ripple";
import i18n from "../../localizations/i18n";

export default class FlightsList extends Component{
    render() {

        const dataArray = this.props.flights;
        if (!dataArray.length)
            return (
                <View style={{flex: 1, alignItems: 'center', paddingVertical: 50}}>
                    <Avatar
                        overlayContainerStyle={{backgroundColor: ColorDefinitions.darkBackgroundColor.shade4}}
                        containerStyle={{elevation: 5}}
                        size={"xlarge"}
                        icon={{name:'airplane-off', type:'material-community', color: ColorDefinitions.accentColor.shade0}}
                        rounded />
                    <Text style={{paddingVertical: 40, fontFamily: 'alpha-regular', fontSize: 18, textAlign: 'center'}}>
                        {i18n.t('string_no_flights_on_date')}
                    </Text>

                    <View style={{elevation: 2, marginVertical: 0, backgroundColor: ColorDefinitions.accentColor.shade0, borderRadius: 2}}>
                        <Ripple rippleOpacity={.8} onPress={() => this.props.navBack()}>
                            <View style={{flexDirection: 'row'}}>
                                <MaterialCommunityIcons name="arrow-left" size={18} color={ColorDefinitions.darkText.shade4} style={{paddingVertical: 9, paddingHorizontal: 5}}/>
                                <Text style={{
                                    textTransform: 'uppercase',
                                    color: ColorDefinitions.darkText.shade4,
                                    fontFamily: 'alpha-regular',
                                    fontSize: 14,
                                    padding: 10,
                                    textAlign: 'center'}}>
                                    {i18n.t('string_try_different_date')}
                                </Text>
                            </View>
                        </Ripple>
                    </View>
                </View>
            );
        return (
            <View style={{flex: 1}}>
                <Accordion
                    animation={true}
                    dataArray={dataArray}
                    renderHeader={(item, expanded)=> this._renderHeader(item, expanded,this.props)}
                    renderContent={(item) => this._renderContent(item, this.props)}
                />
            </View>
        )
    }

    _renderHeader(item, expanded, props) {
        return (
            <ListItem
                chevron={false /*expanded && props.userInputs.cabinClass ?
                    <TouchableOpacity
                        onPress={async () => {
                            await props.setFlight(item);
                            props.navToNextScreen()
                        }}>
                        <MaterialCommunityIcons name='chevron-right-box' size={30} color={ColorDefinitions.darkBackgroundColor.shade4}/>
                    </TouchableOpacity>: false*/}
                containerStyle={{paddingHorizontal: 5, backgroundColor: expanded ? ColorDefinitions.accentColor.shade0 : ColorDefinitions.lightBackgroundColor.shade0}}
                leftIcon={
                    <TouchableOpacity
                        activeOpacity={.9}
                        onPress={()=> Alert.alert('To show Flight information')}>
                        <MaterialCommunityIcons name="information" size={25} color={ColorDefinitions.darkText.shade4}/>
                    </TouchableOpacity>
                }
                rightIcon={
                    <View style={{alignItems: 'center'}}>
                        {expanded ?
                            <MaterialIcons name="expand-less" size={25} color={ColorDefinitions.darkText.shade4}/> :
                            <MaterialIcons name="expand-more" size={25} color={ColorDefinitions.darkText.shade4}/>}
                    </View>
                }
                bottomDivider={true}
                title={
                    <View style={{}}>
                        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                            <View style={{alignItems: 'center'}}>
                                <MaterialCommunityIcons name="airplane-takeoff" size={18} color={ColorDefinitions.darkText.shade4}/>
                                <Text style={{fontSize:12, color:ColorDefinitions.darkText.shade4, fontFamily: 'alpha-regular'}}>{item.flight.departure.airport.airport_code}</Text>
                                <Text style={{fontSize:12, fontFamily: 'alpha-regular'}}>
                                    {moment(item.flight.departure.scheduled_time).format('HH:mm') }
                                </Text>
                            </View>

                            <View style={{
                                backgroundColor: 'gray',
                                height: 1,
                                flex: 1,
                                alignSelf: 'center',
                                marginHorizontal: 5
                            }}/>

                            <Text style={{alignSelf: 'center', paddingHorizontal: 5, fontFamily: 'alpha-regular', fontSize:12}}>
                                {this._getFlightDuration(item.flight.departure.scheduled_time, item.flight.arrival.scheduled_time)}
                            </Text>

                            <View style={{
                                backgroundColor: 'gray',
                                height: 1,
                                flex: 1,
                                alignSelf: 'center',
                                marginHorizontal: 5
                            }}/>

                            <View style={{alignItems: 'center'}}>
                                <MaterialCommunityIcons name="airplane-landing" size={18} color={ColorDefinitions.darkText.shade4}/>
                                <Text style={{fontSize:12, color:ColorDefinitions.darkText.shade4, fontFamily: 'alpha-regular'}}>{item.flight.arrival.airport.airport_code}</Text>
                                <Text style={{fontSize:12, fontFamily: 'alpha-regular'}}>
                                    {moment(item.flight.arrival.scheduled_time).format('HH:mm')}
                                </Text>
                            </View>
                        </View>
                    </View>
                }
            />
        );
    }

    _getFlightDuration =(departure, arrival)=>{
        const arrTime = moment(arrival, 'YYYY-MM-DD HH:mm:ss');
        const departTime = moment(departure, 'YYYY-MM-DD HH:mm:ss');
        const diff = arrTime.diff(departTime);
        const duration = moment.duration(diff);

        const hrs = duration.hours();
        const mins = duration.minutes();

        return `${hrs}hrs ${mins}mins`
    };

    _renderContent(item, props) {
        return (
            <ListItem
                containerStyle={{padding: 0}}
                bottomDivider={true}
                title={
                    <View style={{flex: 3, flexDirection: 'row', justifyContent: 'space-between'}}>
                        {
                            item.equipment.onboarding_equipment.compartments.map( c => {
                                return (
                                    <View
                                        key={c.item.id}
                                        style={{
                                            flex: 1,
                                            justifyContent: 'center',
                                            backgroundColor: (()=>{
                                                    if (c.item.id===1) return '#425761';
                                                    else if (c.item.id===2) return '#27343B';
                                                    else return '#182A33'}
                                            )(),
                                            paddingVertical: 10}}>
                                        <Ripple rippleOpacity={.8} onPress={async () => {
                                            this.props.setCabin(c);
                                            await props.setFlight(item);
                                            props.navToNextScreen()
                                        }}>
                                            <Text style={{textAlign: 'center', fontFamily: 'alpha-regular', color: ColorDefinitions.lightText.shade0}}>{c.item.class_desc}</Text>
                                            <CheckBox
                                                containerStyle={{paddingVertical: 0}}
                                                center
                                                checkedIcon='dot-circle-o'
                                                uncheckedIcon='circle-o'
                                                checkedColor={ColorDefinitions.accentColor.shade0}
                                                checked={props.userInputs.cabinClass ? c.item.id === props.userInputs.cabinClass.id : false}
                                            />
                                            <Text style={{textAlign: 'center', fontFamily: 'alpha-regular', color: ColorDefinitions.lightText.shade0}}>
                                                { 'R '+(c.data.fares[0].amount).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')  }
                                            </Text>
                                        </Ripple>
                                    </View>
                                )
                            })
                        }
                    </View>
                }
            />
        );
    }
}
