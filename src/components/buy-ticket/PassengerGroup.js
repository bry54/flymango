import React from "react";
import {Text, View} from "react-native";
import {Accordion} from "native-base";
import {ListItem} from "react-native-elements";
import {MaterialCommunityIcons} from "@expo/vector-icons";
import ColorDefinitions from "../../constants/ColorDefinitions";

export default (props) => {
    const section =  props.section;
    return (
        <View style={{flex: 1}}>
            <Accordion
                animation={true}
                dataArray={section}
                renderHeader={(item, expanded)=> this._renderHeader(item, expanded,this.props)}
                renderContent={(item) => this._renderContent(item, this.props)}
            />
        </View>
    )
};

const _renderHeader = (item, expanded, props) => {
    return (
        <ListItem
            containerStyle={{paddingHorizontal: 5, backgroundColor: ColorDefinitions.darkBackgroundColor.shade4}}
            bottomDivider={true}
            title={
                <View style={{alignItems: 'center'}}>
                    <MaterialCommunityIcons name="airplane-landing" size={18} color={ColorDefinitions.lightText.shade0}/>
                    <Text style={{fontSize: 12, color: ColorDefinitions.lightText.shade0, fontFamily: 'alpha-regular'}}>SECTION TITLE</Text>
                </View>
            }
        />
    );
};

const _renderContent = (item, props) => {
    return (
        <View>
            <Text>Some Add Person Item Here</Text>
            <ListItem
                containerStyle={{paddingHorizontal: 5, backgroundColor: ColorDefinitions.darkBackgroundColor.shade4}}
                bottomDivider={true}
                title={
                    <View style={{alignItems: 'center'}}>
                        <MaterialCommunityIcons name="airplane-landing" size={18} color={ColorDefinitions.lightText.shade0}/>
                        <Text style={{fontSize: 12, color: ColorDefinitions.lightText.shade0, fontFamily: 'alpha-regular'}}>Passenger Name Here</Text>
                    </View>
                }
            />
        </View>
    );
}

