import React from "react";
import {StyleSheet, Text, View} from "react-native";
import moment from "moment";
import ColorDefinitions from "../../constants/ColorDefinitions";
import Ripple from "react-native-material-ripple";
import i18n from "../../localizations/i18n";

export default (props) => {
    return (
        <>
            <Ripple
                rippleOpacity={.8}
                style={styles.dateItem} onPress={() => {props.updateInternalState('departureDatePickerVisibility', true)}}>
                <Text style={styles.dateTitle}>{i18n.t('lbl_depart_date')}</Text>
                {props.departureDate ? (
                    <View style={styles.dateContainer} >
                        <Text style={styles.dd}>
                            {
                                moment(props.departureDate).format('DD')
                            }
                        </Text>
                        <View style={styles.monthYear}>
                            <Text style={styles.mm}>
                                {
                                    moment(props.departureDate).format('MMM')
                                }
                            </Text>
                            <Text style={styles.yyyy}>
                                {
                                    moment(props.departureDate).format('YYYY')
                                }
                            </Text>
                        </View>
                    </View>
                ) : (
                    <Text style={styles.dd}>
                        {i18n.t('lbl_select')}
                    </Text>
                )}
            </Ripple>

            {props.isRoundTrip && <View style={{backgroundColor: ColorDefinitions.lightBackgroundColor.shade0, width: .4, height: '100%'}}/> }

            {props.isRoundTrip && <Ripple
                rippleOpacity={.8}
                style={styles.dateItem} onPress={() => {props.updateInternalState('returnDatePickerVisibility', true)}}>
                <Text style={styles.dateTitle}>{i18n.t('lbl_return_date')}</Text>
                {props.returnDate ?(
                    <View style={styles.dateContainer} >
                        <Text style={styles.dd}>
                            {
                                moment(props.returnDate).format('DD')
                            }
                        </Text>
                        <View style={styles.monthYear}>
                            <Text style={styles.mm}>
                                {
                                    moment(props.returnDate).format('MMM')
                                }
                            </Text>
                            <Text style={styles.yyyy}>
                                {
                                    moment(props.returnDate).format('YYYY')
                                }
                            </Text>
                        </View>
                    </View>
                ) : (
                    <View style={styles.dateContainer} >
                        <Text style={styles.dd}>
                            {i18n.t('lbl_select')}
                        </Text>
                    </View>
                )
                }
            </Ripple> }
        </>
    )}

const styles = StyleSheet.create({
    container: {
        //flex: 1,
    },

    dateItem:{
        width: '45%',
        justifyContent: 'center',
        padding: 10,
    },

    dateTitle:{
        fontFamily: 'alpha-regular',
        textTransform: 'uppercase',
        fontSize: 12,
        color: ColorDefinitions.accentColor.shade0
    },

    dateContainer:{
        flexDirection: 'row',
        paddingVertical: 5
    },

    dd:{
        fontSize: 34,
        color: ColorDefinitions.lightText.shade0,
        fontFamily: 'alpha-regular',
        paddingRight: 6
    },

    monthYear:{
        flexDirection: 'column',
        marginTop: 4
    },

    mm:{
        color: ColorDefinitions.lightText.shade0,
        fontFamily: 'alpha-regular',
    },

    yyyy:{
        color: ColorDefinitions.lightText.shade0,
        fontFamily: 'alpha-regular',
    },
});
