import React from "react";
import {Button} from "react-native-elements";
import ColorDefinitions from "../../constants/ColorDefinitions";
import Ripple from "react-native-material-ripple";
import {Text, View} from "react-native";
import i18n from "../../localizations/i18n";

export default (props) => (
    <View style={{elevation: 0, marginVertical: 0, backgroundColor: ColorDefinitions.accentColor.shade0}}>
        <Ripple rippleOpacity={.8} onPress={() => {
            if(props.hasMissingInformation)
                props.compileErrors();
            else
                props.searchFlights();
        }}>
            <Text style={{textTransform: 'uppercase', color: ColorDefinitions.darkText.shade4,fontFamily: 'alpha-regular',fontSize: 16,paddingVertical: 16,textAlign: 'center'}}>
                {i18n.t('btn_search_flights')}
            </Text>
        </Ripple>
    </View>
)
