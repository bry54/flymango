import React from "react";
import {StyleSheet, Text, View} from "react-native";
import {Ionicons, MaterialCommunityIcons} from "@expo/vector-icons";
import ColorDefinitions from "../../constants/ColorDefinitions";
import Ripple from "react-native-material-ripple";
import i18n from "../../localizations/i18n";

export default (props) => {
    const ticketInputs = props.ticketInputs;
    return (
        <>
            <View style={styles.passengerItem}>
                <Text style={styles.passengersSectionTitle}>Passengers</Text>
                <View style={styles.passengerInfo}>
                    <Ionicons name="ios-people" size={36} color={ColorDefinitions.darkText.shade4} />
                    <Text style={styles.passengerCount}>
                        {props.totalPassengers}
                    </Text>
                </View>
            </View>

            <View style={styles.cabinItem}>
                <Ripple
                    rippleOpacity={.8}
                    onPress={ () => props.updateInputItem() }
                    style={styles.cabinInfo}>
                    <Text style={styles.passengersSectionTitle}>{i18n.t('lbl_cabin_class')}</Text>
                    <View style={{flexDirection: 'row'}}>
                        <MaterialCommunityIcons name="rotate-3d" size={36} color={ColorDefinitions.darkText.shade4} />
                        <Text style={styles.cabinInfo}>{ticketInputs.cabinClass ? ticketInputs.cabinClass.class_desc : i18n.t('lbl_select')}</Text>
                    </View>
                </Ripple>
            </View>
        </>
    )}

const styles = StyleSheet.create({
    container: {
        //flex: 1,
    },

    passengerItem:{
        width: '40%',
        justifyContent: 'center',
        padding: 10,
    },

    cabinItem:{
        width: '60%',
        justifyContent: 'center',
        padding: 10,
    },

    passengersSectionTitle:{
        fontFamily: 'alpha-regular',
        textTransform: 'uppercase',
        fontSize: 12,
        color: ColorDefinitions.darkText.shade4
    },

    passengerInfo:{
        flexDirection: 'row'
    },

    cabinInfo:{
        flexDirection: 'column',
        fontSize: 30,
        color: ColorDefinitions.darkText.shade4,
        fontFamily: 'alpha-regular',
        textTransform: 'uppercase'
    },

    passengerCount:{
        fontSize: 30,
        paddingHorizontal: 10,
        fontFamily: 'alpha-regular',
        color: ColorDefinitions.darkText.shade4
    }
});
