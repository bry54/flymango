import React from "react";
import {Text, View} from "react-native";
import ColorDefinitions from "../../constants/ColorDefinitions";
import {MaterialCommunityIcons} from "@expo/vector-icons";
import moment from "moment";
import {Divider} from "react-native-elements";

export default (props) => {
    const ticketInputs = props.ticketInputs;
    const ticketParams = props.ticketParams;
    const flightMode = props.flightMode;
    return(
        <View style={{elevation: 10}}>
            <View style={{backgroundColor: ColorDefinitions.darkBackgroundColor.shade4, padding: 10, flexDirection: 'column'}}>
                <View style={{marginVertical: 0}}>
                    <View style={{}}>
                        {flightMode === 'OUTBOUND' ?
                            (
                                <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                                    <Text style={{fontSize: 25, fontFamily: 'alpha-regular', alignSelf: 'center', color: ColorDefinitions.accentColor.shade0}}> OUTBOUND </Text>

                                    <View style={{flexDirection: 'column', marginVertical: 3}}>
                                        <Text style={{fontFamily: 'alpha-regular', color: ColorDefinitions.accentColor.shade0, paddingBottom: 5, fontSize: 15}}>
                                            Airline: {ticketParams.outBoundFlight.marketing_carrier.arline.name.en}
                                        </Text>
                                        <Text style={{fontFamily: 'alpha-regular', color: ColorDefinitions.accentColor.shade0, paddingBottom: 5, fontSize: 15}}>
                                            Flight: {ticketParams.outBoundFlight.marketing_carrier.flight_number}
                                        </Text>
                                        <Text style={{fontFamily: 'alpha-regular', color: ColorDefinitions.accentColor.shade0, paddingBottom: 5, fontSize: 15}}>
                                            Cabin Class: {ticketParams.outBoundCabin.item.class_desc}
                                        </Text>
                                    </View>
                                </View>
                            ) : (
                                <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                                    <View style={{flexDirection: 'column', marginVertical: 3}}>
                                        <Text style={{fontFamily: 'alpha-regular', color: ColorDefinitions.accentColor.shade0, paddingBottom: 5, fontSize: 15}}>
                                            Airline: {ticketParams.inBoundFlight.marketing_carrier.arline.name.en}
                                        </Text>
                                        <Text style={{fontFamily: 'alpha-regular', color: ColorDefinitions.accentColor.shade0, paddingBottom: 5, fontSize: 15}}>
                                            Flight: {ticketParams.inBoundFlight.marketing_carrier.flight_number}
                                        </Text>
                                        <Text style={{fontFamily: 'alpha-regular', color: ColorDefinitions.accentColor.shade0, paddingBottom: 5, fontSize: 15}}>
                                            Cabin Class: {ticketParams.inBoundCabin.item.class_desc}
                                        </Text>
                                    </View>
                                    <Text style={{fontSize: 25, fontFamily: 'alpha-regular',alignSelf: 'center', color: ColorDefinitions.accentColor.shade0}}> INBOUND </Text>
                                </View>
                            )
                        }
                    </View>

                    <Divider style={{marginVertical: 2}}/>

                    <View style={{flexDirection: 'row', marginVertical: 6, justifyContent: 'space-between'}}>
                        <View>
                            <Text style={{fontFamily: 'alpha-regular', color: ColorDefinitions.accentColor.shade0, paddingBottom: 5}}>From</Text>
                            <Text style={{fontFamily: 'alpha-regular', color: ColorDefinitions.lightText.shade0}}>
                                {(flightMode === 'OUTBOUND' ? ticketInputs.origin.name : ticketInputs.destination.name).replace('International', 'Intl')}
                            </Text>
                        </View>

                        {/*<MaterialCommunityIcons name={'arrow-right'} color={ColorDefinitions.accentColor.shade0} style={{paddingHorizontal: 5, paddingTop: 10}}/>*/}

                        <View>
                            <Text style={{fontFamily: 'alpha-regular', color: ColorDefinitions.accentColor.shade0, paddingBottom: 5}}>To</Text>
                            <Text style={{fontFamily: 'alpha-regular', color: ColorDefinitions.lightText.shade0}}>
                                {(flightMode === 'OUTBOUND' ? ticketInputs.destination.name : ticketInputs.origin.name).replace('International', 'Intl')}
                            </Text>
                        </View>
                    </View>

                    <Divider style={{marginVertical: 2}}/>

                    <View style={{flexDirection: 'row', marginVertical: 6, justifyContent: 'space-between'}}>
                        <View>
                            <Text style={{fontFamily: 'alpha-regular', color: ColorDefinitions.accentColor.shade0, paddingBottom: 5}}>Origin Departure Date</Text>
                            <Text style={{fontFamily: 'alpha-regular', color: ColorDefinitions.lightText.shade0}}>
                                {
                                    flightMode === 'OUTBOUND' ?
                                        moment(ticketParams.outBoundFlight.flight.departure.scheduled_time).format('LLL') :
                                        moment(ticketParams.inBoundFlight.flight.departure.scheduled_time).format('LLL')
                                }
                            </Text>
                        </View>
                        <View>
                            <Text style={{fontFamily: 'alpha-regular', color: ColorDefinitions.accentColor.shade0, paddingBottom: 5}}>Destination Arrival Date</Text>
                            <Text style={{fontFamily: 'alpha-regular', color: ColorDefinitions.lightText.shade0}}>
                                {
                                    flightMode === 'OUTBOUND' ?
                                        moment(ticketParams.outBoundFlight.flight.arrival.scheduled_time).format('LLL') :
                                        moment(ticketParams.inBoundFlight.flight.arrival.scheduled_time).format('LLL')
                                }
                            </Text>
                        </View>
                    </View>

                    <Divider style={{marginVertical: 2}}/>

                    <View style={{marginVertical: 3, flexDirection: 'row', justifyContent: 'space-evenly'}}>
                        {ticketParams.passengers.map(pass => {
                            if(pass.data.length)
                                return (
                                    <View style={{flexDirection: 'row', justifyContent: 'space-between'}} key={JSON.stringify(pass)}>
                                        <MaterialCommunityIcons name='chevron-double-left' size={20} color={ColorDefinitions.accentColor.shade0}/>
                                        <Text style={{fontFamily: 'alpha-regular', color: ColorDefinitions.lightText.shade0, textTransform: 'capitalize', marginTop: 2}}>
                                            {pass.data.length + ' ' + (
                                                pass.data.length > 1 ?  pass.passengerType.code : pass.passengerType.code.substr(0, pass.passengerType.code.length - 1 )
                                            )}
                                        </Text>
                                        <MaterialCommunityIcons name='chevron-double-right' size={20} color={ColorDefinitions.accentColor.shade0}/>
                                    </View>
                                )})}
                    </View>
                </View>
                <Divider/>
                <View style={{marginTop: 5,alignSelf: 'flex-end'}}>
                    <Text style={{fontFamily: 'alpha-regular', color: ColorDefinitions.accentColor.shade0, textTransform: 'uppercase', fontSize: 20, textDecorationLine: 'underline'}}>
                        {
                            (()=>{
                                let total = 0;
                                const outboundCabin = ticketParams.outBoundCabin;
                                const inboundCabin = ticketParams.inBoundCabin;
                                ticketParams.passengers.forEach( pass => {
                                    let flightFare;
                                    if (flightMode === "OUTBOUND")
                                        flightFare = outboundCabin.data.fares.find(fare => fare.passenger_type_id === pass.passengerType.id);
                                    else
                                        flightFare = inboundCabin.data.fares.find(fare => fare.passenger_type_id === pass.passengerType.id);

                                    const passTypeCost = parseFloat(flightFare.amount) * pass.data.length;
                                    total += passTypeCost
                                });

                                return 'R '+total.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
                            })()
                        }
                    </Text>
                </View>
            </View>
        </View>
    )
}
