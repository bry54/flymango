import React from "react";
import {StyleSheet, Text, View} from "react-native";
import {MaterialCommunityIcons} from "@expo/vector-icons";
import ColorDefinitions from "../../constants/ColorDefinitions";
import Ripple from "react-native-material-ripple";
import i18n from "../../localizations/i18n";

export default (props) => {
    const ticketInputs = props.ticketInputs;
    return (
    <>
        <Ripple
            rippleOpacity={.8}
            onPress={() => {props.navToAirportSection('origin')}}
            style={styles.locationItem}>
            <Text style={styles.locationTitle}>{i18n.t('lbl_origin')}</Text>
            <Text style={styles.locationCode}>
                {ticketInputs.origin ? ticketInputs.origin.airport_code : i18n.t('lbl_select')}
            </Text>
            <Text style={styles.locationName}>
                {ticketInputs.origin ? ticketInputs.origin.name : i18n.t('string_select_origin')}
            </Text>
        </Ripple>

        <View>
            <MaterialCommunityIcons name="airplane-takeoff" size={26} color={ColorDefinitions.lightText.shade0} />
            {props.isRoundTrip && <MaterialCommunityIcons name="airplane-landing" size={26} color={ColorDefinitions.lightText.shade0} style={{transform: [{ scaleX: -1 }]}}/>}
        </View>

        <Ripple
            rippleOpacity={.8}
            onPress={() => {props.navToAirportSection('destination')}}
            style={styles.locationItem}>
            <Text style={styles.locationTitle}>{i18n.t('lbl_destination')}</Text>
            <Text style={styles.locationCode}>
                {ticketInputs.destination ? ticketInputs.destination.airport_code : i18n.t('lbl_select')}
            </Text>
            <Text style={styles.locationName}>
                {ticketInputs.destination ? ticketInputs.destination.name : i18n.t('string_select_destination')}
            </Text>
        </Ripple>
    </>
)}

const styles = StyleSheet.create({
    container: {
        //flex: 1,
    },

    locationItem: {
        width: '45%',
        justifyContent: 'center',
        padding: 10,
    },

    locationTitle: {
        fontFamily: 'alpha-regular',
        textTransform: 'uppercase',
        fontSize: 12,
        color: ColorDefinitions.accentColor.shade0
    },

    locationCode: {
        fontFamily: 'alpha-regular',
        fontSize: 24,
        color: ColorDefinitions.lightText.shade0,
        paddingVertical: 5
    },

    locationName: {
        fontFamily: 'alpha-regular',
        fontSize: 12,
        color: ColorDefinitions.lightText.shade0
    }
});
