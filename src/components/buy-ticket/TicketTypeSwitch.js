import ColorDefinitions from "../../constants/ColorDefinitions";
import {Button} from "react-native-elements";
import React from "react";

export default (props) => (
    <Button
        onPress={()=>props.switchHandler()}
        containerStyle={{width: '50%'}}
        buttonStyle={{backgroundColor: ColorDefinitions.colorPrimary, borderRadius: 0, borderBottomColor: props.active ? ColorDefinitions.darkBackgroundColor.shade4 : ColorDefinitions.colorPrimary, borderBottomWidth:2}}
        titleStyle={{textTransform: 'uppercase',fontFamily: 'alpha-regular', marginVertical:5, color: ColorDefinitions.darkBackgroundColor.shade4}}
        title={props.title}
        clear/>
)
