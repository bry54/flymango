import React, {PureComponent} from 'react';
import {SafeAreaView} from "react-native";
import {DrawerContentScrollView} from '@react-navigation/drawer';
import DrawerHeader from "./DrawerHeader";
import DrawerLabel from "./DrawerLabel";
import {connect} from "react-redux";
import {updateGlobalControls} from "../../store/utilities/actionsCollection";
import i18n from "../../localizations/i18n";

class NavigationDrawerContent extends PureComponent{
    render () {
        const menuItems = [
            {
                title: null,
                children: [
                    {icon: 'view-dashboard-outline', label: i18n.t('menu_home'), route: 'Home', requiresLogin: false, count: 0},
                    {icon: 'traffic-light', label: i18n.t('menu_flight_status'), route: 'FlightStatus', requiresLogin: false, count: 0},
                    {icon: 'shield-airplane', label: i18n.t('menu_aircraft_fleet'), route: 'AircraftFleet', requiresLogin: false, count: 0},
                    {icon: 'lightbulb-on', label: i18n.t('menu_suggestions'), route: 'SuggestedDestinations', requiresLogin: false, count: 0},
                ],
            }, {
                title: i18n.t('menu_fly_with_mango'),
                children: [
                    {icon: 'map-marker-distance', label: i18n.t('menu_mango_miles'), route: 'MangoMiles', requiresLogin: true, count: 0},
                    {icon: 'wallet', label: i18n.t('menu_buy_ticket'), route: 'BuyTicket', requiresLogin: true, count: 0},
                    {icon: 'ticket-account', label: i18n.t('menu_my_flights'), route: 'MyFlights', requiresLogin: true, count: 0},
                    {icon: 'account-check', label: i18n.t('menu_mobile_checkin'), route: 'MobileCheckIn', requiresLogin: false, count: 0},
                ]
            }, {
                title: i18n.t('menu_additional_services'),
                children: [
                    {icon: 'hotel', label: i18n.t('menu_book_hotel'), route: 'BookHotel', requiresLogin: false, count: 0},
                    {icon: 'car-connected', label: i18n.t('menu_rent_car'), route: 'RentCar', requiresLogin: false, count: 0},
                    {icon: 'google-maps', label: i18n.t('menu_maps'), route: 'AirportMaps', requiresLogin: false, count: 0}
                ]
            }, {
                title: i18n.t('menu_other'),
                children: [
                    {icon: 'settings-outline', label: i18n.t('menu_settings'), route: 'Settings', requiresLogin: false, count: 0},
                    {icon: 'help-box', label: i18n.t('menu_help'), route: 'Help', requiresLogin: false, count: 0},
                ]
            },
        ];
        const user = this.props.user;
        const filteredItems = menuItems.map(menuItem=>{
            return {
                ...menuItem,
                children: menuItem.children.filter(child => !child.requiresLogin)
            }
        });
        const shownMenu = user ? menuItems : filteredItems;
        return(
            <SafeAreaView style={{flex: 1}} forceInset={{ top: 'always', horizontal: 'never' }}>
                <DrawerContentScrollView {...this.props} showsVerticalScrollIndicator={false}>
                    <DrawerHeader
                        initials={user ? user.name.first.charAt(0)+user.name.last.charAt(0) : 'G'}
                        fullName={user ? user.name.first + ' ' +user.name.last : 'Guest User'}
                        userID={user ? user.login.membership_number : null}
                        navHandler={user ? this.navToMyProfile : this.navToAuthOptions}/>

                    {
                        shownMenu.map((_item) => (
                                <DrawerLabel
                                    user={user}
                                    key={JSON.stringify(_item)}
                                    item={_item}
                                    navHandler={this.navToDrawerScreen}/>
                            )
                        )
                    }

                </DrawerContentScrollView>
            </SafeAreaView>
        )
    }

    navToMyProfile = () =>{
        const {navigation} = this.props;
        navigation.navigate('MyProfile')
    };

    navToAuthOptions = () =>{
        const {navigation} = this.props;
        navigation.navigate('Auth')
    };

    navToDrawerScreen = async (routeName) =>{
        const {navigation} = this.props;
        navigation.navigate(routeName);
    };
}

const mapStateToProps = (state) => {
    return {
        user: state.authReducer.loggedInUser
    }
};

const matchDispatchToProps = dispatch => {
    return {
        updateGlobalControl: (controlObj) => dispatch(updateGlobalControls(controlObj))
    }
};

export default connect(mapStateToProps, matchDispatchToProps)(NavigationDrawerContent);
