import React, {PureComponent} from 'react';
import {StyleSheet, Text, View} from "react-native";
import {Badge, Divider, ListItem} from "react-native-elements";
import TypoDefinitions from "../../constants/TypoDefinitions";
import DrawerItem from "@react-navigation/drawer/src/views/DrawerItem";
import ColorDefinitions from "../../constants/ColorDefinitions";
import {MaterialCommunityIcons} from "@expo/vector-icons";
import Ripple from "react-native-material-ripple";

export default class DrawerLabel extends PureComponent{
    render() {
        const item = this.props.item;
        return (
            <View>
                {
                    item.title && <ListItem
                        title={item.title}
                        titleStyle={{fontFamily: 'alpha-regular', fontSize: 16, textTransform: 'uppercase', color: ColorDefinitions.lightText.shade0}}
                        containerStyle={{backgroundColor: ColorDefinitions.darkBackgroundColor.shade4, paddingVertical: 15}}
                    />
                }
                {
                    item.children.map((child) => (
                        <Ripple
                            key={child.route}
                            rippleOpacity={.8}
                            onPress={() => this.props.navHandler(child.route)}>
                        <ListItem
                            bottomDivider={false}
                            containerStyle={{backgroundColor: ColorDefinitions.colorPrimary, padding: 0}}
                            title={
                                <View>
                                    <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                                        <DrawerItem
                                            icon={() => <MaterialCommunityIcons name={child.icon} size={24}
                                                                                color={ColorDefinitions.darkText.shade2}/>}
                                            style={{flex: 1}}
                                            label={() => (
                                                <Text style={styles.labelDefinition}>{child.label}</Text>
                                            )}
                                            activeBackgroundColor={ColorDefinitions.colorPrimaryDark}
                                            activeTintColor={ColorDefinitions.darkText.shade4}
                                        />

                                        {child.count ? <Badge
                                            containerStyle={{margin: 10, marginTop: 18}}
                                            value={child.count}
                                            badgeStyle={{
                                                backgroundColor: ColorDefinitions.darkBackgroundColor.shade0,
                                                borderColor: 'transparent',
                                                borderRadius: 3,
                                            }}/> : null}
                                    </View>
                                    <Divider style={{backgroundColor: ColorDefinitions.darkBackgroundColor.shade4}}/>
                                </View>
                            }
                        />
                        </Ripple>
                    ))
                }
            </View>
        );
    }
}

const styles = StyleSheet.create({
    labelDefinition: {
        color: ColorDefinitions.darkText.shade2,
        fontSize: TypoDefinitions.normalFont,
        fontWeight: TypoDefinitions.normalText,
        textTransform: 'capitalize',
        fontFamily: 'alpha-regular'
    },
});
