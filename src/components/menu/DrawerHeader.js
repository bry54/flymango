import {StyleSheet, Text, View} from "react-native";
import {Avatar} from "react-native-elements";
import React from "react";
import TypoDefinitions from "../../constants/TypoDefinitions";
import ColorDefinitions from "../../constants/ColorDefinitions";
import Ripple from "react-native-material-ripple";

export  default (props) => (
    <Ripple
        rippleOpacity={.8}
        style={{alignItems: 'center', justifyContent: 'center' }}
        onPress={()=>props.navHandler()}>

        <View style={styles.userInfoContainer}>
            <Avatar
                rounded
                title={props.initials}
                size='medium'
                containerStyle={{marginVertical: 7, backgroundColor: 'transparent'}}
                overlayContainerStyle={{backgroundColor: ColorDefinitions.darkBackgroundColor.shade0}}
                titleStyle={{fontFamily: 'alpha-regular', color: ColorDefinitions.lightText.shade3}} />
            <View style={{paddingHorizontal:20, paddingVertical: 4}}>
                <Text style={styles.userName}>{props.fullName}</Text>
                {
                    props.userID ? (
                        <Text style={styles.userID}>{props.userID}</Text>
                    ) : (
                        <Text style={[styles.authButton]}>
                            SignIn or Create account
                        </Text>
                    )
                }
            </View>
        </View>
    </Ripple>
);

const styles = StyleSheet.create({
    userInfoContainer: {
        marginTop: -4,
        marginBottom: 0,
        paddingVertical:20,
        padding: 20,
        width: '100%',
        flexDirection: 'row',
        backgroundColor: ColorDefinitions.darkBackgroundColor.shade4
    },

    userName: {
        color: ColorDefinitions.lightText.shade0,
        fontSize: TypoDefinitions.smallFont,
        fontWeight: TypoDefinitions.normalText,
        textTransform: 'capitalize',
        fontFamily: 'alpha-regular',
        paddingVertical: 5
    },

    userID: {
        color: ColorDefinitions.lightText.shade0,
        fontFamily: 'numeric-regular',
        fontSize: TypoDefinitions.normalFont,
        paddingVertical: 0
    },

    authButton: {
        color: ColorDefinitions.lightText.shade0,
        fontFamily: 'alpha-regular',
        paddingVertical: 0,
        fontSize: TypoDefinitions.xxSmallFont
    },
});
