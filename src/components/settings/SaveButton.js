import React from 'react';
import {StyleSheet, Text, View} from "react-native";
import TypoDefinitions from "../../constants/TypoDefinitions";
import ColorDefinitions from "../../constants/ColorDefinitions";
import Ripple from "react-native-material-ripple";

export default (props) => (
    <View style={{elevation: 0, marginVertical: 0, backgroundColor: ColorDefinitions.accentColor.shade0}}>
        <Ripple rippleOpacity={.8} onPress={() => props.buttonAction()}>
            <Text style={{textTransform: 'uppercase', color: ColorDefinitions.darkText.shade4,fontFamily: 'alpha-regular',fontSize: 16,paddingVertical: 16,textAlign: 'center'}}>
                {props.buttonTitle}
            </Text>
        </Ripple>
    </View>
);

const styles = StyleSheet.create({
    labelDefinition: {
        color: ColorDefinitions.lightText.shade0,
        fontSize: TypoDefinitions.smallFont,
        fontWeight: TypoDefinitions.normalText,
        textTransform: 'capitalize',
        fontFamily: 'alpha-regular'
    },
});
