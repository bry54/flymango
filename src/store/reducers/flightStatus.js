import {GET_FLIGHT_STATUS} from "../utilities/actionTypes";
import airports from "../../utilities/api/airports";

const initialState = {
    airports: airports,
    flights: []
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_FLIGHT_STATUS: {
            const flights = action.payload;

            return {
                ...state,
                flights: flights
            };
        }

        default:
            return state;
    }
};

export {initialState, reducer};
