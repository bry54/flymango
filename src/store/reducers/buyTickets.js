import {
    BUY_TICKETS_APPEND_PERSON,
    BUY_TICKETS_FLATTEN_TICKETS,
    BUY_TICKETS_REMOVE_PERSON,
    BUY_TICKETS_SET_FLIGHT_SEARCH_RESULTS,
    BUY_TICKETS_UPDATE_TICKET_PARAMS,
    SAVE_TICKET,
    UPDATE_TICKET_INPUTS
} from "../utilities/actionTypes";
import airports from "../../utilities/api/airports";
import compartments from "../../utilities/api/compartments";
import passengerTypes from "../../utilities/api/passenger_codes";
import {defaultTickets} from '../../utilities/MockUpData'
import moment from "moment";

const initialState = {
    passengerTypes: passengerTypes,
    airports: airports,
    cabinClasses: compartments,

    inputs: {
        airportSearch: '',
        isRoundTrip: false,
        departureDate: null,
        returnDate: null,
        origin: null,
        destination: null,
        adults: 0,
        children: 0,
        infants: 0,
        cabinClass: null,
        cabinToggleCount: 0,
    },
    foundFlights: {},
    ticketParams:{
        outBoundFlight: null,
        outBoundCabin: null,
        inBoundFlight: null,
        inBoundCabin: null,
        passengers: []
    },
    savedTickets: defaultTickets,
    flattenedTickets: {
        futureFlights: [],
        historicalFlights: []
    }
};

const reducer = (state = initialState, action) => {
    switch (action.type) {

        case UPDATE_TICKET_INPUTS: {
            const inputObject = action.payload;
            const key = inputObject.key;
            let value = inputObject.value;
            let cabinObj = null;
            let toggleCount = state.inputs.cabinToggleCount;
            let ticketParamsClone = JSON.parse(JSON.stringify(state.ticketParams));

            if(key === 'cabinClass' && value===null){
                toggleCount++;
                if (toggleCount > 3) toggleCount = 1;

                cabinObj = compartments.find(compartment => compartment.id === toggleCount );
                value = cabinObj;
            }

            if (key === 'adults' || key === 'children' || key === 'infants') {
                const found = ticketParamsClone.passengers.find(item => item.passengerType.code === key);
                if (found) {
                    found.maxPeople = value;
                    const foundIndex = ticketParamsClone.passengers.findIndex(item => item.passengerType.id === found.passengerType.id);
                    ticketParamsClone.passengers[foundIndex] = found;
                } else {
                    const passObj = {
                        passengerType: passengerTypes.find(pt => pt.code === key),
                        maxPeople: value,
                        data: [],
                    };
                    ticketParamsClone.passengers.push(passObj);
                }
            }

            return {
                ...state,
                inputs: {
                    ...state.inputs,
                    [key]: value,
                    cabinToggleCount: toggleCount,
                },
                ticketParams: ticketParamsClone
            };
        }

        case BUY_TICKETS_UPDATE_TICKET_PARAMS: {
            const paramObject = action.payload;
            const key = paramObject.key;
            const value = paramObject.value;

            return {
                ...state,
                ticketParams: {
                    ...state.ticketParams,
                    [key]: value,
                }
            };
        }

        case SAVE_TICKET:{
            let ticketsClone = state.savedTickets.map(t => t);

            const ticket = action.payload;
            const found = ticketsClone.find(t => JSON.stringify(ticket) === JSON.stringify(t));
            if(!found){
                ticketsClone.push(ticket)
            }

            return {
                ...state,
                savedTickets: ticketsClone,
                ticketParams:{
                    outBoundFlight: null,
                    outBoundCabin: null,
                    inBoundFlight: null,
                    inBoundCabin: null,
                    passengers: []
                },
                inputs: {
                    ...state.inputs,
                    airportSearch: '',
                    adults: 0,
                    children: 0,
                    infants: 0,
                    cabinClass: null,
                    cabinToggleCount: 0,
                },
            }
        }

        case BUY_TICKETS_SET_FLIGHT_SEARCH_RESULTS:{
            const flights = action.payload;
            return {
                ...state,
                foundFlights: flights
            }
        }

        case BUY_TICKETS_APPEND_PERSON:{
            const tempPerson = action.payload;
            const passengerCode = tempPerson.passengerCode;
            const passengersClone = state.ticketParams.passengers;

            const passengerCodeIndex = passengersClone.findIndex(pc => pc.passengerType.code == passengerCode);

            passengersClone[passengerCodeIndex].data.push(tempPerson);

            return {
                ...state,
                ticketParams: {
                    ...state.ticketParams,
                    passengers: passengersClone,
                }
            }
        }

        case BUY_TICKETS_REMOVE_PERSON:{
            const tempPerson = action.payload;
            const passengerCode = tempPerson.passengerCode;
            const passengersClone = JSON.parse(JSON.stringify(state.ticketParams.passengers));

            const passengerCodeIndex = passengersClone.findIndex(pc => pc.passengerType.code == passengerCode);

            const index = passengersClone[passengerCodeIndex].data.findIndex(p=> p.firstName === tempPerson.firstName);

            console.log(tempPerson, index)

            if (index !== -1)
                passengersClone[passengerCodeIndex].data.splice(index, 1);

            return {
                ...state,
                ticketParams: {
                    ...state.ticketParams,
                    passengers: passengersClone,
                }
            }
        }

        case BUY_TICKETS_FLATTEN_TICKETS: {
            let flatTickets = {
                futureFlights: [],
                historicalFlights: []
            };

            flattenFlights(state.savedTickets).forEach(f => {
                const departDate = moment(f.flight.departure.scheduled_time);
                if(departDate.isAfter(moment())){
                    flatTickets.futureFlights.push(f)
                }
                if(departDate.isBefore(moment())){
                    flatTickets.historicalFlights.push(f)
                }
            });

            return {
                ...state,
                flattenedTickets: flatTickets
            };
        }

        default:
            return state;
    }
};

const flattenFlights = (myFlights) =>{
    let flattenedFlights = [];
    myFlights.forEach(flight => {
        if (flight.outBoundFlight) {
            const f = flight.outBoundFlight;
            flattenedFlights.push(f)
        }
        if (flight.inBoundFlight) {
            const f = flight.inBoundFlight;
            flattenedFlights.push(f)
        }
    });

    return flattenedFlights.sort((a, b) => new moment(a.flight.departure.scheduled_time) - new moment(b.flight.departure.scheduled_time));
};

export {initialState, reducer};
