import thunk from 'redux-thunk';
import {AsyncStorage} from "react-native";
import {createMigrate, persistReducer, persistStore} from 'redux-persist'
import {applyMiddleware, combineReducers, compose, createStore} from "redux";

import {reducer as authReducer} from '../../store/reducers/auth'
import {initialState as initialGlobalReducer, reducer as globalReducer} from '../../store/reducers/global'
import {initialState as initialBuyTicketsReducer, reducer as buyTicketsReducer} from '../../store/reducers/buyTickets'
import {
    initialState as initialFlightStatusReducer,
    reducer as flightStatusReducer
} from '../../store/reducers/flightStatus'

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const migrations = {
    // Migration Version in production: 8, increase by 1 when publishing a modified store!!!
    8: (state) => {
        return {
            // reinitialize modified reducers.
            ...state,
            //authReducer: initialAuthReducer,
            globalReducer: initialGlobalReducer,
            buyTicketsReducer: initialBuyTicketsReducer,
            flightStatusReducer: initialFlightStatusReducer
        }
    },
};

const persistConfig = {
    key: 'root',
    storage: AsyncStorage,
    timeout: null,
    version: 8,
    debug: false,
    migrate: createMigrate(migrations, { debug: false }),
};

const rootReducer = combineReducers({
    globalReducer: globalReducer,
    flightStatusReducer: flightStatusReducer,
    authReducer: authReducer,
    buyTicketsReducer: buyTicketsReducer,
});

const persistedReducer = persistReducer(persistConfig, rootReducer);

const store = createStore(persistedReducer, composeEnhancers(applyMiddleware(thunk)));
const  persistor = persistStore(store);

export { store, persistor };
