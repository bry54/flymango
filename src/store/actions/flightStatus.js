import {GET_FLIGHT_STATUS,} from '../utilities/actionTypes';
import {getFlightStatusesByFlightNumberApi, getFlightStatusesByRouteApi} from "../../utilities/api";

export const getFlightStatusByRoute = (params) => async dispatch => {
    let flights = getFlightStatusesByRouteApi(params.origin, params.destination, params.departureDate);
    dispatch({
        type: GET_FLIGHT_STATUS,
        payload: flights
    });
};

export const getFlightStatusByFlightNumber = (params) => async dispatch => {
    let flights = getFlightStatusesByFlightNumberApi(params.flightNumber, params.departureDate);
    dispatch({
        type: GET_FLIGHT_STATUS,
        payload: flights
    });
};
