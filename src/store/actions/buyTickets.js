import {
    BUY_TICKETS_APPEND_PERSON,
    BUY_TICKETS_FLATTEN_TICKETS,
    BUY_TICKETS_REMOVE_PERSON,
    BUY_TICKETS_SET_FLIGHT_SEARCH_RESULTS,
    BUY_TICKETS_UPDATE_TICKET_PARAMS,
    BUY_TICKETS_UPDATE_VISIBLE_ITEMS,
    SAVE_TICKET,
    UPDATE_TICKET_INPUTS
} from '../utilities/actionTypes';
import {searchFlightsApi} from "../../utilities/api";

export const buyTicketsUpdateVisibleItems = (item) => async dispatch => {
    dispatch({
        type: BUY_TICKETS_UPDATE_VISIBLE_ITEMS,
        payload: item
    });
};

export const updateTicketInputs = (inputObject) => async dispatch => {
    dispatch({
        type: UPDATE_TICKET_INPUTS,
        payload: inputObject
    });
};

export const updateTicketParams = (paramObject) => async dispatch => {
    dispatch({
        type: BUY_TICKETS_UPDATE_TICKET_PARAMS,
        payload: paramObject
    });
};

export const searchFlights = (searchParams) => async dispatch =>{
    const flights = searchFlightsApi(searchParams.origin, searchParams.destination, searchParams.departureDate, searchParams.returnDate);
    dispatch({
        type: BUY_TICKETS_SET_FLIGHT_SEARCH_RESULTS,
        payload: flights
    })
};

export const appendPerson = (person) => async dispatch =>{
    dispatch({
        type: BUY_TICKETS_APPEND_PERSON,
        payload: person
    })
};

export const removePerson = (person) => async dispatch =>{
    dispatch({
        type: BUY_TICKETS_REMOVE_PERSON,
        payload: person
    })
};

export const saveTicket = (ticket) => async dispatch =>{
    dispatch({
        type: SAVE_TICKET,
        payload: ticket
    });

    dispatch({
        type: BUY_TICKETS_FLATTEN_TICKETS,
        payload: null
    });
};
