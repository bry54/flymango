import {AUTH_ATTEMPT_SIGN_UP, AUTH_NULLIFY_LOGGED_IN_USER, AUTH_SET_LOGGED_IN_USER} from '../utilities/actionTypes';
import users from "../../utilities/api/users";

export const attemptSignIn = (credentials, DEMO=false) => async dispatch => {
    let user = null;
    if (DEMO)
        user = users[1];
    else if (credentials.signInMod === "membership")
        user = users.find(user => user.login.membership_number === credentials.membershipNumber && user.login.password === credentials.password)
    else
        user = users.find(user=>user.contacts.email === credentials.email && user.login.password === credentials.password);

    dispatch({
        type: AUTH_SET_LOGGED_IN_USER,
        payload: user
    });
};

export const attemptSignUp = (credentials) => async dispatch => {
    dispatch({
        type: AUTH_ATTEMPT_SIGN_UP,
        payload: credentials
    });
};

export const attemptSignOut = () => async dispatch => {
    dispatch({
        type: AUTH_NULLIFY_LOGGED_IN_USER,
        payload: null
    });
};
