export default [{
    "id": 1,
    "class_code": "EC",
    "class_desc": "ECONOMY",
    "flynet": true,
    "seat_power": true,
    "usb": false,
    "live_tv": false
}, {
    "id": 2,
    "class_code": "BC",
    "class_desc": "BUSINESS",
    "flynet": false,
    "seat_power": false,
    "usb": true,
    "live_tv": true
}, {
    "id": 3,
    "class_code": "FC",
    "class_desc": "FIRST CLASS",
    "flynet": false,
    "seat_power": true,
    "usb": false,
    "live_tv": false
}]
