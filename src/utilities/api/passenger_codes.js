export default [{
    "id": 1,
    "name": "Adults",
    "age_desc": "14+ Years",
    "code": "adults"
}, {
    "id": 2,
    "name": "Children",
    "age_desc": "4-14 Years",
    "code": "children"
}, {
    "id": 3,
    "name": "Infants",
    "age_desc": "0-3 Years",
    "code": "infants"
}]