export default [{
    "id": 1,
    "code": "FE",
    "definition": "Flight Early"
}, {
    "id": 2,
    "code": "NI",
    "definition": "Next Information"
}, {
    "id": 3,
    "code": "OT",
    "definition": "Flight On Time"
}, {
    "id": 4,
    "code": "DL",
    "definition": "Flight Delayed"
}, {
    "id": 5,
    "code": "NO",
    "definition": "No Status"
}]
