import airports from "./airports";
import aircrafts from "./aircrafts";
import airlines from "./airlines";
import compartments from "./compartments";
import flightStatusCodes from './flight-status-codes'
import timeStatusCodes from './time-status-codes'
import terminals from './terminals'
import schedules from "./schedules";
import flightStatuses from "./flight-statuses";
import moment from "moment";

export const getFlightById = (id) =>{
    return schedules.find(flight => flight.id === id);
};

export const getFlightByFlightNumber = (flightNumber) => {
    return schedules.find(flight => flight.marketing_carrier.flight_number === flightNumber);

};

export const getAirportById = (id) =>{
    return airports.find(airport => airport.id === id);
};

export const getAircraftById = (id) =>{
    return aircrafts.find(aircraft => aircraft.id === id)
};

export const getAirlineById = (id) =>{
    return airlines[0];
    //return airlines.find(airline => airline.id === id)
};

export const getCompartmentById = (id) =>{
    return compartments.find(compartment => compartment.id === id)
};

export const getFlightStatusCodeById = (id) =>{
    return flightStatusCodes.find(code => code.id === id)
};

export const getTimeStatusCodeById = (id) =>{
    return timeStatusCodes.find(code => code.id === id)
};

export const getTerminalById = (id) =>{
    return terminals.find(terminal => terminal.id === id)
};

export const searchFlightsApi = (originAirport, destinationAirport, departDate=null, returnDate=null) => {
    const departDateStart = moment(departDate).startOf('day');
    const returnDateStart = moment(returnDate).startOf('day');
    let inBoundFlights = [];
    const outBoundFlights = schedules.filter( schedule => (
        schedule.flight.departure.scheduled_time !== null &&
        schedule.flight.arrival.scheduled_time !== null &&
        //departDateStart.isSame(moment(schedule.flight.departure.scheduled_time).startOf('day')) &&
        schedule.flight.departure.airport_id === originAirport.id &&
        schedule.flight.arrival.airport_id === destinationAirport.id)
    );

    if (returnDate)
        inBoundFlights = schedules.filter( schedule => (
            schedule.flight.departure.scheduled_time !== null &&
            schedule.flight.arrival.scheduled_time !== null &&
            //returnDateStart.isSame(moment(schedule.flight.arrival.scheduled_time).startOf('day')) &&
            schedule.flight.departure.airport_id === destinationAirport.id &&
            schedule.flight.arrival.airport_id === originAirport.id)
        );

    return {
        flightsTo: flightsTo(outBoundFlights),
        flightsFrom: flightsFrom(inBoundFlights)
    }
};

export const getFlightStatusesByRouteApi = (originAirport, destinationAirport, departureDate) => {
    const origin_airport_id = originAirport.id;
    const destination_airport_id = destinationAirport.id;
    const departDate = moment(departureDate).startOf('day');

    const dataset = flightStatuses.map(flight => {
        return {
            ...flight,
            flightObj: getFlightById(flight.flight_id)
        }
    });

    const foundFlights = dataset.filter(data => {
        const scheduledDeparture = moment(data.departure.scheduled_utc_time).startOf('day');

        return (
            data.flightObj.flight.departure.airport_id === origin_airport_id &&
            data.flightObj.flight.arrival.airport_id === destination_airport_id &&
            scheduledDeparture.isSame(departDate, 'day')
        )
    });

    return formattedFlightStatus(foundFlights);
};

export const getFlightStatusesByFlightNumberApi = (flightNumber, departureDate) => {
    const departDate = moment(departureDate).startOf('day');
    const dataset = flightStatuses.map(flight => {
        return {
            ...flight,
            flightObj: getFlightById(flight.flight_id)
        }
    });

    const foundFlights = dataset.filter(data => {
        const scheduledDate = moment(data.departure.scheduled_utc_time).startOf('day');
        const scheduledFlight = data.flightObj.marketing_carrier.flight_number === flightNumber;

        return scheduledFlight && scheduledDate.isSame(departDate, 'day')
    });

    return formattedFlightStatus(foundFlights);
};

const flightsTo = (scheduledFlights) => {
    return scheduledFlights.map( scheduledFlight => {
        return {
            ...scheduledFlight,
            flight: {
                ...scheduledFlight.flight,
                departure: {
                    ...scheduledFlight.flight.departure,
                    airport: getAirportById(scheduledFlight.flight.departure.airport_id),
                    terminal: getTerminalById(scheduledFlight.flight.departure.terminal_id)
                },
                arrival: {
                    ...scheduledFlight.flight.arrival,
                    airport: getAirportById(scheduledFlight.flight.arrival.airport_id),
                    terminal: getTerminalById(scheduledFlight.flight.arrival.terminal_id)
                },
            },
            marketing_carrier: {
                ...scheduledFlight.marketing_carrier,
                arline: getAirlineById(scheduledFlight.marketing_carrier.airline_id)
            },
            equipment: {
                ...scheduledFlight.equipment,
                aircraft: getAircraftById(scheduledFlight.equipment.aircraft_id),
                onboarding_equipment:{
                    ...scheduledFlight.equipment.onboarding_equipment,
                    compartments: scheduledFlight.equipment.onboarding_equipment.compartments.map(compartment => {
                        return {
                            item: compartments.find(c => c.id === compartment.compartment_id),
                            data: {...compartment}
                        }}
                    ),
                }
            }
        }
    })
};

const flightsFrom = (scheduledFlights) => {
    return scheduledFlights.map( scheduledFlight => {
        return {
            ...scheduledFlight,
            flight: {
                ...scheduledFlight.flight,
                departure: {
                    ...scheduledFlight.flight.departure,
                    airport: getAirportById(scheduledFlight.flight.departure.airport_id),
                    terminal: getTerminalById(scheduledFlight.flight.departure.terminal_id)
                },
                arrival: {
                    ...scheduledFlight.flight.arrival,
                    airport: getAirportById(scheduledFlight.flight.arrival.airport_id),
                    terminal: getTerminalById(scheduledFlight.flight.arrival.terminal_id)
                },
            },
            marketing_carrier: {
                ...scheduledFlight.marketing_carrier,
                arline: getAirlineById(scheduledFlight.marketing_carrier.airline_id)
            },
            equipment: {
                ...scheduledFlight.equipment,
                aircraft: getAircraftById(scheduledFlight.equipment.aircraft_id),
                onboarding_equipment:{
                    ...scheduledFlight.equipment.onboarding_equipment,
                    compartments: scheduledFlight.equipment.onboarding_equipment.compartments.map(compartment => {
                        return {
                            item: compartments.find(c => c.id === compartment.compartment_id),
                            data: {...compartment}
                        }}
                    ),
                }
            }
        }
    })
};

const formattedFlightStatus = (foundFlights) => {
    return foundFlights.map(obj => {
        return {
            ...obj,
            flightObj: {
                ...obj.flightObj,
                equipment: {
                    ...obj.flightObj.equipment,
                    aircraft: getAircraftById(obj.flightObj.equipment.aircraft_id)
                },
                flight: {
                    ...obj.flightObj.flight,
                    arrival: {
                        ...obj.flightObj.flight.arrival,
                        airport: getAirportById(obj.flightObj.flight.arrival.airport_id),
                        terminal: getTerminalById(obj.flightObj.flight.arrival.terminal_id)
                    },
                    departure: {
                        ...obj.flightObj.flight.departure,
                        airport: getAirportById(obj.flightObj.flight.departure.airport_id),
                        terminal: getTerminalById(obj.flightObj.flight.departure.terminal_id)
                    }
                },
                marketing_carrier: {
                    ...obj.flightObj.marketing_carrier,
                    organization: getAirlineById(1)
                },
                flight_time_status: getTimeStatusCodeById(obj.flight_time_status_id),
                flight_time_status_id: obj.flight_time_status_id,
            },
        }
    })
};
