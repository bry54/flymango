export default [{
    "id": 1,
    "code": "CD",
    "definition": "Cancelled"
}, {
    "id": 2,
    "code": "DP",
    "definition": "Departed"
}, {
    "id": 3,
    "code": "LD",
    "definition": "Landed"
}, {
    "id": 4,
    "code": "RT",
    "definition": "Rerouted"
}, {
    "id": 5,
    "code": "NA",
    "definition": "No Status"
}]
