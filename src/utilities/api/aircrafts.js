export default [{
    "id": 1,
    "aircraft_code": "777",
    "airline_equipment_code": "E61",
    "name": {
        "en": "Boeing",
        "other": null
    }
}, {
    "id": 2,
    "aircraft_code": "A330",
    "airline_equipment_code": "C99",
    "name": {
        "en": "Airbus",
        "other": null
    }
}, {
    "id": 3,
    "aircraft_code": "767",
    "airline_equipment_code": "W88",
    "name": {
        "en": "Boeing",
        "other": null
    }
}, {
    "id": 4,
    "aircraft_code": "E190",
    "airline_equipment_code": "N85",
    "name": {
        "en": "Embraer E-jets",
        "other": null
    }
}, {
    "id": 5,
    "aircraft_code": "CRJ100",
    "airline_equipment_code": "U75",
    "name": {
        "en": "Bombardier",
        "other": null
    }
}, {
    "id": 6,
    "aircraft_code": "60N",
    "airline_equipment_code": "K52",
    "name": {
        "en": "Bombardier",
        "other": null
    }
}, {
    "id": 7,
    "aircraft_code": "51X",
    "airline_equipment_code": "E45",
    "name": {
        "en": "Embraer E-jets",
        "other": null
    }
}, {
    "id": 8,
    "aircraft_code": "07Q",
    "airline_equipment_code": "C03",
    "name": {
        "en": "Airbus",
        "other": null
    }
}, {
    "id": 9,
    "aircraft_code": "10C",
    "airline_equipment_code": "X84",
    "name": {
        "en": "Boeing",
        "other": null
    }
}, {
    "id": 10,
    "aircraft_code": "14L",
    "airline_equipment_code": "J53",
    "name": {
        "en": "Boeing",
        "other": null
    }
}, {
    "id": 11,
    "aircraft_code": "58V",
    "airline_equipment_code": "H23",
    "name": {
        "en": "Airbus",
        "other": null
    }
}, {
    "id": 12,
    "aircraft_code": "83L",
    "airline_equipment_code": "X49",
    "name": {
        "en": "Airbus",
        "other": null
    }
}, {
    "id": 13,
    "aircraft_code": "94A",
    "airline_equipment_code": "A19",
    "name": {
        "en": "Boeing",
        "other": null
    }
}, {
    "id": 14,
    "aircraft_code": "95F",
    "airline_equipment_code": "N03",
    "name": {
        "en": "Boeing",
        "other": null
    }
}, {
    "id": 15,
    "aircraft_code": "57Q",
    "airline_equipment_code": "Z23",
    "name": {
        "en": "Airbus",
        "other": null
    }
}, {
    "id": 16,
    "aircraft_code": "86F",
    "airline_equipment_code": "U40",
    "name": {
        "en": "Airbus",
        "other": null
    }
}, {
    "id": 17,
    "aircraft_code": "49V",
    "airline_equipment_code": "B55",
    "name": {
        "en": "Bombardier",
        "other": null
    }
}, {
    "id": 18,
    "aircraft_code": "28Q",
    "airline_equipment_code": "Y38",
    "name": {
        "en": "Bombardier",
        "other": null
    }
}, {
    "id": 19,
    "aircraft_code": "65J",
    "airline_equipment_code": "G82",
    "name": {
        "en": "ATR",
        "other": null
    }
}, {
    "id": 20,
    "aircraft_code": "69H",
    "airline_equipment_code": "I26",
    "name": {
        "en": "ATR",
        "other": null
    }
}]
