export default [
    {
    "id": 1,
    "airport_code": "BFN",
    "name": "Bloemfontein Airport",
    "position": {
        "lat": "34.4329",
        "long": "-119.8371"
    },
    "city": {
        "name": "Bloemfontein",
        "code": "BNF"
    },
    "province": {
        "name": "Free State",
        "code": "FS"
    },
    "country": {
        "name": "South Africa",
        "code": "ZA"
    }
}, {
    "id": 2,
    "airport_code": "CPT",
    "name": "Cape Town International Airport",
    "position": {
        "lat": "33.9754",
        "long": "-118.417"
    },
    "city": {
        "name": "Cape Town",
        "code": "CPT"
    },
    "province": {
        "name": "Western Cape",
        "code": "WC"
    },
    "country": {
        "name": "South Africa",
        "code": "ZA"
    }
}, {
    "id": 3,
    "airport_code": "DUR",
    "name": "King Shaka International Airport",
    "position": {
        "lat": "35.6124",
        "long": "-88.8412"
    },
    "city": {
        "name": "Durban",
        "code": "DUR"
    },
    "province": {
        "name": "KwaZulu Natal",
        "code": "KZN"
    },
    "country": {
        "name": "South Africa",
        "code": "ZA"
    }
}, {
    "id": 4,
    "airport_code": "ELS",
    "name": "East London Airport",
    "position": {
        "lat": "28.5642",
        "long": "-82.4165"
    },
    "city": {
        "name": "East London",
        "code": "EL"
    },
    "province": {
        "name": "Eastern Cape",
        "code": "EC"
    },
    "country": {
        "name": "South Africa",
        "code": "ZA"
    }
}, {
    "id": 5,
    "airport_code": "GRJ",
    "name": "George Airport",
    "position": {
        "lat": "33.2781",
        "long": "-111.7096"
    },
    "city": {
        "name": "George",
        "code": "GRJ"
    },
    "province": {
        "name": "Western Cape",
        "code": "WC"
    },
    "country": {
        "name": "South Africa",
        "code": "ZA"
    }
}, {
    "id": 6,
    "airport_code": "HLA",
    "name": "Lanseria International Airport",
    "position": {
        "lat": "33.7866",
        "long": "-118.2987"
    },
    "city": {
        "name": "Johannesburg",
        "code": "JHB"
    },
    "province": {
        "name": "Gauteng",
        "code": "GP"
    },
    "country": {
        "name": "South Africa",
        "code": "ZA"
    }
}, {
    "id": 7,
    "airport_code": "JNB",
    "name": "O.R. Tambo International Airport",
    "position": {
        "lat": "40.4344",
        "long": "-80.0248"
    },
    "city": {
        "name": "Johannesburg",
        "code": "JHB"
    },
    "province": {
        "name": "Gauteng",
        "code": "GP"
    },
    "country": {
        "name": "South Africa",
        "code": "ZA"
    }
}, {
    "id": 8,
    "airport_code": "PLZ",
    "name": "Port Elizabeth Airport",
    "position": {
        "lat": "30.3449",
        "long": "-81.6831"
    },
    "city": {
        "name": "Port Elizabeth",
        "code": "PE"
    },
    "province": {
        "name": "Eastern Cape",
        "code": "EC"
    },
    "country": {
        "name": "South Africa",
        "code": "ZA"
    }
}, {
    "id": 9,
    "airport_code": "ZNZ",
    "name": "Zanzibar International Airport",
    "position": {
        "lat": "42.6395",
        "long": "-112.3138"
    },
    "city": {
        "name": "Zanzibar Archipelago",
        "code": "BDV"
    },
    "province": {
        "name": "Unguja Island",
        "code": "ID"
    },
    "country": {
        "name": "Tanzania",
        "code": "TZN"
    }
}, {
    "id": 10,
    "airport_code": "HRE",
    "name": "R.G Mugabe International Airport",
    "position": {
        "lat": "38.9",
        "long": "-77.0369"
    },
    "city": {
        "name": "Harare",
        "code": "HRE"
    },
    "province": {
        "name": "Harare Metropolitan Province",
        "code": "DC"
    },
    "country": {
        "name": "Zimbabwe",
        "code": "ZW"
    }
}]
