import images from '../assets/images';


export const recoItems = [
    {
        key: '00',
        title: 'Limpopo, Game park Visits',
        text: 'Enjoy and experience amazing wildlife',
        image: images.holiday._01,
        starting_at: 'R 500'
    }, {
        key: '03',
        title: 'Zanzibar, Tanzanian Archipelago',
        text: 'Meet and enjoy the company of Swahili people',
        image: images.others._01,
        starting_at: 'R 1000'
    }, {
        key: '05',
        title: 'Victoria Falls, Zimbabwe Safari',
        text: 'Get in close proximity with untamed wild-life',
        image: images.holiday._02,
        starting_at: 'R 700'
    }
];

export const aircrafts = [
    {
    "id": 1,
    "aircraft_code": "737",
    "airline_equipment_code": "CLASSIC",
    image: images.planes._11,
    "name": {
        "en": "Boeing",
        "other": null
    },
    specs:{
        seat_width: '45cm',
        leg_room: '80cm',
        length: '45m',
        width: '3.7m',
        cargo: '30m3',
        inflight_entertainment: true
    }
}, {
    "id": 2,
    "aircraft_code": "737",
    "airline_equipment_code": "MAX",
    image: images.planes._10,
    "name": {
        "en": "Boeing",
        "other": null
    },
    specs:{
        seat_width: '50cm',
        leg_room: '76cm',
        length: '30m',
        width: '4m',
        cargo: '30m3',
    }
}];

export const defaultTickets =[
    {
        inBoundFlight: null,
        inBoundCabin: null,
        passengers: [
            {
                passengerType: {
                    id: 1,
                    name: 'Adults',
                    age_desc: '14+ Years',
                    code: 'adults'
                },
                maxPeople: 1,
                data: [
                    {
                        passengerCode: 'adults',
                        firstName: 'Brian',
                        lastName: 'Sithole ',
                        email: null,
                        phone: null,
                        passport: null
                    }
                ]
            }
        ],
        outBoundCabin: {
            item: {
                id: 3,
                class_code: 'FC',
                class_desc: 'FIRST CLASS',
                flynet: false,
                seat_power: true,
                usb: false,
                live_tv: false
            },
            data: {
                compartment_id: 3,
                fares: [
                    {
                        passenger_type_id: 1,
                        amount: 2065
                    },
                    {
                        passenger_type_id: 2,
                        amount: 2123
                    },
                    {
                        passenger_type_id: 3,
                        amount: 2592
                    }
                ]
            }
        },
        outBoundFlight: {
            id: 44,
            total_journey: 'P1T2dH9dM',
            flight_time_status_id: 4,
            distance: 500,
            flight: {
                status_id: 3,
                departure: {
                    airport_id: 3,
                    scheduled_time: '2020-04-05 21:30:00',
                    terminal_id: 6,
                    airport: {
                        id: 3,
                        airport_code: 'DUR',
                        name: 'King Shaka International Airport',
                        position: {
                            lat: '35.6124',
                            'long': '-88.8412'
                        },
                        city: {
                            name: 'Durban',
                            code: 'DUR'
                        },
                        province: {
                            name: 'KwaZulu Natal',
                            code: 'KZN'
                        },
                        country: {
                            name: 'South Africa',
                            code: 'ZA'
                        }
                    },
                    terminal: {
                        id: 6,
                        name: '3',
                        gate: 'G98'
                    }
                },
                arrival: {
                    airport_id: 6,
                    scheduled_time: '2020-04-06 00:15:00',
                    terminal_id: 1,
                    airport: {
                        id: 6,
                        airport_code: 'HLA',
                        name: 'Lanseria International Airport',
                        position: {
                            lat: '33.7866',
                            'long': '-118.2987'
                        },
                        city: {
                            name: 'Johannesburg',
                            code: 'JHB'
                        },
                        province: {
                            name: 'Gauteng',
                            code: 'GP'
                        },
                        country: {
                            name: 'South Africa',
                            code: 'ZA'
                        }
                    },
                    terminal: {
                        id: 1,
                        name: '6',
                        gate: 'S90'
                    }
                }
            },
            marketing_carrier: {
                airline_id: 1,
                flight_number: 'MNG-696961',
                arline: {
                    id: 1,
                    airline_id: 'MNG',
                    airline_id_icao: 'MNG',
                    name: {
                        en: 'Mango Airlines',
                        other: null
                    }
                }
            },
            equipment: {
                aircraft_id: 4,
                aircraft: {
                    id: 4,
                    aircraft_code: 'E190',
                    airline_equipment_code: 'N85',
                    name: {
                        en: 'Embraer E-jets',
                        other: null
                    }
                },
                onboarding_equipment: {
                    inflight_entertainment: false,
                    compartments: [
                        {
                            item: {
                                id: 1,
                                class_code: 'EC',
                                class_desc: 'ECONOMY',
                                flynet: true,
                                seat_power: true,
                                usb: false,
                                live_tv: false
                            },
                            data: {
                                compartment_id: 1,
                                fares: [
                                    {
                                        passenger_type_id: 1,
                                        amount: 1234
                                    },
                                    {
                                        passenger_type_id: 2,
                                        amount: 1028
                                    },
                                    {
                                        passenger_type_id: 3,
                                        amount: 1089
                                    }
                                ]
                            }
                        },
                        {
                            item: {
                                id: 2,
                                class_code: 'BC',
                                class_desc: 'BUSINESS',
                                flynet: false,
                                seat_power: false,
                                usb: true,
                                live_tv: true
                            },
                            data: {
                                compartment_id: 2,
                                fares: [
                                    {
                                        passenger_type_id: 1,
                                        amount: 1751
                                    },
                                    {
                                        passenger_type_id: 2,
                                        amount: 1631
                                    },
                                    {
                                        passenger_type_id: 3,
                                        amount: 1941
                                    }
                                ]
                            }
                        },
                        {
                            item: {
                                id: 3,
                                class_code: 'FC',
                                class_desc: 'FIRST CLASS',
                                flynet: false,
                                seat_power: true,
                                usb: false,
                                live_tv: false
                            },
                            data: {
                                compartment_id: 3,
                                fares: [
                                    {
                                        passenger_type_id: 1,
                                        amount: 2065
                                    },
                                    {
                                        passenger_type_id: 2,
                                        amount: 2123
                                    },
                                    {
                                        passenger_type_id: 3,
                                        amount: 2592
                                    }
                                ]
                            }
                        }
                    ]
                }
            }
        }
    },

    {
        passengers: [
            {
                passengerType: {
                    id: 1,
                    name: 'Adults',
                    age_desc: '14+ Years',
                    code: 'adults'
                },
                maxPeople: 1,
                data: [
                    {
                        passengerCode: 'adults',
                        firstName: 'Brian',
                        lastName: 'Sithole ',
                        email: null,
                        phone: null,
                        passport: null
                    }
                ]
            }
        ],
        outBoundCabin: {
            item: {
                id: 3,
                class_code: 'FC',
                class_desc: 'FIRST CLASS',
                flynet: false,
                seat_power: true,
                usb: false,
                live_tv: false
            },
            data: {
                compartment_id: 3,
                fares: [
                    {
                        passenger_type_id: 1,
                        amount: 2536
                    },
                    {
                        passenger_type_id: 2,
                        amount: 2779
                    },
                    {
                        passenger_type_id: 3,
                        amount: 2205
                    }
                ]
            }
        },
        outBoundFlight: {
            id: 101,
            total_journey: 'P1T9dH0dM',
            flight_time_status_id: 1,
            distance: 3740,
            flight: {
                status_id: 2,
                departure: {
                    airport_id: 2,
                    scheduled_time: '2020-03-18 22:28:47',
                    terminal_id: 7,
                    airport: {
                        id: 2,
                        airport_code: 'CPT',
                        name: 'Cape Town International Airport',
                        position: {
                            lat: '33.9754',
                            'long': '-118.417'
                        },
                        city: {
                            name: 'Cape Town',
                            code: 'CPT'
                        },
                        province: {
                            name: 'Western Cape',
                            code: 'WC'
                        },
                        country: {
                            name: 'South Africa',
                            code: 'ZA'
                        }
                    },
                    terminal: {
                        id: 7,
                        name: '7',
                        gate: 'Z76'
                    }
                },
                arrival: {
                    airport_id: 9,
                    scheduled_time: '2020-03-19 00:02:47',
                    terminal_id: 1,
                    airport: {
                        id: 9,
                        airport_code: 'ZNZ',
                        name: 'Zanzibar International Airport',
                        position: {
                            lat: '42.6395',
                            'long': '-112.3138'
                        },
                        city: {
                            name: 'Zanzibar Archipelago',
                            code: 'BDV'
                        },
                        province: {
                            name: 'Unguja Island',
                            code: 'ID'
                        },
                        country: {
                            name: 'Tanzania',
                            code: 'TZN'
                        }
                    },
                    terminal: {
                        id: 1,
                        name: '6',
                        gate: 'S90'
                    }
                }
            },
            marketing_carrier: {
                airline_id: 1,
                flight_number: 'MNG-606598',
                arline: {
                    id: 1,
                    airline_id: 'MNG',
                    airline_id_icao: 'MNG',
                    name: {
                        en: 'Mango Airlines',
                        other: null
                    }
                }
            },
            equipment: {
                aircraft_id: 1,
                aircraft: {
                    id: 1,
                    aircraft_code: '777',
                    airline_equipment_code: 'E61',
                    name: {
                        en: 'Boeing',
                        other: null
                    }
                },
                onboarding_equipment: {
                    inflight_entertainment: true,
                    compartments: [
                        {
                            item: {
                                id: 1,
                                class_code: 'EC',
                                class_desc: 'ECONOMY',
                                flynet: true,
                                seat_power: true,
                                usb: false,
                                live_tv: false
                            },
                            data: {
                                compartment_id: 1,
                                fares: [
                                    {
                                        passenger_type_id: 1,
                                        amount: 1250
                                    },
                                    {
                                        passenger_type_id: 2,
                                        amount: 1269
                                    },
                                    {
                                        passenger_type_id: 3,
                                        amount: 1154
                                    }
                                ]
                            }
                        },
                        {
                            item: {
                                id: 2,
                                class_code: 'BC',
                                class_desc: 'BUSINESS',
                                flynet: false,
                                seat_power: false,
                                usb: true,
                                live_tv: true
                            },
                            data: {
                                compartment_id: 2,
                                fares: [
                                    {
                                        passenger_type_id: 1,
                                        amount: 1620
                                    },
                                    {
                                        passenger_type_id: 2,
                                        amount: 1638
                                    },
                                    {
                                        passenger_type_id: 3,
                                        amount: 1691
                                    }
                                ]
                            }
                        },
                        {
                            item: {
                                id: 3,
                                class_code: 'FC',
                                class_desc: 'FIRST CLASS',
                                flynet: false,
                                seat_power: true,
                                usb: false,
                                live_tv: false
                            },
                            data: {
                                compartment_id: 3,
                                fares: [
                                    {
                                        passenger_type_id: 1,
                                        amount: 2536
                                    },
                                    {
                                        passenger_type_id: 2,
                                        amount: 2779
                                    },
                                    {
                                        passenger_type_id: 3,
                                        amount: 2205
                                    }
                                ]
                            }
                        }
                    ]
                }
            }
        },
        inBoundCabin: {
            item: {
                id: 2,
                class_code: 'BC',
                class_desc: 'BUSINESS',
                flynet: false,
                seat_power: false,
                usb: true,
                live_tv: true
            },
            data: {
                compartment_id: 2,
                fares: [
                    {
                        passenger_type_id: 1,
                        amount: 1812
                    },
                    {
                        passenger_type_id: 2,
                        amount: 1803
                    },
                    {
                        passenger_type_id: 3,
                        amount: 1841
                    }
                ]
            }
        },
        inBoundFlight: {
            id: 75,
            total_journey: 'P1T3dH9dM',
            flight_time_status_id: 5,
            distance: 3740,
            flight: {
                status_id: 5,
                departure: {
                    airport_id: 9,
                    scheduled_time: '2020-06-12 19:00:23',
                    terminal_id: 5,
                    airport: {
                        id: 9,
                        airport_code: 'ZNZ',
                        name: 'Zanzibar International Airport',
                        position: {
                            lat: '42.6395',
                            'long': '-112.3138'
                        },
                        city: {
                            name: 'Zanzibar Archipelago',
                            code: 'BDV'
                        },
                        province: {
                            name: 'Unguja Island',
                            code: 'ID'
                        },
                        country: {
                            name: 'Tanzania',
                            code: 'TZN'
                        }
                    },
                    terminal: {
                        id: 5,
                        name: '4',
                        gate: 'V26'
                    }
                },
                arrival: {
                    airport_id: 2,
                    scheduled_time: '2020-06-12 21:28:23',
                    terminal_id: 3,
                    airport: {
                        id: 2,
                        airport_code: 'CPT',
                        name: 'Cape Town International Airport',
                        position: {
                            lat: '33.9754',
                            'long': '-118.417'
                        },
                        city: {
                            name: 'Cape Town',
                            code: 'CPT'
                        },
                        province: {
                            name: 'Western Cape',
                            code: 'WC'
                        },
                        country: {
                            name: 'South Africa',
                            code: 'ZA'
                        }
                    },
                    terminal: {
                        id: 3,
                        name: '1',
                        gate: 'F89'
                    }
                }
            },
            marketing_carrier: {
                airline_id: 1,
                flight_number: 'MNG-619468',
                arline: {
                    id: 1,
                    airline_id: 'MNG',
                    airline_id_icao: 'MNG',
                    name: {
                        en: 'Mango Airlines',
                        other: null
                    }
                }
            },
            equipment: {
                aircraft_id: 15,
                aircraft: {
                    id: 15,
                    aircraft_code: '57Q',
                    airline_equipment_code: 'Z23',
                    name: {
                        en: 'Airbus',
                        other: null
                    }
                },
                onboarding_equipment: {
                    inflight_entertainment: false,
                    compartments: [
                        {
                            item: {
                                id: 1,
                                class_code: 'EC',
                                class_desc: 'ECONOMY',
                                flynet: true,
                                seat_power: true,
                                usb: false,
                                live_tv: false
                            },
                            data: {
                                compartment_id: 1,
                                fares: [
                                    {
                                        passenger_type_id: 1,
                                        amount: 1342
                                    },
                                    {
                                        passenger_type_id: 2,
                                        amount: 1261
                                    },
                                    {
                                        passenger_type_id: 3,
                                        amount: 1393
                                    }
                                ]
                            }
                        },
                        {
                            item: {
                                id: 2,
                                class_code: 'BC',
                                class_desc: 'BUSINESS',
                                flynet: false,
                                seat_power: false,
                                usb: true,
                                live_tv: true
                            },
                            data: {
                                compartment_id: 2,
                                fares: [
                                    {
                                        passenger_type_id: 1,
                                        amount: 1812
                                    },
                                    {
                                        passenger_type_id: 2,
                                        amount: 1803
                                    },
                                    {
                                        passenger_type_id: 3,
                                        amount: 1841
                                    }
                                ]
                            }
                        },
                        {
                            item: {
                                id: 3,
                                class_code: 'FC',
                                class_desc: 'FIRST CLASS',
                                flynet: false,
                                seat_power: true,
                                usb: false,
                                live_tv: false
                            },
                            data: {
                                compartment_id: 3,
                                fares: [
                                    {
                                        passenger_type_id: 1,
                                        amount: 2533
                                    },
                                    {
                                        passenger_type_id: 2,
                                        amount: 2275
                                    },
                                    {
                                        passenger_type_id: 3,
                                        amount: 2691
                                    }
                                ]
                            }
                        }
                    ]
                }
            }
        }
    }
];

export const contactUs = {
    main:{
        name: 'Mango Reception',
        phone: [{
            name: 'South Africa',
            phones:['086 101 0217']
        },{
            name: 'International',
            phones: ['JNB: +27 11 086 6100' ,'CPT: +27 21 815 4100']
        }],
        physical_address:{
            line1: 'Mezzanine Level',
            line2: 'Domestic Departure Terminal',
            line3: 'OR Tambo International Airport',
            line4: '1627'
        },
        postal_address:{
            line1: 'P.O. Box 1273',
            line2: 'OR Tambo International Airport',
            line3: '1627',
        }
    },

    call_center: {
        name: 'Mango Call Centre',
        phone: [{
            name: 'South Africa',
            phones: ['086 100 1234']
        },{ name: 'International',
            phones: ['JNB: +27 (11) 086 6100', 'CPT: +27 (21) 815 4100']
        }],
        email: {
            enquiries: 'enquiries@flymango.com',
            spokesperson: 'spokesperson@flymango.com'
        }
    },

    others: [
        {
            icon:'help-box',
            name: 'Mango Corporate Help Desk',
            phone:	'086 101 0210'
        },  {
            icon:'help-box',
            name: 'Mango Travel Agent Help Desk',
            phone:	'086 101 0212'
        }, {
            icon:'lifebuoy',
            name: 'Mango Guest Care',
            phone: '086 101 0002',
            email: 'guestcare@flymango.com'
        }, {
            icon:'calendar-edit',
            name: 'Mango Flight Schedule Changes',
            phone:	'086 101 0216'
        }, {
            icon:'medical-bag',
            name: 'Mango Medicals',
            phone: '086 101 0214',
            email: 'Medicals@flymango.com'
        }, {
            icon:'cash-refund',
            name: 'Mango Refunds',
            phone:	'086 101 0211',
            email: 'MangoRefunds@flymango.com'
        }, {
            icon:'account-group',
            name: 'Mango Group Bookings',
            phone: '086 101 0003',
            email: 'groupbookings@flymango.com'
        }, {
            icon:'cart',
            name: 'Mango National Baggage Line',
            phone: '086 172 7522'
        }
    ]
};
