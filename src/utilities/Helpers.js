export const updateItemVisibility = (itemKey, itemVisibility, itemMode, handlerFunction) =>{
    const item ={
        key: itemKey,
        visibility: itemVisibility,
        mode: itemMode
    };
    handlerFunction(item)
};

export const updateInputItem = (itemKey, itemValue, handlerFunction) =>{
    const item = {
        key: itemKey,
        value: itemValue
    };
    handlerFunction(item);
};
