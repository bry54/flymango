import React from 'react';
import {Platform, Text, TouchableOpacity} from "react-native";
import {createStackNavigator} from '@react-navigation/stack';
import ColorDefinitions from "../../constants/ColorDefinitions";
import MenuButton from "../../components/menu/MenuButton";
import {createMaterialTopTabNavigator} from "@react-navigation/material-top-tabs";
import StatusByFlightNumberScreen from "../../screens/main/flight-status/StatusByFlightNumberScreen";
import StatusByRouteScreen from "../../screens/main/flight-status/StatusByRouteScreen";
import SettingsButton from "../../components/menu/SettingsButton";
import SearchResultsScreen from "../../screens/main/flight-status/SearchResultsScreen";
import DetailedStatusScreen from "../../screens/main/flight-status/DetailedStatusScreen";

const FlightStatusNavigator = createStackNavigator();
const FlightStatusTabNavigator = createMaterialTopTabNavigator();

export default () =>(
    <FlightStatusNavigator.Navigator
        initialRouteName="TabsNavigator"
        headerMode={Platform.OS === 'ios' ? 'float' : 'screen'}
        screenOptions={{
            headerTintColor: ColorDefinitions.darkText.shade4,
            headerStyle: { backgroundColor: ColorDefinitions.colorPrimary },
        }}>

        <FlightStatusNavigator.Screen
            name="TabsNavigator"
            component={TabsNavigator}
            options={({ navigation, route }) => ({
                headerTitle: 'Flight Status',
                headerTitleStyle: {fontFamily: 'alpha-regular'},
                headerLeft: () => ( <MenuButton navigation={navigation}/> ),
                headerRight: () => ( <SettingsButton navigation={navigation}/> ),
            })}
        />

        <FlightStatusNavigator.Screen
            name="SearchResults"
            component={SearchResultsScreen}
            options={({ navigation, route }) => ({
                headerTitle: 'Flight Status',
                headerTitleStyle: {fontFamily: 'alpha-regular'},
                headerRight: () => (
                    <TouchableOpacity
                        activeOpacity={.9}
                        onPress={() => {
                        navigation.popToTop();
                        navigation.navigate('Home');
                    }}>
                        <Text style={{fontFamily: 'alpha-regular', fontSize: 18, paddingHorizontal: 10}}>Cancel</Text>
                    </TouchableOpacity>
                ),
            })}
        />

        <FlightStatusNavigator.Screen
            name="DetailedFlightStatus"
            component={DetailedStatusScreen}
            options={({ navigation, route }) => ({
                headerTitle: 'Flight Status',
                headerTitleStyle: {fontFamily: 'alpha-regular'},
                headerRight: () => (
                    <TouchableOpacity
                        activeOpacity={.9}
                        onPress={() => {
                        navigation.popToTop();
                        navigation.navigate('Home');
                    }}>
                        <Text style={{fontFamily: 'alpha-regular', fontSize: 18, paddingHorizontal: 10}}>Done</Text>
                    </TouchableOpacity>
                ),
            })}
        />
    </FlightStatusNavigator.Navigator>
);

const TabsNavigator = () => (
    <FlightStatusTabNavigator.Navigator
        lazy={true}
        initialRouteName="StatusByFlightNumber"
        headerMode="screen"
        tabBarOptions={{
            indicatorStyle: {borderColor: ColorDefinitions.darkBackgroundColor.shade1, borderBottomWidth: 2},
            labelStyle: {color: ColorDefinitions.darkText.shade4, fontFamily: 'alpha-regular'},
            style: { backgroundColor: ColorDefinitions.colorPrimary },
        }}>

        <FlightStatusTabNavigator.Screen
            name="StatusByRoute"
            component={StatusByRouteScreen}
            options={({ navigation, route }) => ({
                title: 'Route',
                headerShown: true
            })}
        />

        <FlightStatusTabNavigator.Screen
            name="StatusByFlightNumber"
            component={StatusByFlightNumberScreen}
            options={({ navigation, route }) => ({
                title: 'Flight Number',
                headerShown: true
            })}
        />
    </FlightStatusTabNavigator.Navigator>
);
