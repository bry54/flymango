import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import LandingScreen from "../../screens/main/airport-maps/LandingScreen";
import ColorDefinitions from "../../constants/ColorDefinitions";
import MenuButton from "../../components/menu/MenuButton";
import SettingsButton from "../../components/menu/SettingsButton";

const AirportStack = createStackNavigator();

export default () => (
    <AirportStack.Navigator
        initialRouteName="Landing"
        headerMode="screen"
        screenOptions={{
            headerTintColor: ColorDefinitions.darkText.shade4,
            headerStyle: { backgroundColor: ColorDefinitions.colorPrimary },
        }}>

        <AirportStack.Screen
            name="Landing"
            component={LandingScreen}
            options={({ navigation, route }) => ({
                headerTitle: 'Airport Maps',
                headerTitleStyle: {fontFamily: 'alpha-regular'},
                headerLeft: () => ( <MenuButton navigation={navigation}/> ),
                headerRight: () => ( <SettingsButton navigation={navigation}/> ),
            })}
        />
    </AirportStack.Navigator>
)
