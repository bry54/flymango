import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import LandingScreen from "../../screens/main/mango-miles/LandingScreen";
import DummyScreen from "../../screens/main/utilities/LandingScreen";
import ColorDefinitions from "../../constants/ColorDefinitions";
import MenuButton from "../../components/menu/MenuButton";
import SettingsButton from "../../components/menu/SettingsButton";

const MangoMilesStack = createStackNavigator();

export default () => (
    <MangoMilesStack.Navigator
        initialRouteName="Landing"
        headerMode="screen"
        screenOptions={{
            headerTintColor: ColorDefinitions.darkText.shade4,
            headerStyle: { backgroundColor: ColorDefinitions.colorPrimary },
        }}>

        <MangoMilesStack.Screen
            name="Landing"
            component={LandingScreen}
            options={({ navigation, route }) => ({
                headerTitle: 'Mango Miles',
                headerTitleStyle: {fontFamily: 'alpha-regular'},
                headerLeft: () => ( <MenuButton navigation={navigation}/> ),
                headerRight: () => ( <SettingsButton navigation={navigation}/> ),
            })}
        />

        <MangoMilesStack.Screen
            name="Dummy"
            component={DummyScreen}
            options={({ navigation, route }) => ({
                headerTitle: 'Mango Miles',
                headerTitleStyle: {fontFamily: 'alpha-regular'},
            })}
        />
    </MangoMilesStack.Navigator>
)
