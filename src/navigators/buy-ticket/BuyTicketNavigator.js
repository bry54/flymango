import React from 'react';
import {Platform, TouchableOpacity} from "react-native";
import {createStackNavigator} from '@react-navigation/stack';
import ColorDefinitions from "../../constants/ColorDefinitions";
import MenuButton from "../../components/menu/MenuButton";
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import SettingsButton from "../../components/menu/SettingsButton";
import SearchResultsScreen from "../../screens/main/buy-ticket/SearchResultsScreen";
import PassengerDetailsScreen from "../../screens/main/buy-ticket/PassengerDetailsScreen";
import OneWayTripScreen from "../../screens/main/buy-ticket/LandingScreen";
import RoundTripScreen from "../../screens/main/buy-ticket/LandingScreen";
import AddPersonScreen from "../../screens/main/buy-ticket/AddPersonScreen";
import AirportSelectScreen from "../../screens/main/buy-ticket/AirportSelectScreen";
import {MaterialCommunityIcons} from "@expo/vector-icons";
import ConfirmBookingScreen from "../../screens/main/buy-ticket/ConfirmBookingScreen";
import MakePaymentScreen from "../../screens/main/buy-ticket/MakePaymentScreen";

const BuyTicketNavigator = createStackNavigator();
const FlightsTabNavigator = createMaterialTopTabNavigator();

export default () => (
    <BuyTicketNavigator.Navigator
        initialRouteName="TabsNavigator"
        headerMode={Platform.OS === 'ios' ? 'float' : 'screen'}
        screenOptions={{
            headerTintColor: ColorDefinitions.darkText.shade4,
            headerStyle: {backgroundColor: ColorDefinitions.colorPrimary},
        }}>

        <FlightsTabNavigator.Screen
            name="AirportSelection"
            component={AirportSelectScreen}
            options={({navigation, route}) => ({
                headerTitle: 'Select Airport',
                headerTitleStyle: {fontFamily: 'alpha-regular'},
            })}
        />

        <BuyTicketNavigator.Screen
            name="TabsNavigator"
            component={TabsNavigator}

            options={({navigation, route}) => ({
                headerTitle: 'Buy Tickets',
                headerTitleStyle: {fontFamily: 'alpha-regular'},
                headerLeft: () => (<MenuButton navigation={navigation}/>),
                headerRight: () => (<SettingsButton navigation={navigation}/>),
            })}
        />

        <BuyTicketNavigator.Screen
            name="OutboundFlightSelection"
            component={SearchResultsScreen}
            options={({navigation, route}) => ({
                headerTitle: 'Outbound Flights ',
                headerTitleStyle: {fontFamily: 'alpha-regular'},
                headerRight: () => (<SettingsButton navigation={navigation}/>),
            })}
        />

        <BuyTicketNavigator.Screen
            name="InboundFlightSelection"
            component={SearchResultsScreen}
            options={({navigation, route}) => ({
                headerTitle: 'Inbound Flights ',
                headerTitleStyle: {fontFamily: 'alpha-regular'},
                headerRight: () => (<SettingsButton navigation={navigation}/>),
            })}
        />

        <BuyTicketNavigator.Screen
            name="PassengerDetails"
            component={PassengerDetailsScreen}
            options={({navigation, route}) => ({
                headerTitle: 'Passenger Details ',
                headerTitleStyle: {fontFamily: 'alpha-regular'},
                headerRight: () => (
                    <TouchableOpacity
                        activeOpacity={.9}
                        onPress={()=> navigation.navigate('BookingConfirmation')}>
                        <MaterialCommunityIcons name='checkbox-marked-circle-outline' color={ColorDefinitions.darkText.shade4} size={30} style={{paddingHorizontal: 15, elevation: 5}}/>
                    </TouchableOpacity>
                ),
            })}
        />

        <BuyTicketNavigator.Screen
            name="AddPerson"
            component={AddPersonScreen}
            options={({navigation, route}) => ({
                headerTitle: 'Add People',
                headerTitleStyle: {fontFamily: 'alpha-regular'},
            })}
        />

        <BuyTicketNavigator.Screen
            name="BookingConfirmation"
            component={ConfirmBookingScreen}
            options={({navigation, route}) => ({
                headerTitle: 'Confirm Booking',
                headerTitleStyle: {fontFamily: 'alpha-regular'},
                headerRight: () => (
                    <TouchableOpacity
                        activeOpacity={.9}
                        onPress={()=> navigation.navigate('MakePayment')}>
                        <MaterialCommunityIcons name='checkbox-marked-circle-outline' color={ColorDefinitions.darkText.shade4} size={30} style={{paddingHorizontal: 15, elevation: 5}}/>
                    </TouchableOpacity>
                ),
            })}
        />

        <BuyTicketNavigator.Screen
            name="MakePayment"
            component={MakePaymentScreen}
            options={({navigation, route}) => ({
                headerTitle: 'Make Payment',
                headerTitleStyle: {fontFamily: 'alpha-regular'},
            })}
        />
    </BuyTicketNavigator.Navigator>
);

const TabsNavigator = () => (
    <FlightsTabNavigator.Navigator
        lazy={true}
        initialRouteName="OneWayTrip"
        headerMode="screen"
        tabBarOptions={{
            indicatorStyle: {borderColor: ColorDefinitions.darkBackgroundColor.shade1, borderBottomWidth: 2},
            labelStyle: {color: ColorDefinitions.darkText.shade4, fontFamily: 'alpha-regular'},
            style: {backgroundColor: ColorDefinitions.colorPrimary},
        }}>

        <FlightsTabNavigator.Screen
            name="OneWayTrip"
            component={OneWayTripScreen}
            options={({navigation, route}) => ({
                title: 'ONE WAY',
                headerShown: true
            })}
        />

        <FlightsTabNavigator.Screen
            name="RoundTrip"
            component={RoundTripScreen}
            options={({navigation, route}) => ({
                title: 'ROUND TRIP',
                headerShown: true
            })}
        />
    </FlightsTabNavigator.Navigator>
);
