import React from 'react';
import {createDrawerNavigator} from '@react-navigation/drawer';
import NavigationDrawerContent from "../components/menu/NavigationDrawerContent";
import ColorDefinitions from "../constants/ColorDefinitions";
import DrawerLabel from "../components/menu/DrawerLabel";
import HomeNavigator from "./home/HomeNavigator";
import MangoMilesNavigator from "./mango-miles/MangoMilesNavigator";
import MyFlightsNavigator from "./my-flights/MyFlightsNavigator";
import BuyTicketNavigator from "./buy-ticket/BuyTicketNavigator";
import CheckInNavigator from "./mobile-checkin/CheckInNavigator";
import BookHotelNavigator from "./book-hotel/BookHotelNavigator";
import RentCarNavigator from "./rent-car/RentCarNavigator";
import AirportNavigator from "./airport-maps/AirportNavigator";
import SettingsNavigator from "./settings/SettingsNavigator";
import HelpNavigator from "./help/HelpNavigator";
import AboutAppNavigator from "./about-app/AboutAppNavigator";
import MyProfileNavigator from "./my-profile/MyProfileNavigator";
import FlightStatusNavigator from "./flight-status/FlightStatusNavigator";
import NotificationsNavigator from "./options/NotificationsNavigator";
import FleetNavigator from "./fleet/FleetNavigator";
import FlightSuggestionsNavigator from "./flight-suggestions/FlightSuggestionsNavigator";

const Drawer = createDrawerNavigator();

export default () => (
    <Drawer.Navigator
        initialRouteName='Home'
        drawerType='slide'
        drawerContent={props => <NavigationDrawerContent {...props} />}
        overlayColor={'rgba( 14,  6, 50, .8)' }
        drawerContentOptions={{
            activeBackgroundColor: ColorDefinitions.colorPrimaryDark,
            activeTintColor: ColorDefinitions.darkText.shade4,
            itemStyle: { marginVertical: 0 },
            labelStyle: { fontFamily: 'alpha-regular', fontWeight: '700', width: '100%'}
        }}
        drawerStyle={{
            backgroundColor: ColorDefinitions.colorPrimary,
        }}>
        <Drawer.Screen
            name="Home"
            component={HomeNavigator}
            options={({ navigation, route }) => ({
                title: 'Home',
                drawerIcon: null,
                drawerLabel: () => (<DrawerLabel label='Home' count={null}/>),
            })}
        />

        <Drawer.Screen
            name="SuggestedDestinations"
            component={FlightSuggestionsNavigator}
            options={({ navigation, route }) => ({
                title: 'Suggested Destinations',
                drawerIcon: null,
                drawerLabel: () => (<DrawerLabel label='Suggested Destinations' count={null}/>),
            })}
        />

        <Drawer.Screen
            name="MangoMiles"
            component={MangoMilesNavigator}
            options={({ navigation, route }) => ({
                title: 'Mango Miles',
                drawerIcon: null,
                drawerLabel: () => (<DrawerLabel label='My Mango Miles' count={null}/>),
            })}
        />

        <Drawer.Screen
            name="MyFlights"
            component={MyFlightsNavigator}
            options={({ navigation, route }) => ({
                title: 'My Flights',
                drawerIcon: null,
                drawerLabel: () => (<DrawerLabel label='My Flights' count={null}/>),
            })}
        />

        <Drawer.Screen
            name="BuyTicket"
            component={BuyTicketNavigator}
            options={({ navigation, route }) => ({
                title: 'Buy Ticket',
                drawerIcon: null,
                drawerLabel: () => (<DrawerLabel label='Buy Ticket' count={null}/>),
            })}
        />

        <Drawer.Screen
            name="MobileCheckIn"
            component={CheckInNavigator}
            options={({ navigation, route }) => ({
                title: 'Mobile Check In',
                drawerIcon: null,
                drawerLabel: () => (<DrawerLabel label='Mobile Check In' count={null}/>),
            })}
        />

        <Drawer.Screen
            name="FlightStatus"
            component={FlightStatusNavigator}
            options={({ navigation, route }) => ({
                title: 'Flight Status',
                drawerIcon: null,
                drawerLabel: () => (<DrawerLabel label='Flight Status' count={null}/>),
            })}
        />

        <Drawer.Screen
            name="AircraftFleet"
            component={FleetNavigator}
            options={({ navigation, route }) => ({
                title: 'Aircraft Fleet',
                drawerIcon: null,
                drawerLabel: () => (<DrawerLabel label='Aircraft Fleet' count={null}/>),
            })}
        />

        <Drawer.Screen
            name="BookHotel"
            component={BookHotelNavigator}
            options={({ navigation, route }) => ({
                title: 'Book Hotel',
                drawerIcon: null,
                drawerLabel: () => (<DrawerLabel label='Book Hotel' count={null}/>),
            })}
        />

        <Drawer.Screen
            name="RentCar"
            component={RentCarNavigator}
            options={({ navigation, route }) => ({
                title: 'Rent A Car',
                drawerIcon: null,
                drawerLabel: () => (<DrawerLabel label='Rent A Car' count={null}/>),
            })}
        />

        <Drawer.Screen
            name="AirportMaps"
            component={AirportNavigator}
            options={({ navigation, route }) => ({
                title: 'Airport Maps',
                drawerIcon: null,
                drawerLabel: () => (<DrawerLabel label='Airport Maps' count={null}/>),
            })}
        />

        <Drawer.Screen
            name="Settings"
            component={SettingsNavigator}
            options={({ navigation, route }) => ({
                title: 'Settings',
                drawerIcon: null,
                drawerLabel: () => (<DrawerLabel label='Settings' count={null}/>),
            })}
        />

        <Drawer.Screen
            name="Help"
            component={HelpNavigator}
            options={({ navigation, route }) => ({
                title: 'Help',
                drawerIcon: null,
                drawerLabel: () => (<DrawerLabel label='Help' count={null}/>),
            })}
        />

        <Drawer.Screen
            name="AboutApp"
            component={AboutAppNavigator}
            options={({ navigation, route }) => ({
                enabled: false,
                title: 'About App',
                drawerIcon: null,
                drawerLabel: () => null//() => (<DrawerLabel label='About App' count={null}/>),
            })}
        />

        <Drawer.Screen
            name="MyProfile"
            component={MyProfileNavigator}
            options={({ navigation, route }) => ({
                title: () => null,
                drawerIcon: null,
                drawerLabel: () => null
            })}
        />

        <Drawer.Screen
            name="Notifications"
            component={NotificationsNavigator}
            options={({ navigation, route }) => ({
                title: () => null,
                drawerIcon: null,
                drawerLabel: () => null
            })}
        />
    </Drawer.Navigator>
)

