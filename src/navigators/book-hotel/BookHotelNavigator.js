import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import LandingScreen from "../../screens/main/book-hotel/LandingScreen";
import ColorDefinitions from "../../constants/ColorDefinitions";
import MenuButton from "../../components/menu/MenuButton";
import SettingsButton from "../../components/menu/SettingsButton";

const HotelBookingStack = createStackNavigator();

export default () => (
    <HotelBookingStack.Navigator
        initialRouteName="Landing"
        headerMode="screen"
        screenOptions={{
            headerTintColor: ColorDefinitions.darkText.shade4,
            headerStyle: { backgroundColor: ColorDefinitions.colorPrimary },
        }}>

        <HotelBookingStack.Screen
            name="Landing"
            component={LandingScreen}
            options={({ navigation, route }) => ({
                headerTitle: 'Book Hotel',
                headerTitleStyle: {fontFamily: 'alpha-regular'},
                headerLeft: () => ( <MenuButton navigation={navigation}/> ),
                headerRight: () => ( <SettingsButton navigation={navigation}/> ),
            })}
        />
    </HotelBookingStack.Navigator>
)
