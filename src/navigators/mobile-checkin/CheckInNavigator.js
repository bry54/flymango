import React from 'react';
import {Platform} from "react-native";
import {createStackNavigator} from '@react-navigation/stack';
import ColorDefinitions from "../../constants/ColorDefinitions";
import MenuButton from "../../components/menu/MenuButton";
import {createMaterialTopTabNavigator} from "@react-navigation/material-top-tabs";
import TicketCheckInScreen from "../../screens/main/mobile-checkin/TicketCheckInScreen";
import SavedFlightsScreen from "../../screens/main/mobile-checkin/SavedFlightsScreen";
import SettingsButton from "../../components/menu/SettingsButton";
import DummyScreen from "../../screens/main/utilities/LandingScreen";

const MobileCheckInNavigator = createStackNavigator();
const CheckInTabNavigator = createMaterialTopTabNavigator();

export default () =>(
    <MobileCheckInNavigator.Navigator
        initialRouteName="PnrCheckin" //should be TabsNavigator
        headerMode={Platform.OS === 'ios' ? 'float' : 'screen'}
        screenOptions={{
            headerTintColor: ColorDefinitions.darkText.shade4,
            headerStyle: { backgroundColor: ColorDefinitions.colorPrimary },
        }}>

        {
            // pnr is temporary use tab navigator}
        }
        <MobileCheckInNavigator.Screen
            name="PnrCheckin"
            component={TicketCheckInScreen}
            options={({ navigation, route }) => ({
                headerTitle: 'Mobile Check-in',
                headerTitleStyle: {fontFamily: 'alpha-regular'},
                headerLeft: () => ( <MenuButton navigation={navigation}/> ),
                headerRight: () => ( <SettingsButton navigation={navigation}/> ),
            })}
        />

        <MobileCheckInNavigator.Screen
            name="TabsNavigator"
            component={TabsNavigator}
            options={({ navigation, route }) => ({
                headerTitle: 'Mobile Check-in',
                headerTitleStyle: {fontFamily: 'alpha-regular'},
                headerLeft: () => ( <MenuButton navigation={navigation}/> ),
                headerRight: () => ( <SettingsButton navigation={navigation}/> ),
            })}
        />

        <MobileCheckInNavigator.Screen
            name="Dummy"
            component={DummyScreen}
            options={({ navigation, route }) => ({
                headerTitle: 'Under Construction',
                headerTitleStyle: {fontFamily: 'alpha-regular'},
                headerRight: () => ( <SettingsButton navigation={navigation}/> ),
            })}
        />
    </MobileCheckInNavigator.Navigator>
);

const TabsNavigator = () => (
    <CheckInTabNavigator.Navigator
        lazy={true}
        initialRouteName="TicketCheckIn"
        headerMode="screen"
        tabBarOptions={{
            indicatorStyle: {borderColor: ColorDefinitions.darkBackgroundColor.shade1, borderBottomWidth: 2},
            labelStyle: {color: ColorDefinitions.darkText.shade4, fontFamily: 'alpha-regular'},
            style: { backgroundColor: ColorDefinitions.colorPrimary },
        }}>

        <CheckInTabNavigator.Screen
            name="SavedFlights"
            component={SavedFlightsScreen}
            options={({ navigation, route }) => ({
                title: 'My Flights',
                headerShown: true
            })}
        />

        <CheckInTabNavigator.Screen
            name="TicketCheckIn"
            component={TicketCheckInScreen}
            options={({ navigation, route }) => ({
                title: 'PNR OR E-Ticket',
                headerShown: true
            })}
        />
    </CheckInTabNavigator.Navigator>
);
