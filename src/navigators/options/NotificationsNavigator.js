import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import LandingScreen from "../../screens/main/notifications/LandingScreen";
import ColorDefinitions from "../../constants/ColorDefinitions";
import MenuButton from "../../components/menu/MenuButton";
import SettingsButton from "../../components/menu/SettingsButton";

const RentCarStack = createStackNavigator();

export default () => (
    <RentCarStack.Navigator
        initialRouteName="Landing"
        headerMode="screen"
        screenOptions={{
            headerTintColor: ColorDefinitions.darkText.shade4,
            headerStyle: { backgroundColor: ColorDefinitions.colorPrimary },
        }}>

        <RentCarStack.Screen
            name="Landing"
            component={LandingScreen}
            options={({ navigation, route }) => ({
                headerTitle: 'Received Messages',
                headerTitleStyle: {fontFamily: 'alpha-regular'},
                headerLeft: () => ( <MenuButton navigation={navigation}/> ),
                headerRight: () => ( <SettingsButton navigation={navigation}/> ),
            })}
        />
    </RentCarStack.Navigator>
)
