import React from 'react';
import {Platform} from "react-native";
import {createStackNavigator} from '@react-navigation/stack';
import LandingScreen from "../../screens/main/settings/LandingScreen";
import ColorDefinitions from "../../constants/ColorDefinitions";
import MenuButton from "../../components/menu/MenuButton";
import LocationScreen from "../../screens/main/settings/LocationScreen";
import AirportScreen from "../../screens/main/settings/AirportScreen";
import NotificationScreen from "../../screens/main/settings/NotificationScreen";
import LanguageScreen from "../../screens/main/settings/LanguageScreen";
import CurrencyScreen from "../../screens/main/settings/CurrencyScreen";
import PrivacyScreen from "../../screens/main/settings/PrivacyScreen";
import WeatherUnitsScreen from "../../screens/main/settings/WeatherUnitsScreen";
import SettingsButton from "../../components/menu/SettingsButton";

const SettingsStack = createStackNavigator();

export default () => (
    <SettingsStack.Navigator
        initialRouteName="Landing"
        headerMode={Platform.OS === 'ios' ? 'float' : 'screen'}
        screenOptions={{
            headerTintColor: ColorDefinitions.darkText.shade4,
            headerStyle: { backgroundColor: ColorDefinitions.colorPrimary },
        }}>

        <SettingsStack.Screen
            name="Landing"
            component={LandingScreen}
            options={({ navigation, route }) => ({
                headerTitle: 'Settings',
                headerTitleStyle: {fontFamily: 'alpha-regular'},
                headerLeft: () => ( <MenuButton navigation={navigation}/> ),
                headerRight: () => ( <SettingsButton navigation={navigation}/> ),
            })}
        />

        <SettingsStack.Screen
            name="LocationSettings"
            component={LocationScreen}
            options={({ navigation, route }) => ({
                headerTitle: 'Location Settings',
                headerTitleStyle: {fontFamily: 'alpha-regular'},
            })}
        />
        <SettingsStack.Screen
            name="AirportSettings"
            component={AirportScreen}
            options={({ navigation, route }) => ({
                headerTitle: 'Default Airport',
                headerTitleStyle: {fontFamily: 'alpha-regular'},
            })}
        />
        <SettingsStack.Screen
            name="NotificationsSettings"
            component={NotificationScreen}
            options={({ navigation, route }) => ({
                headerTitle: 'Notification Settings',
                headerTitleStyle: {fontFamily: 'alpha-regular'},
            })}
        />
        <SettingsStack.Screen
            name="LanguageSettings"
            component={LanguageScreen}
            options={({ navigation, route }) => ({
                headerTitle: 'Language Settings',
                headerTitleStyle: {fontFamily: 'alpha-regular'},
            })}
        />
        <SettingsStack.Screen
            name="CurrencySettings"
            component={CurrencyScreen}
            options={({ navigation, route }) => ({
                headerTitle: 'Currency Settings',
                headerTitleStyle: {fontFamily: 'alpha-regular'},
            })}
        />
        <SettingsStack.Screen
            name="PrivacySettings"
            component={PrivacyScreen}
            options={({ navigation, route }) => ({
                headerTitle: 'Privacy Settings',
                headerTitleStyle: {fontFamily: 'alpha-regular'},
            })}
        />
        <SettingsStack.Screen
            name="WeatherUnits"
            component={WeatherUnitsScreen}
            options={({ navigation, route }) => ({
                headerTitle: 'Measurement Units',
                headerTitleStyle: {fontFamily: 'alpha-regular'},
            })}
        />
    </SettingsStack.Navigator>
)
