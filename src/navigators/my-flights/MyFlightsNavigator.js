import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import FlightHistoryScreen from "../../screens/main/my-flights/FlightHistoryScreen";
import UpcomingFlightsScreen from "../../screens/main/my-flights/UpcomingFlightsScreen";
import ColorDefinitions from "../../constants/ColorDefinitions";
import MenuButton from "../../components/menu/MenuButton";
import SettingsButton from "../../components/menu/SettingsButton";
import {createMaterialTopTabNavigator} from "@react-navigation/material-top-tabs";

const MyFlightsStack = createStackNavigator();
const FlightsTabNavigator = createMaterialTopTabNavigator();

export default () => (
    <MyFlightsStack.Navigator
        initialRouteName="FlightsTabNavigator"
        headerMode="screen"
        screenOptions={{
            headerTintColor: ColorDefinitions.darkText.shade4,
            headerStyle: { backgroundColor: ColorDefinitions.colorPrimary },
        }}>

        <MyFlightsStack.Screen
            name="FlightsTabNavigator"
            component={TabsNavigator}
            options={({ navigation, route }) => ({
                headerTitle: 'My Flights',
                headerTitleStyle: {fontFamily: 'alpha-regular'},
                headerLeft: () => ( <MenuButton navigation={navigation}/> ),
                headerRight: () => ( <SettingsButton navigation={navigation}/> ),
            })}
        />
    </MyFlightsStack.Navigator>
);

const TabsNavigator = () => (
    <FlightsTabNavigator.Navigator
        lazy={true}
        initialRouteName="UpcomingFlights"
        headerMode="screen"
        tabBarOptions={{
            indicatorStyle: {borderColor: ColorDefinitions.darkBackgroundColor.shade1, borderBottomWidth: 2},
            labelStyle: {color: ColorDefinitions.darkText.shade4, fontFamily: 'alpha-regular'},
            style: { backgroundColor: ColorDefinitions.colorPrimary },
        }}>

        <FlightsTabNavigator.Screen
            name="UpcomingFlights"
            component={UpcomingFlightsScreen}
            options={({ navigation, route }) => ({
                title: 'Upcoming Flights',
                headerShown: true
            })}
        />

        <FlightsTabNavigator.Screen
            name="TravelHistory"
            component={FlightHistoryScreen}
            options={({ navigation, route }) => ({
                title: 'Travel History',
                headerShown: true
            })}
        />
    </FlightsTabNavigator.Navigator>
);
