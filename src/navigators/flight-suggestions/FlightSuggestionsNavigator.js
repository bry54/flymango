import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import ColorDefinitions from "../../constants/ColorDefinitions";
import MenuButton from "../../components/menu/MenuButton";
import SettingsButton from "../../components/menu/SettingsButton";
import DummyScreen from "../../screens/main/utilities/LandingScreen";
import SuggestionsScreen from "../../screens/main/flight-suggestions/SuggestionsScreen";

const HomeStack = createStackNavigator();

export default () => (
    <HomeStack.Navigator
        initialRouteName="FlightSuggestionsLanding"
        headerMode="screen"
        screenOptions={{
            headerTintColor: ColorDefinitions.darkText.shade4,
            headerStyle: { backgroundColor: ColorDefinitions.colorPrimary },
        }}>

        <HomeStack.Screen
            name="FlightSuggestionsLanding"
            component={SuggestionsScreen}
            options={({ navigation, route }) => ({
                headerTitle: 'Specials For You',
                headerTitleStyle: {fontFamily: 'alpha-regular'},
                headerLeft: () => ( <MenuButton navigation={navigation}/> ),
                headerRight: () => ( <SettingsButton navigation={navigation}/> ),
            })}
        />

        <HomeStack.Screen
            name="Dummy"
            component={DummyScreen}
            options={({ navigation, route }) => ({
                headerTitle: 'Marketing Information',
                headerTitleStyle: {fontFamily: 'alpha-regular'},
                headerRight: () => ( <SettingsButton navigation={navigation}/> ),
            })}
        />
    </HomeStack.Navigator>
)
