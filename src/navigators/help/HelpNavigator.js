import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import LandingScreen from "../../screens/main/help/LandingScreen";
import ColorDefinitions from "../../constants/ColorDefinitions";
import MenuButton from "../../components/menu/MenuButton";
import HelpWebViewScreen from "../../screens/main/help/HelpWebViewScreen";
import SettingsButton from "../../components/menu/SettingsButton";
import ContactInformationScreen from "../../screens/main/help/ContactInformationScreen";

const HelpStack = createStackNavigator();

export default () => (
    <HelpStack.Navigator
        initialRouteName="Landing"
        headerMode="screen"
        screenOptions={{
            headerTintColor: ColorDefinitions.darkText.shade4,
            headerStyle: { backgroundColor: ColorDefinitions.colorPrimary },
        }}>

        <HelpStack.Screen
            name="Landing"
            component={LandingScreen}
            options={({ navigation, route }) => ({
                headerTitle: 'Help',
                headerTitleStyle: {fontFamily: 'alpha-regular'},
                headerLeft: () => ( <MenuButton navigation={navigation}/> ),
                headerRight: () => ( <SettingsButton navigation={navigation}/> ),
            })}
        />

        <HelpStack.Screen
            name="ContactUs"
            component={ContactInformationScreen}
            options={({ navigation, route }) => ({
                headerTitle: 'Contact Us',
                headerTitleStyle: {fontFamily: 'alpha-regular'}
            })}
        />

        <HelpStack.Screen
            name="WebHelp"
            component={HelpWebViewScreen}
            options={({ navigation, route }) => ({
                headerTitle: 'Help',
                headerTitleStyle: {fontFamily: 'alpha-regular'}
            })}
        />
    </HelpStack.Navigator>
)
