import React, {Component} from 'react';
import {ScrollView, StyleSheet, Switch, Text, TouchableOpacity, View} from 'react-native';
import {connect} from "react-redux";
import TypoDefinitions from "../../constants/TypoDefinitions";
import ColorDefinitions from "../../constants/ColorDefinitions";
import {Button, Input} from "react-native-elements";
import moment from "moment";
import {Appearance} from "react-native-appearance";
import DateTimePickerModal from "react-native-modal-datetime-picker";
import {MaterialCommunityIcons} from "@expo/vector-icons";
import i18n from "../../localizations/i18n";

class SignUpScreen extends Component {
    state={
        email: '',
        phone: '',
        password: '',
        passwordConfirm: '',
        givenNames: '',
        surname: '',
        agreeTC: false,
        confirmCorrectness: false,
        dateOfBirth: new Date(),
        dobPickerVisible: false,
    };

    render () {
        const colorScheme = Appearance.getColorScheme();
        return (
            <ScrollView
                showsHorizontalScrollIndicator={false}
                showsVerticalScrollIndicator={false}
                style={styles.container}>

                <DateTimePickerModal
                    isDarkModeEnabled={colorScheme === 'dark'}
                    isVisible={this.state.dobPickerVisible}
                    mode="date"
                    onConfirm={async (date) => {
                        await this._updateInputControl('dateOfBirth', date);
                        this._updateInputControl('dobPickerVisible', false)
                    }}
                    onCancel={() => this._updateInputControl('dobPickerVisible', false)}
                />

                <View style={styles.signUpSection}>
                    <Text style={styles.sectionHeading}>{i18n.t('lbl_personal_information')}</Text>
                    <Input
                        rightIcon={<MaterialCommunityIcons name={'rename-box'} size={20} color={ColorDefinitions.accentColor.shade0}/>}
                        value={this.state.givenNames}
                        containerStyle={{ paddingTop: 5}}
                        inputContainerStyle={{borderColor: ColorDefinitions.accentColor.shade0, borderBottomWidth:2, paddingTop: 0}}
                        inputStyle={{fontFamily: 'alpha-regular'}}
                        label={i18n.t('lbl_first_name')}
                        labelStyle={{fontFamily: 'alpha-regular', fontWeight: TypoDefinitions.lightText, fontSize: TypoDefinitions.xSmallFont, color: ColorDefinitions.darkText.shade4}}
                        placeholder='John'
                        onChangeText={val => this._updateInputControl('givenNames', val)}
                    />

                    <Input
                        rightIcon={<MaterialCommunityIcons name={'rename-box'} size={20} color={ColorDefinitions.accentColor.shade0}/>}
                        value={this.state.surname}
                        containerStyle={{ paddingTop: 5}}
                        inputContainerStyle={{borderColor: ColorDefinitions.accentColor.shade0, borderBottomWidth:2}}
                        inputStyle={{fontFamily: 'alpha-regular'}}
                        label={i18n.t('lbl_surname')}
                        labelStyle={{fontFamily: 'alpha-regular', fontWeight: TypoDefinitions.lightText, fontSize: TypoDefinitions.xSmallFont, color: ColorDefinitions.darkText.shade4}}
                        placeholder='Doe'
                        onChangeText={val => this._updateInputControl('surname', val)}
                    />

                    <TouchableOpacity
                        activeOpacity={.9}
                        onPress={() => {this._updateInputControl('dobPickerVisible', true)}}>
                        <Input
                            rightIcon={<MaterialCommunityIcons name={'calendar'} size={20} color={ColorDefinitions.accentColor.shade0}/>}
                            editable={false}
                            value={ moment(this.state.dateOfBirth).format('DD.MM.YYYY') }
                            containerStyle={{ paddingTop: 30}}
                            inputContainerStyle={{borderColor: ColorDefinitions.accentColor.shade0, borderBottomWidth:2}}
                            label={i18n.t('lbl_date_of_birth')}
                            labelStyle={{fontFamily: 'alpha-regular', fontWeight: TypoDefinitions.lightText, fontSize: TypoDefinitions.xSmallFont, color: ColorDefinitions.darkText.shade4}}
                            inputStyle={{fontFamily: 'alpha-regular'}}
                            placeholder='dd.mm.yyyy'
                        />
                    </TouchableOpacity>
                </View>

                <View style={styles.signUpSection}>
                    <Text style={styles.sectionHeading}>{i18n.t('lbl_contact_information')}</Text>
                    <Input
                        rightIcon={<MaterialCommunityIcons name={'email'} size={20} color={ColorDefinitions.accentColor.shade0}/>}
                        value={this.state.email}
                        keyboardType='email-address'
                        containerStyle={{ paddingTop: 5}}
                        inputContainerStyle={{borderColor: ColorDefinitions.accentColor.shade0, borderBottomWidth:2}}
                        inputStyle={{fontFamily: 'alpha-regular'}}
                        label={i18n.t('lbl_email_address')}
                        labelStyle={{fontFamily: 'alpha-regular', fontWeight: TypoDefinitions.lightText, fontSize: TypoDefinitions.xSmallFont, color: ColorDefinitions.darkText.shade4}}
                        placeholder='jogndoe@email.com'
                        onChangeText={val => this._updateInputControl('email', val)}
                    />

                    <Input
                        rightIcon={<MaterialCommunityIcons name={'cellphone'} size={20} color={ColorDefinitions.accentColor.shade0}/>}
                        value={this.state.phone}
                        keyboardType='phone-pad'
                        containerStyle={{ paddingTop: 5}}
                        inputContainerStyle={{borderColor: ColorDefinitions.accentColor.shade0, borderBottomWidth:2}}
                        inputStyle={{fontFamily: 'alpha-regular'}}
                        label={i18n.t('lbl_phone')}
                        labelStyle={{fontFamily: 'alpha-regular', fontWeight: TypoDefinitions.lightText, fontSize: TypoDefinitions.xSmallFont, color: ColorDefinitions.darkText.shade4}}
                        placeholder='+## #### ### ###'
                        onChangeText={val => this._updateInputControl('phone', val)}
                    />
                </View>

                <View style={styles.signUpSection}>
                    <Text style={styles.sectionHeading}>{i18n.t('lbl_security_information')}</Text>
                    <Input
                        autoCapitalize='none'
                        value={this.state.password}
                        containerStyle={{ paddingTop: 5}}
                        inputContainerStyle={{borderColor: ColorDefinitions.accentColor.shade0, borderBottomWidth:2}}
                        secureTextEntry={true}
                        label={i18n.t('lbl_password')}
                        labelStyle={{fontFamily: 'alpha-regular', fontWeight: TypoDefinitions.lightText, fontSize: TypoDefinitions.xSmallFont, color: ColorDefinitions.darkText.shade4}}
                        placeholder={i18n.t('lbl_password')}
                        onChangeText={val => this._updateInputControl('password', val)}
                    />

                    <Input
                        autoCapitalize='none'
                        value={this.state.passwordConfirm}
                        containerStyle={{ paddingTop: 5}}
                        inputContainerStyle={{borderColor: ColorDefinitions.accentColor.shade0, borderBottomWidth:2}}
                        secureTextEntry={true}
                        label={i18n.t('lbl_password_confirm')}
                        labelStyle={{fontFamily: 'alpha-regular', fontWeight: TypoDefinitions.lightText, fontSize: TypoDefinitions.xSmallFont, color: ColorDefinitions.darkText.shade4}}
                        placeholder={i18n.t('lbl_password_confirm')}
                        onChangeText={val => this._updateInputControl('passwordConfirm', val)}
                    />

                    <View style={{marginHorizontal:10, marginVertical: 10}}>
                        <View style={{flexDirection: 'row', justifyContent: 'space-between', marginVertical: 5}}>
                            <Text style={{paddingVertical: 2, fontFamily: 'alpha-regular', color: ColorDefinitions.darkText.shade4}}>
                                {i18n.t('lbl_agree_tc')}
                            </Text>
                            <Switch
                                value={this.state.agreeTC}
                                onValueChange = {(val) =>{this._updateInputControl('agreeTC', val)}}/>
                        </View>

                        <View style={{flexDirection: 'row', justifyContent: 'space-between', marginVertical: 5}}>
                            <Text style={{paddingVertical: 2, fontFamily: 'alpha-regular', color: ColorDefinitions.darkText.shade4}}>
                                {i18n.t('lbl_info_correct')}
                            </Text>
                            <Switch
                                value={this.state.confirmCorrectness}
                                onValueChange = {(val) =>{this._updateInputControl('confirmCorrectness', val)}}/>
                        </View>
                    </View>

                    <Button
                        onPress={()=>this._attemptSignUp()}
                        containerStyle={{margin: 10}}
                        buttonStyle={{backgroundColor: ColorDefinitions.accentColor.shade0, borderRadius: 0}}
                        titleStyle={{textTransform: 'capitalize', fontFamily: 'alpha-regular', marginVertical:5, color: ColorDefinitions.darkText.shade4}}
                        title={i18n.t('btn_sign_up')}
                        raised
                        disabled = {
                            !this.state.email||
                            !this.state.password||
                            !this.state.givenNames||
                            !this.state.surname||
                            !this.state.phone||
                            !this.state.dateOfBirth||
                            !this.state.passwordConfirm||
                            !this.state.agreeTC||
                            !this.state.confirmCorrectness||
                            (this.state.password !== this.state.passwordConfirm)
                        }/>
                </View>
            </ScrollView>
        )
    }

    _updateInputControl = (inputKey, inputValue) => {
        this.setState({
            ...this.state,
            [inputKey] : inputValue
        });
    };

    _attemptSignUp = () => {
        const {navigation} = this.props;
        navigation.navigate('SignIn');
    };
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'transparent'
    },

    signUpSection:{
        marginBottom: 20
    },

    sectionHeading:{
        backgroundColor: ColorDefinitions.darkBackgroundColor.shade4,
        fontFamily: 'alpha-regular',
        fontSize: TypoDefinitions.xxBigFont,
        paddingVertical: 20,
        paddingHorizontal: 10,
        color: ColorDefinitions.lightText.shade0
    }
});

const mapStateToProps = (state) => {
    return {

    }
};

const matchDispatchToProps = dispatch => {
    return {

    }
};

export default connect(mapStateToProps, matchDispatchToProps)(SignUpScreen);
