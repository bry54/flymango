import React, {Component} from 'react';
import {ScrollView, StyleSheet, Text, View} from 'react-native';
import {connect} from "react-redux";
import {Button, Input} from "react-native-elements";
import TypoDefinitions from "../../constants/TypoDefinitions";
import {attemptSignIn} from "../../store/utilities/actionsCollection";
import {Dropdown} from "react-native-material-dropdown";
import {SCLAlert, SCLAlertButton} from "react-native-scl-alert";
import SnackBar from 'react-native-snackbar-component'
import ColorDefinitions from "../../constants/ColorDefinitions";
import {MaterialCommunityIcons} from "@expo/vector-icons";
import Ripple from "react-native-material-ripple";
import i18n from '../../localizations/i18n'

class SignInScreen extends Component {

    state = {
        showPassword: false,
        showDemoInfo: true,
        showLoginError: false,
        signInMods: [{label: i18n.t('lbl_email_address'), value: 'email'}, {label: i18n.t('lbl_membership_number'), value: 'membership'}],
        signInMod: 'membership',
        membershipNumber: '',
        email: '',
        password: ''
    };

    render () {
        return (
            <ScrollView contentContainerStyle={{flex: 1}}>
                <SnackBar
                    backgroundColor='rgba( 14,  6, 50, .8)'
                    messageStyle={{ }}
                    actionStyle={{ }}
                    visible={this.state.showDemoInfo}
                    textMessage={`DEMO CREDENTIALS ${'\n'}${'\n'}email          : samkuzah@muzukuru.com ${'\n'}member # : SK-911702 ${'\n'}password  : secret`}
                    actionHandler={()=>{this._updateInputControl('showDemoInfo', false)}}
                    actionText="Dismiss"/>

                <SCLAlert
                    overlayStyle={{backgroundColor: ColorDefinitions.darkBackgroundColor.rgbaShade5}}
                    show={this.state.showLoginError}
                    onRequestClose={() => this._updateInputControl('showLoginError', false)}
                    theme="danger"
                    title="Login Error"
                    titleStyle={{ fontFamily: 'alpha-regular' }}
                    subtitle={'Login credentials you entered do not match any account.'}
                    subtitleStyle={{ fontFamily: 'alpha-regular' }}>

                    <SCLAlertButton
                        textStyle={{ fontFamily: 'alpha-regular', fontWeight: '400' }}
                        theme="info"
                        onPress={() =>
                            this._updateInputControl('showLoginError', false)
                        }>
                        Try Again
                    </SCLAlertButton>

                    <SCLAlertButton
                        textStyle={{ fontFamily: 'alpha-regular', fontWeight: '400' }}
                        theme="success"
                        onPress={() =>{
                            const {navigation} = this.props;
                            navigation.navigate('Main', { screen: 'Home'});
                            this._updateInputControl('showLoginError', false);
                        }}>
                        Continue as guest
                    </SCLAlertButton>
                </SCLAlert>

                <View style={styles.container}>
                    <View style={{marginTop: 10, marginHorizontal: 10}}>
                        <Dropdown
                            value={this.state.signInMod}
                            inputContainerStyle={{borderBottomWidth: 2}}
                            onChangeText={(value) => this._updateInputControl('signInMod', value)}
                            style={{fontSize: 18, fontFamily: 'alpha-regular', color: ColorDefinitions.darkText.shade4}}
                            labelFontSize={14}
                            itemTextStyle={{ fontFamily: 'alpha-regular' }}
                            baseColor={ColorDefinitions.accentColor.shade0}
                            label={<Text style={{fontFamily: 'alpha-regular', color: ColorDefinitions.darkText.shade4}}>{i18n.t('lbl_sign_in_method')}</Text>}
                            data={ this.state.signInMods }
                            valueExtractor={(item) => item.value}
                            labelExtractor={(item) => item.label} />
                    </View>

                    <View style={styles.idHolder}>
                        {this.state.signInMod === 'membership' ? (
                            <Input
                                rightIcon={<MaterialCommunityIcons name={'passport'} size={20} color={ColorDefinitions.accentColor.shade0}/>}
                                autoCapitalize='none'
                                value={this.state.membershipNumber}
                                inputContainerStyle={{borderColor: ColorDefinitions.accentColor.shade0, borderBottomWidth:2}}
                                inputStyle={{fontFamily: 'alpha-regular'}}
                                label={i18n.t('lbl_membership_number')}
                                labelStyle={{fontFamily: 'alpha-regular', fontWeight: TypoDefinitions.lightText, fontSize: TypoDefinitions.xSmallFont, color: ColorDefinitions.darkText.shade4}}
                                placeholder='XX-######'
                                onChangeText={val => this._updateInputControl('membershipNumber', val)}
                            />
                        ) : (
                            <Input
                                rightIcon={<MaterialCommunityIcons name={'email'} size={20} color={ColorDefinitions.accentColor.shade0}/>}
                                autoCapitalize='none'
                                keyboardType='email-address'
                                value={this.state.email}
                                inputContainerStyle={{borderColor: ColorDefinitions.accentColor.shade0, borderBottomWidth:2}}
                                inputStyle={{fontFamily: 'alpha-regular'}}
                                label={i18n.t('lbl_email_address')}
                                labelStyle={{fontFamily: 'alpha-regular', fontWeight: TypoDefinitions.lightText, fontSize: TypoDefinitions.xSmallFont, color: ColorDefinitions.darkText.shade4}}
                                placeholder='johndoe@emaildomain.com'
                                onChangeText={val => this._updateInputControl('email', val)}
                            />)}
                    </View>

                    <View style={styles.passwordHolder}>
                        <Input
                            autoCapitalize='none'
                            value={this.state.password}
                            inputContainerStyle={{borderColor: ColorDefinitions.accentColor.shade0, borderBottomWidth:2}}
                            secureTextEntry={this.state.showPassword ? false : true}
                            label={i18n.t('lbl_password')}
                            labelStyle={{fontFamily: 'alpha-regular', fontWeight: TypoDefinitions.lightText, fontSize: TypoDefinitions.xSmallFont, color: ColorDefinitions.darkText.shade4}}
                            placeholder={i18n.t('lbl_password')}
                            onChangeText={val => this._updateInputControl('password', val)}
                            rightIcon={{
                                onPress: ()=> this._updateInputControl('showPassword', !this.state.showPassword),
                                type: 'ionicon',
                                size: 20,
                                name: this.state.showPassword ? 'ios-eye-off' : 'ios-eye',
                                color: ColorDefinitions.accentColor.shade0
                            }}
                        />
                    </View>

                    <View style={styles.actionsContainer}>
                        {
                            ( (this.state.signInMod === 'membership' && !this.state.membershipNumber) ) ||
                            ( (this.state.signInMod === 'EMAIL' && !this.state.email) || !this.state.password) ? (
                                <Button
                                    containerStyle={styles.optionButton}
                                    buttonStyle={{backgroundColor: ColorDefinitions.accentColor.shade0, borderRadius: 0}}
                                    titleStyle={{textTransform: 'capitalize', color: ColorDefinitions.darkText.shade4, fontFamily: 'alpha-regular', marginVertical:5}}
                                    title={i18n.t('btn_sign_in')}
                                    disabled={ true }/>
                            ) : (
                                <View style={{elevation: 5, marginVertical: 10, backgroundColor: ColorDefinitions.accentColor.shade0}}>
                                    <Ripple rippleOpacity={.8} onPress={() => this._attemptSignIn()}>
                                        <Text style={{textTransform: 'uppercase', color: ColorDefinitions.darkText.shade4,fontFamily: 'alpha-regular',fontSize: 16,paddingVertical: 16,textAlign: 'center'}}>
                                            {i18n.t('btn_sign_in')}
                                        </Text>
                                    </Ripple>
                                </View>
                            )}

                        <Button
                            onPress={() => {this._navToForgotPassword()}}
                            containerStyle={styles.optionButton}
                            buttonStyle={styles.buttonStyle}
                            titleStyle={{textDecorationLine: 'underline',fontFamily: 'alpha-regular'}}
                            title={i18n.t('btn_forgot_password')}
                            type="clear"/>

                        <View style={{flexDirection: 'row', marginVertical: 15}}>
                            <View style={{backgroundColor: 'gray', height: 1, flex: 1, alignSelf: 'center'}} />
                            <Text style={{ alignSelf:'center', paddingHorizontal:5, fontFamily: 'alpha-regular' }}>
                                {i18n.t('lbl_or')}
                            </Text>
                            <View style={{backgroundColor: 'grey', height: 1, flex: 1, alignSelf: 'center'}} />
                        </View>

                        <View style={{elevation: this.state.showDemoInfo ? 0 : 5, marginVertical: 10, backgroundColor: ColorDefinitions.darkBackgroundColor.shade4}}>
                            <Ripple rippleOpacity={.8} onPress={() => this._navToSignUp()}>
                                <Text style={{textTransform: 'uppercase', color: ColorDefinitions.lightText.shade0,fontFamily: 'alpha-regular',fontSize: 16,paddingVertical: 14,textAlign: 'center'}}>
                                    {i18n.t('btn_sign_up')}
                                </Text>
                            </Ripple>
                        </View>
                    </View>
                </View>
            </ScrollView>
        )
    }

    _navToForgotPassword = ( ) =>{
        const {navigation} = this.props;
        navigation.navigate('ForgotPassword')
    };

    _updateInputControl = (inputKey, inputValue) => {
        this.setState({
            ...this.state,
            [inputKey] : inputValue
        });
    };

    _attemptSignIn = async () =>{
        const {navigation} = this.props;
        const credentials = {
            signInMod: this.state.signInMod,
            membershipNumber: this.state.membershipNumber.replace(/\s+/g, ''),
            email: this.state.email.replace(/\s+/g, ''),
            password: this.state.password,
        };

        await this.props.attemptSignIn(credentials);

        if (this.props.user)
            navigation.navigate('Main', { screen: 'Home'});
        else{
            this._updateInputControl('showDemoInfo', true);
            this._updateInputControl('showLoginError', true);
        }
    };

    _navToSignUp = () => {
        const {navigation} = this.props;
        navigation.navigate('Auth', { screen: 'SignUp'})
    };
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginHorizontal: 10,
    },

    pickerHolder: {
        borderBottomColor: ColorDefinitions.accentColor.shade0,
        borderBottomWidth: 2,
        overflow: 'hidden'
    },

    idHolder:{
        paddingTop: 20,
        paddingBottom: 10,
    },

    passwordHolder:{

    },

    actionsContainer:{
        marginHorizontal: 10,
        marginTop: 40,
    },

    optionButton:{
        marginVertical: 10,
    },
});

const mapStateToProps = (state) => {
    return {
        user: state.authReducer.loggedInUser
    }
};

const matchDispatchToProps = dispatch => {
    return {
        attemptSignIn: (credentials) => dispatch(attemptSignIn(credentials))
    }
};

export default connect(mapStateToProps, matchDispatchToProps)(SignInScreen);
