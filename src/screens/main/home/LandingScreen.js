import React, {Component} from 'react';
import {BackHandler, ImageBackground, StyleSheet, Text, TouchableWithoutFeedback, View} from 'react-native';
import ColorDefinitions from "../../../constants/ColorDefinitions";
import {connect} from "react-redux";
import AppIntroSlider from "react-native-app-intro-slider";
import {Divider, Icon, ListItem, PricingCard} from "react-native-elements";
import {Ionicons} from '@expo/vector-icons';
import Ripple from "react-native-material-ripple";
import moment from "moment";
import i18n from "../../../localizations/i18n";

class LandingScreen extends Component {

    state={
        exitApp: false,
        snackVisible: false
    };

    constructor(props) {
        super(props);
        this._backHandler = this.backAction.bind(this);
    }

    componentDidMount() {
        this._unsubscribe = this.props.navigation.addListener('focus', () => {
            this.updateInputItem('exitApp',false);
            BackHandler.addEventListener("hardwareBackPress", this._backHandler);
        });
    }

    componentWillUnmount() {
        BackHandler.removeEventListener( "hardwareBackPress", this._backHandler);
        this._unsubscribe();
    }

    backAction = () => {
        if (this.props.navigation.isFocused()) {
            BackHandler.exitApp();
            return true;
        }
    };

    updateInputItem = (itemKey, itemValue) =>{
        this.setState({
            ...this.state,
            [itemKey] : itemValue
        })
    };

    render () {
        const user = this.props.user;
        const showFlights = !!user;
        const guestTitle = i18n.t('lbl_guest');
        return (
            <View style={styles.container}>
                <View style={{backgroundColor: ColorDefinitions.darkBackgroundColor.shade4, elevation: 5}}>
                    <Ripple rippleOpacity={.8} onPress={() => this._onHeaderPress()}>
                        <ListItem
                            containerStyle={{backgroundColor: ColorDefinitions.darkBackgroundColor.shade4, padding: 10}}
                            title={user ? i18n.t('lbl_greeting', { user: user.name.first }) : i18n.t('lbl_greeting', { user: guestTitle })}
                            subtitle={this._getSubTitle()}
                            titleStyle={{
                                fontFamily: 'alpha-regular',
                                fontSize: 22,
                                color: ColorDefinitions.lightText.shade0,
                                padding: 0,
                                borderRightColor: ColorDefinitions.lightText.shade0,
                                borderRightWidth: 1,
                                textTransform: 'capitalize'
                            }}
                            chevron={() => (<Ionicons name="ios-arrow-forward" size={26} color={ColorDefinitions.lightText.shade0}/>)}
                        />
                    </Ripple>
                    <Divider/>
                    <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                        <View style={{width: '40%', elevation: 0, backgroundColor: ColorDefinitions.darkBackgroundColor.shade4, borderRadius: 0}}>
                            <Ripple rippleOpacity={.8} onPress={() => this._navToMobileCheckIn()}>
                                <Text style={{paddingVertical: 17, textTransform: 'uppercase', color: ColorDefinitions.lightText.shade0, fontFamily: 'alpha-regular',fontSize: 15,textAlign: 'center'}}>
                                    {i18n.t('btn_mobile_check_in')}
                                </Text>
                            </Ripple>
                        </View>

                        <Icon
                            reverse
                            raised
                            size={16}
                            name='lightbulb-on'
                            type='material-community'
                            reverseColor={'teal'}
                            color={ColorDefinitions.accentColor.shade0}
                            onPress={() => this._navToFlightSuggestions()} />

                        {user && <View style={{width: '40%', elevation: 0, backgroundColor: ColorDefinitions.darkBackgroundColor.shade4, borderRadius: 0}}>
                            <Ripple rippleOpacity={.8} onPress={() => this._navToBuyTicket()}>
                                <Text style={{paddingVertical: 17, textTransform: 'uppercase', color: ColorDefinitions.lightText.shade0, fontFamily: 'alpha-regular',fontSize: 15,textAlign: 'center'}}>
                                    {i18n.t('btn_search_flights')}
                                </Text>
                            </Ripple>
                        </View> }
                    </View>
                </View>
                <AppIntroSlider
                    showPrevButton={false}
                    showNextButton={false}
                    showDoneButton={false}
                    dotStyle={{backgroundColor: 'gray'}}
                    activeDotStyle={{backgroundColor: ColorDefinitions.accentColor.shade0}}
                    renderItem={showFlights ? this._renderSpecialFlightItem : this._renderShowcaseItem}
                    slides={showFlights ? i18n.t('special_flights') : i18n.t('showcase_slides') }
                    onDone={() => {}}/>
            </View>
        );
    }

    _renderSpecialFlightItem = ({ item }) => {
        return (
            <TouchableWithoutFeedback onPress={()=> this._navToSpecialsDetail(item) }>
                <ImageBackground source={item.image} style={styles.specialsContainer}>
                    <View style={{marginBottom: 50}}>
                        <PricingCard
                            onButtonPress={() => this._navToSpecialsDetail()}
                            containerStyle={{borderWidth: 0, backgroundColor: 'rgba(31, 19, 78, .8)' }}
                            color={ColorDefinitions.accentColor.shade0}
                            title={item.title}
                            titleStyle={styles.title}
                            price={i18n.t('lbl_starting_at',{ price: item.fee})}
                            pricingStyle={styles.fee}
                            info={[item.text]}
                            infoStyle={styles.text}
                            button={{
                                title: i18n.t('btn_search_now'),
                                titleStyle: { fontFamily: 'alpha-regular', color: ColorDefinitions.darkText.shade4, textTransform: 'capitalize'},
                            }}
                        />
                    </View>
                </ImageBackground>
            </TouchableWithoutFeedback>
        );
    };

    _renderShowcaseItem = ({ item }) => {
        return (
            <ImageBackground source={item.image} style={styles.specialsContainer}>
                <View style={{marginBottom: 50}}>
                    <PricingCard
                        onButtonPress={() => this._navToSpecialsDetail()}
                        containerStyle={{borderWidth: 0, backgroundColor: 'rgba(31, 19, 78, 0.8)' }}
                        color={ColorDefinitions.accentColor.shade0}
                        title={item.title}
                        titleStyle={styles.title}
                        price={null}
                        pricingStyle={styles.fee}
                        info={[item.text]}
                        infoStyle={styles.text}
                        button={{
                            title: i18n.t('btn_read_more'),
                            titleStyle: { fontFamily: 'alpha-regular', color: ColorDefinitions.darkText.shade4, textTransform: 'capitalize'},
                            buttonStyle:{ backgroundColor: 'red'}
                        }}
                    />
                </View>
            </ImageBackground>
        );
    };

    _onHeaderPress = () => {
        const user = this.props.user;
        if (user) {
            if (this.props.flights.length)
                this._navToMyFlights();
            else
                this._navToBuyTicket()
        } else
            this._navToSignUp();
    };

    _getSubTitle = () => {
        const user = this.props.user;
        const flights = this._getFlights();
        if (user) {
            if (flights.length)
                return (
                    <View style={{borderRightColor: ColorDefinitions.lightText.shade0, borderRightWidth: 1, paddingRight:10, paddingVertical: 3}}>
                        <Text style={{fontFamily: 'alpha-regular', fontSize: 15, color: ColorDefinitions.lightText.shade0,paddingVertical: 3}}>
                            {i18n.t('lbl_next_flight_destination', {destination: flights[0].flight.arrival.airport.city.name}) }
                        </Text>
                        <Text style={{fontFamily: 'alpha-regular', fontSize: 15, color: ColorDefinitions.lightText.shade0,paddingVertical: 3}}>
                            { i18n.t('lbl_next_flight_date', {date: moment(flights[0].flight.departure.scheduled_time).format('ddd, DD MMM YYYY')})}
                        </Text>
                        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                            <Text style={{fontFamily: 'alpha-regular', fontSize: 15, color: ColorDefinitions.lightText.shade0}}>
                                { i18n.t('lbl_next_flight_boarding', {time: moment(flights[0].flight.departure.scheduled_time).subtract('30', 'minutes').format('HH:mm')})}
                            </Text>

                            <Text style={{fontFamily: 'alpha-regular', fontSize: 15, color: ColorDefinitions.lightText.shade0}}>
                                { i18n.t('lbl_next_flight_departure', {time: moment(flights[0].flight.departure.scheduled_time).format('HH:mm')})}
                            </Text>
                        </View>
                    </View>
                );
            else
                return (
                    <View style={{borderRightColor: ColorDefinitions.lightText.shade0, borderRightWidth: 1, paddingTop:5}}>
                        <Text style={{fontFamily: 'alpha-regular', fontSize: 14, color: ColorDefinitions.lightText.shade0,padding: 3}}>
                            {i18n.t('string_buy_from_recent')}
                        </Text>
                    </View>
                );
        } else
            return (
                <View style={{borderRightColor: ColorDefinitions.lightText.shade0, borderRightWidth: 1, paddingTop:5}}>
                    <Text style={{fontFamily: 'alpha-regular', fontSize: 14, color: ColorDefinitions.lightText.shade0,padding: 3}}>
                        {i18n.t('string_sign_in_register')}
                    </Text>
                </View>
            )
    };

    _navToBuyTicket = () =>{
        const {navigation} = this.props;
        navigation.navigate('BuyTicket');
    };

    _navToMyFlights = () =>{
        const {navigation} = this.props;
        navigation.navigate('MyFlights');
    };

    _navToMobileCheckIn = () =>{
        const {navigation} = this.props;
        navigation.navigate('MobileCheckIn');
    };

    _navToSpecialsDetail = (item=null) =>{
        const {navigation} = this.props;
        navigation.navigate("BuyTicket")
    };

    _navToFlightSuggestions =() => {
        const {navigation} = this.props;
        navigation.navigate('SuggestedDestinations');
    };

    _navToSignUp =() => {
        const {navigation} = this.props;
        navigation.navigate('Auth', { screen: 'StartOptions'})
    };

    _getFlights = () =>{
        const myFlights = this.props.flights;
        return myFlights.futureFlights;
    };
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: ColorDefinitions.mainBackground,
    },

    specialsContainer: {
        height: '100%',
        justifyContent: 'flex-end',
        paddingHorizontal: 16,
    },

    fee: {fontWeight: 'normal',
        color: 'rgba(255, 255, 255, 1.0)',
        backgroundColor: 'transparent',
        textAlign: 'left',
        fontSize: 20,
        fontFamily: 'alpha-regular',
        marginBottom: 5,
    },

    text: {
        fontWeight: 'normal',
        color: 'rgba(255, 255, 255, 1.0)',
        backgroundColor: 'transparent',
        textAlign: 'left',
        fontSize: 18,
        fontFamily: 'alpha-regular',
        marginBottom: 10,
    },

    title: {
        fontWeight: 'normal',
        color: 'rgba(255, 255, 255, 1.0)',
        fontSize: 26,
        backgroundColor: 'transparent',
        textAlign: 'center',
        marginBottom: 16,
        fontFamily: 'alpha-regular'
    },
});

const mapStateToProps = (state) => {
    return {
        user: state.authReducer.loggedInUser,
        flights: state.buyTicketsReducer.flattenedTickets
    }
};

const matchDispatchToProps = dispatch => {
    return {

    }
};

export default connect(mapStateToProps, matchDispatchToProps)(LandingScreen);
