import React, {Component} from 'react';
import {ScrollView, StyleSheet, Text, View} from 'react-native';
import ColorDefinitions from "../../../constants/ColorDefinitions";
import {connect} from "react-redux";
import SectionHeader from "../../../components/mango-miles/SectionHeader";
import {Divider, ListItem} from "react-native-elements";
import Ripple from "react-native-material-ripple";

class LandingScreen extends Component {

    render () {
        const mangoMilesSections = [
            {title: 'Mango Miles Classes', subTitle: 'Check out Mango Miles classes.'},
            {title: 'Mango Miles Statement', subTitle: 'Read the Mango Miles Official Statement of Purpose.'},
            {title: 'Apply MangoMiles Card', subTitle: 'Apply for a Mango Miles Card.'},
            {title: 'You & Friends', subTitle: 'See how you compare with your friends who fly Mango.'},
        ];

        return (
            <ScrollView
                showsHorizontalScrollIndicator={false}
                showsVerticalScrollIndicator={false}>
                <SectionHeader
                    sectionTitle = 'Mango Miles'
                    sectionDescription ='Fly with Mango and accumulate miles for your MangoMiles account. You can win award tickets with your Miles on Mango Airlines and partner airlines, get extra legroom seats and baggage allowance.'/>

                <View style={{elevation:5,paddingHorizontal: 10, paddingVertical: 15, backgroundColor: ColorDefinitions.lightBackgroundColor.shade0, flexDirection: 'row', justifyContent: 'space-between'}}>
                    <View>
                        <Text style={{textAlign: 'left', fontFamily: 'alpha-regular', paddingVertical: 2, fontSize: 15, color: ColorDefinitions.darkText.shade4}}>TOTAL</Text>
                        <Text style={{textAlign: 'left', fontFamily: 'alpha-regular', paddingVertical: 2, fontSize: 15, color: ColorDefinitions.darkText.shade4}}>MILES</Text>
                    </View>

                    <View>
                        <Text style={{fontFamily: 'numeric-regular', paddingVertical: 2, fontSize: 38, color: ColorDefinitions.darkText.shade4}}>1000</Text>
                    </View>
                </View>

                <Divider style={{backgroundColor: ColorDefinitions.lightBackgroundColor.shade0}}/>

                <View style={{elevation:5, paddingHorizontal: 10, paddingVertical: 15, marginBottom: 0, backgroundColor: ColorDefinitions.lightBackgroundColor.shade0}}>
                    <Text style={{textAlign: 'center', fontFamily: 'numeric-regular',fontSize: 15}}>
                        Your 10000mi will expire on 30-12-2020
                    </Text>
                </View>
                {
                    mangoMilesSections.map((item, index) => (
                        <Ripple key={index} rippleOpacity={.8} onPress={() => this._navToSection()}>
                        <ListItem
                            title={item.title}
                            titleStyle={{fontSize: 17,fontFamily: 'alpha-regular', color: ColorDefinitions.darkText.shade4}}
                            subtitle={item.subTitle}
                            subtitleStyle={{fontFamily: 'alpha-regular'}}
                            bottomDivider
                            chevron/>
                        </Ripple>
                    ))
                }
            </ScrollView>
        );
    }

    _navToSection = (section=null) => {
        const {navigation} = this.props;
        navigation.navigate('Dummy')
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: ColorDefinitions.mainBackground,
    },
});

const mapStateToProps = (state) => {
    return {

    }
};

const matchDispatchToProps = dispatch => {
    return {

    }
};

export default connect(mapStateToProps, matchDispatchToProps)(LandingScreen);
