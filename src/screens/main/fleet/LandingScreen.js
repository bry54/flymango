import React, {Component} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import ColorDefinitions from "../../../constants/ColorDefinitions";
import {connect} from "react-redux";
import {aircrafts} from "../../../utilities/MockUpData";
import AppIntroSlider from "react-native-app-intro-slider";
import {Tile} from "react-native-elements";
import {MaterialCommunityIcons, MaterialIcons} from "@expo/vector-icons";

class LandingScreen extends Component {

    render () {
        return (
            <AppIntroSlider
                showPrevButton={false}
                showNextButton={false}
                showDoneButton={false}
                dotStyle={{backgroundColor: 'gray'}}
                activeDotStyle={{backgroundColor: ColorDefinitions.accentColor.shade0}}
                renderItem={ this._renderShowcaseItem }
                slides={ aircrafts }
                onDone={() => {}}/>
        );
    }

    _renderShowcaseItem = ({ item }) => {
        return (
            <Tile
                title={
                    <Text style={{fontFamily: 'alpha-regular', fontWeight: 'normal'}}>
                        {`${item.name.en} ${item.aircraft_code} ${item.airline_equipment_code}`}
                    </Text>
                }
                containerStyle={{height: '90%'}}
                imageSrc={item.image}>

                <View>
                    <View style={{paddingTop: 10, flexDirection: 'row', justifyContent: 'space-between'}}>
                        <View>
                            <Text style={{fontFamily: 'alpha-regular', paddingVertical: 8, color:ColorDefinitions.accentColor.shade0}}>Length</Text>
                            <View style={{flexDirection: 'row', justifyContent: 'flex-start'}}>
                                <MaterialCommunityIcons name={'alpha-l-box'} size={20} color={ColorDefinitions.darkPrimaryColor.shade5}/>
                                <Text style={{fontFamily: 'alpha-regular', paddingHorizontal: 5, paddingVertical: 2, color: ColorDefinitions.darkPrimaryColor.shade5}}>{item.specs.length}</Text>
                            </View>
                        </View>

                        <View style={{borderRightWidth: .5}}/>

                        <View>
                            <Text style={{fontFamily: 'alpha-regular', paddingVertical: 8, color:ColorDefinitions.accentColor.shade0}}>Width</Text>
                            <View style={{flexDirection: 'row', justifyContent: 'flex-start'}}>
                                <MaterialCommunityIcons name={'alpha-w-box'} size={20} color={ColorDefinitions.darkPrimaryColor.shade5}/>
                                <Text style={{fontFamily: 'alpha-regular', paddingHorizontal: 5, paddingVertical: 2, color: ColorDefinitions.darkPrimaryColor.shade5}}>{item.specs.width}</Text>
                            </View>
                        </View>

                        <View style={{borderRightWidth: .5}}/>

                        <View>
                            <Text style={{fontFamily: 'alpha-regular', paddingVertical: 8, color:ColorDefinitions.accentColor.shade0}}>Seat Width</Text>
                            <View style={{flexDirection: 'row', justifyContent: 'flex-start'}}>
                                <MaterialIcons name={'event-seat'} size={20} color={ColorDefinitions.darkPrimaryColor.shade5}/>
                                <Text style={{fontFamily: 'alpha-regular', paddingHorizontal: 5, paddingVertical: 2, color: ColorDefinitions.darkPrimaryColor.shade5}}>{item.specs.seat_width}</Text>
                            </View>
                        </View>

                        <View style={{borderRightWidth: .5}}/>

                        <View>
                            <Text style={{fontFamily: 'alpha-regular', paddingVertical: 8, color:ColorDefinitions.accentColor.shade0}}>Leg Room</Text>
                            <View style={{flexDirection: 'row', justifyContent: 'flex-start'}}>
                                <MaterialCommunityIcons name={'seat-legroom-normal'} size={20} color={ColorDefinitions.darkPrimaryColor.shade5}/>
                                <Text style={{fontFamily: 'alpha-regular', paddingHorizontal: 5, paddingVertical: 2, color: ColorDefinitions.darkPrimaryColor.shade5}}>{item.specs.leg_room}</Text>
                            </View>
                        </View>
                    </View>
                </View>
            </Tile>
        );
    };
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: ColorDefinitions.mainBackground,
    },
});

const mapStateToProps = (state) => {
    return {

    }
};

const matchDispatchToProps = dispatch => {
    return {

    }
};

export default connect(mapStateToProps, matchDispatchToProps)(LandingScreen);
