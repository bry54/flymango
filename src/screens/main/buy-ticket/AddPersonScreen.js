import React, {Component} from 'react';
import {ScrollView, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {connect} from "react-redux";
import TypoDefinitions from "../../../constants/TypoDefinitions";
import {Button, Input} from "react-native-elements";
import DateTimePicker from '@react-native-community/datetimepicker';
import moment from "moment";
import {appendPerson} from "../../../store/utilities/actionsCollection";
import ColorDefinitions from "../../../constants/ColorDefinitions";
import {MaterialCommunityIcons} from "@expo/vector-icons";
import Ripple from "react-native-material-ripple";

class AddPersonScreen extends Component {

    state = {
        dobModalVisible: false,
        firstName: null,
        lastName: null,
        email: null,
        phone: null,
        dob: null,
        passport: null,
    };

    render () {
        const formIncomplete= !this.state.firstName;
        return (
            <ScrollView
                showsHorizontalScrollIndicator={false}
                showsVerticalScrollIndicator={false}
                contentContainerStyle={styles.container}>
                {this.state.dobModalVisible && (
                    <DateTimePicker
                        timeZoneOffsetInMinutes={0}
                        value={new Date()}
                        mode={'date'}
                        is24Hour={true}
                        display="default"
                        onChange={this._onDateChange}
                    />
                )}
                <View style={styles.signUpSection}>
                    <Input
                        rightIcon={<MaterialCommunityIcons name={'rename-box'} size={20} color={ColorDefinitions.accentColor.shade0}/>}
                        value={this.state.firstName}
                        containerStyle={{ paddingTop: 5}}
                        inputContainerStyle={{borderColor: ColorDefinitions.accentColor.shade0, borderBottomWidth:2, paddingTop: 0}}
                        inputStyle={{fontFamily: 'alpha-regular'}}
                        label='First Name'
                        labelStyle={{fontFamily: 'alpha-regular', fontWeight: TypoDefinitions.lightText, fontSize: TypoDefinitions.xSmallFont, color: ColorDefinitions.darkText.shade4}}
                        placeholder='John'
                        onChangeText={val => this._updateInputControl('firstName', val)}
                    />

                    <Input
                        rightIcon={<MaterialCommunityIcons name={'rename-box'} size={20} color={ColorDefinitions.accentColor.shade0}/>}
                        value={this.state.lastName}
                        containerStyle={{ paddingTop: 5}}
                        inputContainerStyle={{borderColor: ColorDefinitions.accentColor.shade0, borderBottomWidth:2}}
                        inputStyle={{fontFamily: 'alpha-regular'}}
                        label='Surname'
                        labelStyle={{fontFamily: 'alpha-regular', fontWeight: TypoDefinitions.lightText, fontSize: TypoDefinitions.xSmallFont, color: ColorDefinitions.darkText.shade4}}
                        placeholder='Doe'
                        onChangeText={val => this._updateInputControl('lastName', val)}
                    />

                    <Input
                        rightIcon={<MaterialCommunityIcons name={'passport'} size={20} color={ColorDefinitions.accentColor.shade0}/>}
                        value={this.state.passport}
                        keyboardType='default'
                        containerStyle={{ paddingTop: 5}}
                        inputContainerStyle={{borderColor: ColorDefinitions.accentColor.shade0, borderBottomWidth:2}}
                        inputStyle={{fontFamily: 'alpha-regular'}}
                        label='Identification Number'
                        labelStyle={{fontFamily: 'alpha-regular', fontWeight: TypoDefinitions.lightText, fontSize: TypoDefinitions.xSmallFont, color: ColorDefinitions.darkText.shade4}}
                        placeholder='DN9056785'
                        onChangeText={val => this._updateInputControl('passport', val)}
                    />

                    <Input
                        rightIcon={<MaterialCommunityIcons name={'email'} size={20} color={ColorDefinitions.accentColor.shade0}/>}
                        value={this.state.email}
                        keyboardType='email-address'
                        containerStyle={{ paddingTop: 5}}
                        inputContainerStyle={{borderColor: ColorDefinitions.accentColor.shade0, borderBottomWidth:2}}
                        inputStyle={{fontFamily: 'alpha-regular'}}
                        label='Email Address'
                        labelStyle={{fontFamily: 'alpha-regular', fontWeight: TypoDefinitions.lightText, fontSize: TypoDefinitions.xSmallFont, color: ColorDefinitions.darkText.shade4}}
                        placeholder='jogndoe@email.com'
                        onChangeText={val => this._updateInputControl('email', val)}
                    />

                    <Input
                        rightIcon={<MaterialCommunityIcons name={'cellphone'} size={20} color={ColorDefinitions.accentColor.shade0}/>}
                        value={this.state.phone}
                        keyboardType='phone-pad'
                        containerStyle={{ paddingTop: 5}}
                        inputContainerStyle={{borderColor: ColorDefinitions.accentColor.shade0, borderBottomWidth:2}}
                        inputStyle={{fontFamily: 'alpha-regular'}}
                        label='Phone'
                        labelStyle={{fontFamily: 'alpha-regular', fontWeight: TypoDefinitions.lightText, fontSize: TypoDefinitions.xSmallFont, color: ColorDefinitions.darkText.shade4}}
                        placeholder='+263 553 8347 917'
                        onChangeText={val => this._updateInputControl('phone', val)}
                    />

                    <TouchableOpacity
                        activeOpacity={.9}
                        onPress={() => {this._updateInputControl('dobModalVisible', true)}}>
                        <Input
                            rightIcon={<MaterialCommunityIcons name={'calendar'} size={20} color={ColorDefinitions.accentColor.shade0}/>}
                            editable={false}
                            value={this.state.dob}
                            containerStyle={{ paddingTop: 30}}
                            inputContainerStyle={{borderColor: ColorDefinitions.accentColor.shade0, borderBottomWidth:2}}
                            label='Enter date of birth'
                            labelStyle={{fontFamily: 'alpha-regular', fontWeight: TypoDefinitions.lightText, fontSize: TypoDefinitions.xSmallFont, color: ColorDefinitions.darkText.shade4}}
                            inputStyle={{fontFamily: 'alpha-regular'}}
                            placeholder='dd.mm.yyyy'
                        />
                    </TouchableOpacity>
                </View>

                {formIncomplete ? (
                    <Button
                        buttonStyle={{backgroundColor: ColorDefinitions.accentColor.shade0, borderRadius: 0}}
                        titleStyle={{textTransform: 'uppercase', fontFamily: 'alpha-regular', marginVertical:5, color: ColorDefinitions.darkText.shade4}}
                        title="Add Person"
                        disabled = {true}/>
                ) : (
                    <View style={{elevation: 0, marginVertical: 0, backgroundColor: ColorDefinitions.accentColor.shade0}}>
                        <Ripple rippleOpacity={.8} onPress={() => this._backToPassengerDetails()}>
                            <Text style={{textTransform: 'uppercase',color: ColorDefinitions.darkText.shade4,fontFamily: 'alpha-regular',fontSize: 16,paddingVertical: 16,textAlign: 'center'}}>
                                {'Add Person'}
                            </Text>
                        </Ripple>
                    </View>
                )}
            </ScrollView>
        )
    }

    _updateInputControl = (inputKey, inputValue) => {
        this.setState({
            ...this.state,
            [inputKey] : inputValue
        });
    };

    _onDateChange = (event, selectedDate) => {
        this._updateInputControl('dob', moment(selectedDate).format('MM.DD.YYYY'));
        this._updateInputControl('dobModalVisible', false);
    };

    _backToPassengerDetails = () =>{
        const {navigation} = this.props;
        const passengerType = this.props.route.params.PASSENGER_TYPE;

        const passenger = {
            passengerCode: passengerType.code,
            firstName: this.state.firstName,
            lastName: this.state.lastName,
            email: this.state.email,
            phone: this.state.phone,
            passport: this.state.passport,
        };

        this.props.appendPerson(passenger);
        navigation.goBack();
    };
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'transparent',
        flex: 1,
        justifyContent: 'space-between'
    },
});

const mapStateToProps = (state) => {
    return {

    }
};

const matchDispatchToProps = dispatch => {
    return {
        appendPerson: (person) => dispatch(appendPerson(person)),
    }
};

export default connect(mapStateToProps, matchDispatchToProps)(AddPersonScreen);
