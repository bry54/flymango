import React, {Component} from 'react';
import {SectionList, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import ColorDefinitions from "../../../constants/ColorDefinitions";
import {connect} from "react-redux";
import {ListItem} from "react-native-elements";
import {MaterialCommunityIcons} from "@expo/vector-icons";
import {removePerson} from "../../../store/actions/buyTickets";

class PassengerDetailsScreen extends Component {

    componentDidMount() {
        this.props.navigation.addListener(
            'focus', () => {
                this.forceUpdate()
            }
        );
    }

    render () {
        const DATA = this.props.passengers;
        const filtered = DATA.filter(item => item.maxPeople);

        return (
            <View style={styles.container}>
                <SectionList
                    sections={filtered}
                    keyExtractor={(item, index) => item + index}
                    renderItem={({ item }) => _renderContent(item, this.props) }
                    renderSectionHeader={({ section: { passengerType, maxPeople } }) => (_renderHeader(passengerType, maxPeople, this.props))}
                />
            </View>
        );
    }
}

const _renderHeader = (item, maxPeople, props) => {
    const _passengers = props.passengers;
    const displayedItem = _passengers.find(p => p.passengerType.id === item.id);

    const addedPeople = displayedItem.data.length;

    return (
        <ListItem
            rightIcon={
                <TouchableOpacity
                    disabled={addedPeople >= maxPeople}
                    onPress={() => {
                    props.navigation.navigate('AddPerson', { PASSENGER_TYPE: item})}
                }>
                {addedPeople < maxPeople && <MaterialCommunityIcons name='account-plus' color={ColorDefinitions.lightText.shade0} size={24} style={{paddingHorizontal: 15}}/>}
                </TouchableOpacity>}
            containerStyle={{paddingHorizontal: 5, backgroundColor: ColorDefinitions.darkBackgroundColor.shade4}}
            bottomDivider={true}
            title={
                <View style={{alignItems: 'center', flexDirection: 'row'}}>
                    <Text style={{fontSize: 14, color: ColorDefinitions.lightText.shade0, fontFamily: 'alpha-regular', textTransform: 'uppercase'}}>
                        {item.name} ({addedPeople}/{maxPeople}) {maxPeople - addedPeople + ' Slots Available'}
                    </Text>
                </View>
            }
        />
    );
};

const _renderContent = (item, props) => {
    return (
        <ListItem
            leftAvatar={{title: item.firstName.charAt(0)}}
            rightAvatar={<TouchableOpacity
                onPress={() => {
                    props.removePerson(item)}
                }>
                <MaterialCommunityIcons name='account-minus' size={24}/>
            </TouchableOpacity>}
            containerStyle={{paddingHorizontal: 10, backgroundColor: ColorDefinitions.lightBackgroundColor.shade0}}
            bottomDivider={true}
            title={item.firstName +' ' +item.lastName}
            titleStyle={{fontSize: 16, color: ColorDefinitions.darkText.shade4, fontFamily: 'alpha-regular', textTransform: 'uppercase'}}
            subtitle={item.passport}
            subtitleStyle={{fontSize: 14, fontFamily: 'alpha-regular', textTransform: 'uppercase'}}
        />
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: ColorDefinitions.mainBackground,
    },
});

const mapStateToProps = (state) => {
    return {
        passengers : state.buyTicketsReducer.ticketParams.passengers
    }
};

const matchDispatchToProps = dispatch => {
    return {
        removePerson: (person) => dispatch(removePerson(person))
    }
};

export default connect(mapStateToProps, matchDispatchToProps)(PassengerDetailsScreen);
