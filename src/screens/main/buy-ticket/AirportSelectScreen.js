import React, {Component} from 'react';
import {FlatList, StyleSheet, View} from 'react-native';
import {connect} from "react-redux";
import {Ionicons, MaterialCommunityIcons} from '@expo/vector-icons';
import {Input, ListItem} from "react-native-elements";
import {updateTicketInputs} from "../../../store/utilities/actionsCollection";
import TypoDefinitions from "../../../constants/TypoDefinitions";
import ColorDefinitions from "../../../constants/ColorDefinitions";

/***
 *
 */
class AirportSelectScreen extends Component {
    render () {
        const ticketInputs = this.props.inputs;
        const airports = this.props.airports;

        return (
            <View style={{flex: 1}}>
                <Input
                    rightIcon={<MaterialCommunityIcons name={'airport'} size={20} color={ColorDefinitions.accentColor.shade0}/>}
                    onChangeText={ (text)=>this.props.updateTicketInputs({key:'airportSearch', value:text})}
                    value={ticketInputs.airportSearch}
                    containerStyle={{ elevation: 2, marginBottom: 2 }}
                    inputContainerStyle={{borderColor: ColorDefinitions.accentColor.shade0, borderBottomWidth:2}}
                    labelStyle={{fontFamily: 'alpha-regular', fontWeight: TypoDefinitions.lightText, fontSize: TypoDefinitions.xSmallFont, color: ColorDefinitions.darkText.shade4}}
                    inputStyle={{fontFamily: 'alpha-regular'}}
                    placeholder='Type to filter airports' />

                <FlatList
                    keyExtractor={(item) => JSON.stringify(item.id)}
                    data={ airports.filter( item => {
                            return item.airport_code.toLowerCase().includes(ticketInputs.airportSearch.toLowerCase()) ||
                                item.name.toLowerCase().includes(ticketInputs.airportSearch.toLowerCase()) ||
                                item.city.name.toLowerCase().includes(ticketInputs.airportSearch.toLowerCase()) ||
                                item.city.code.toLowerCase().includes(ticketInputs.airportSearch.toLowerCase()) ||
                                item.province.name.toLowerCase().includes(ticketInputs.airportSearch.toLowerCase())||
                                item.province.code.toLowerCase().includes(ticketInputs.airportSearch.toLowerCase())
                        })
                    }
                    renderItem={(item) => this.renderItem(item)}/>
            </View>
        );
    }

    renderItem = ({ item }) => {
        const airPortKey= this.props.route.params.AIRPORT_KEY;
        return (
            <ListItem
                onPress={() => {
                    this.props.updateTicketInputs({key: airPortKey, value:item});
                    this.props.updateTicketInputs({key:'airportSearch', value:''});
                    this.props.navigation.goBack();
                }}
                title={item.province.name+', '+item.city.name}
                titleStyle={{fontFamily: 'alpha-regular', color: ColorDefinitions.darkText.shade4, paddingVertical: 3}}
                subtitle={item.airport_code+' - '+item.name}
                subtitleStyle={{fontFamily: 'alpha-regular'}}
                rightIcon={()=>(<Ionicons name="ios-star-outline" size={26} color={ColorDefinitions.accentColor.shade0} />)}
                bottomDivider
            />
        )
    };
}

const styles = StyleSheet.create({
    container: {

    },
});

const mapStateToProps = (state) => {
    return {
        inputs: state.buyTicketsReducer.inputs,
        airportSearch: state.buyTicketsReducer.inputs.airportSearch,
        airports: state.buyTicketsReducer.airports
    }
};

const matchDispatchToProps = dispatch => {
    return {
        updateTicketInputs: (item) => dispatch (updateTicketInputs(item)),
    }
};

export default connect(mapStateToProps, matchDispatchToProps)(AirportSelectScreen);
