import React, {Component} from 'react';
import {ScrollView, StyleSheet, Text, View} from 'react-native';
import ColorDefinitions from "../../../constants/ColorDefinitions";
import {connect} from "react-redux";
import {MaterialCommunityIcons} from "@expo/vector-icons";
import {Divider} from "react-native-elements";
import FlightSummary from "../../../components/buy-ticket/FlightSummary";
import Ripple from "react-native-material-ripple";


class ConfirmBookingScreen extends Component {
    state = {
        useSavedCards: this.props.user && this.props.user.payment_methods.length,
        cvsDialogOpen: false,
        fullName: null,
        cardNumber: null,
        expiryDate: null,
        cardType: null,
        cardCVC: null,
        paymentSnack: false
    };

    render () {
        return (
            <View style={styles.container}>
                <View style={{backgroundColor: ColorDefinitions.darkBackgroundColor.shade4, paddingVertical: 5, paddingHorizontal: 10, flexDirection: 'row', justifyContent: 'space-between'}}>
                    <View>
                        <Text style={{fontSize: 14, fontFamily: 'alpha-regular', color: ColorDefinitions.accentColor.shade0, paddingVertical: 4}}>Total</Text>
                        <Text style={{fontSize: 18, fontFamily: 'alpha-regular', color: ColorDefinitions.lightText.shade0}}>{this.getTotal()}</Text>
                    </View>

                    <View style={{elevation: 0, marginVertical: 8, borderColor: ColorDefinitions.accentColor.shade0, borderRadius: 5, borderWidth: .5}}>
                        <Ripple rippleOpacity={.8} onPress={() => this.props.navigation.navigate('MakePayment')}>
                            <View style={{flexDirection: 'row'}}>
                                <Text style={{color: ColorDefinitions.accentColor.shade0,fontFamily: 'alpha-regular',fontSize: 14,textAlign: 'center', padding: 8}}>
                                    {'Payment'}
                                </Text>
                                <MaterialCommunityIcons name="arrow-right" size={15} color={ColorDefinitions.accentColor.shade0} style={{paddingHorizontal: 5, paddingTop:10}}/>
                            </View>
                        </Ripple>
                    </View>
                </View>

                <Divider style={{padding: 1, backgroundColor: ColorDefinitions.accentColor.shade0}}/>

                <ScrollView showsVerticalScrollIndicator={false}>
                    {(this.props.ticketParams.outBoundFlight && this.props.ticketParams.outBoundCabin) && <FlightSummary
                        ticketParams = {this.props.ticketParams}
                        ticketInputs = {this.props.ticketInputs}
                        flightMode="OUTBOUND"/>}

                    {
                        (this.props.ticketParams.inBoundFlight && this.props.ticketParams.inBoundCabin) && <View>
                            <View style={{backgroundColor: ColorDefinitions.darkBackgroundColor.shade4}}>
                                <Divider style={{padding: .5, marginVertical: 1, backgroundColor: ColorDefinitions.accentColor.shade0}}/>
                                <Divider style={{padding: .5, marginVertical: 1, backgroundColor: ColorDefinitions.accentColor.shade0}}/>
                            </View>

                            <FlightSummary
                                ticketParams={this.props.ticketParams}
                                ticketInputs={this.props.ticketInputs}
                                flightMode="INBOUND"/>
                        </View>
                    }
                </ScrollView>
            </View>
        );
    }

    getTotal = () =>{
        const modes = ['OUTBOUND','INBOUND'];
        const ticketParams = this.props.ticketParams;
        let total = 0;
        const outboundCabin = ticketParams.outBoundCabin;
        const inboundCabin = ticketParams.inBoundCabin;

        modes.forEach(mode =>{
            ticketParams.passengers.forEach( pass => {
                let flightFare;
                if (mode === "OUTBOUND") {
                    flightFare = outboundCabin ?  outboundCabin.data.fares.find(fare => fare.passenger_type_id === pass.passengerType.id) : {amount: 0.00};
                } else{
                    flightFare = inboundCabin ? inboundCabin.data.fares.find(fare => fare.passenger_type_id === pass.passengerType.id) : {amount: 0.00};
                }
                const passTypeCost = parseFloat(flightFare.amount) * pass.data.length;
                total += passTypeCost
            });
        });

        return 'R '+total.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: ColorDefinitions.darkBackgroundColor.shade4,
    }
});

const mapStateToProps = (state) => {
    return {
        user: state.authReducer.loggedInUser,
        ticketParams: state.buyTicketsReducer.ticketParams,
        ticketInputs: state.buyTicketsReducer.inputs
    }
};

const matchDispatchToProps = dispatch => {
    return {

    }
};

export default connect(mapStateToProps, matchDispatchToProps)(ConfirmBookingScreen);
