import React, {Component} from 'react';
import {ScrollView, StyleSheet, Text, View} from 'react-native';
import {connect} from "react-redux";
import {AntDesign} from '@expo/vector-icons';
import {Divider, ListItem} from "react-native-elements";
import {
    buyTicketsUpdateVisibleItems,
    searchFlights,
    updateTicketInputs
} from "../../../store/utilities/actionsCollection";
import AirportSelect from "../../../components/buy-ticket/AirportSelection";
import DateSelection from "../../../components/buy-ticket/DateSelection";
import SearchFlightsButton from "../../../components/buy-ticket/SearchFlightsButton";
import PassangerSelection from "../../../components/buy-ticket/PassangerSelection";
import DateTimePickerModal from "react-native-modal-datetime-picker";
import {Appearance} from "react-native-appearance";
import {SCLAlert, SCLAlertButton} from 'react-native-scl-alert'
import ColorDefinitions from "../../../constants/ColorDefinitions";
import SnackBar from "react-native-snackbar-component";
import Ripple from "react-native-material-ripple";
import i18n from "../../../localizations/i18n";

class LandingScreen extends Component {

    state = {
        departureDatePickerVisibility: false,
        returnDatePickerVisibility: false,
        showDemoInfo: false,
        noFlights: false,
        missingInformation: false,
        missingInformationMessage: ''
    };

    componentDidMount() {
        this.props.navigation.addListener(
            'focus', () => {
                const isRoundTrip = this.props.route.name === 'RoundTrip';
                this._updateInputItem('isRoundTrip', isRoundTrip)
            }
        );
    }

    render () {
        const colorScheme = Appearance.getColorScheme();
        const ticketInputs = this.props.inputs;
        const passengerTypes = this.props.passengerTypes;
        const totalPass = ticketInputs.adults + ticketInputs.children + ticketInputs.infants;

        return (
            <ScrollView showsHorizontalScrollIndicator={false} showsVerticalScrollIndicator={false} contentContainerStyle={styles.container}>
                <SCLAlert
                    overlayStyle={{backgroundColor: ColorDefinitions.darkBackgroundColor.rgbaShade5}}
                    show={this.state.noFlights}
                    onRequestClose={() => this._updateInternalState('noFlights', false)}
                    theme="info"
                    title="Flights Not Found"
                    titleStyle={{ fontFamily: 'alpha-regular' }}
                    subtitle={'There are no flights scheduled on the specified dates.'}
                    subtitleStyle={{ fontFamily: 'alpha-regular' }}>

                    <SCLAlertButton
                        textStyle={{ fontFamily: 'alpha-regular', fontWeight: '400' }}
                        theme="info"
                        onPress={() =>
                            this._updateInternalState('noFlights', false)
                        }>
                        Okay
                    </SCLAlertButton>
                </SCLAlert>

                <SCLAlert
                    overlayStyle={{backgroundColor: ColorDefinitions.darkBackgroundColor.rgbaShade5}}
                    show={this.state.missingInformation}
                    onRequestClose={() => this._updateInternalState('noFlights', false)}
                    theme="danger"
                    title="Missing Information"
                    titleStyle={{ fontFamily: 'alpha-regular' }}
                    subtitle={this.state.missingInformationMessage}
                    subtitleStyle={{ fontFamily: 'alpha-regular' }}>

                    <SCLAlertButton
                        textStyle={{ fontFamily: 'alpha-regular', fontWeight: '400' }}
                        theme="info"
                        onPress={() =>
                            this._updateInternalState('missingInformation', false)
                        }>
                        Okay
                    </SCLAlertButton>
                </SCLAlert>

                <SnackBar
                    backgroundColor='rgba( 14,  6, 50, .8)'
                    messageStyle={{ fontFamily: 'alpha-regular', fontWeight: '400'}}
                    actionStyle={{ fontFamily: 'alpha-regular', fontWeight: '400'}}
                    visible={this.state.showDemoInfo}
                    textMessage={
                        this.props.route.name === 'RoundTrip' ? `DEMO FLIGHT${'\n'}${'\n'}Airports  :  From BNF to GRJ ${'\n'}Dates are not strictly binded` :
                            `DEMO FLIGHT${'\n'}${'\n'}Airports   :   From DUR to HRE ${'\n'}Dates are not strictly binded`
                    }
                    actionHandler={()=>{this._updateInternalState('showDemoInfo', false)}}
                    actionText="Dismiss"/>
                <View>
                    <DateTimePickerModal
                        isDarkModeEnabled={colorScheme === 'dark'}
                        isVisible={this.state.departureDatePickerVisibility}
                        mode="date"
                        onConfirm={async (date) => {
                            this._updateInternalState('departureDatePickerVisibility', false);
                            await this._updateInputItem('departureDate', date);
                        }}
                        onCancel={() => this._updateInternalState('departureDatePickerVisibility', false)}
                    />

                    <DateTimePickerModal
                        isDarkModeEnabled={colorScheme === 'dark'}
                        isVisible={this.state.returnDatePickerVisibility}
                        mode="date"
                        onConfirm={async (date) => {
                            this._updateInternalState('returnDatePickerVisibility', false);
                            await this._updateInputItem('returnDate', date);
                        }}
                        onCancel={() => this._updateInternalState('returnDatePickerVisibility', false)}
                    />

                    <View style={{elevation: 5, marginBottom: 2}}>
                        <View style={styles.locationsSection}>
                            <AirportSelect
                                isRoundTrip={ticketInputs.isRoundTrip}
                                ticketInputs={ticketInputs}
                                navToAirportSection={this._navToAirportSelection}/>
                        </View>

                        <Divider/>

                        <View style={styles.datesSection}>
                            <DateSelection
                                isRoundTrip={ticketInputs.isRoundTrip}
                                departureDate={ticketInputs.departureDate}
                                returnDate={ticketInputs.returnDate}
                                updateInternalState={this._updateInternalState}/>
                        </View>

                        <View style={styles.passengerSection}>
                            <PassangerSelection
                                updateInputItem={()=>this._updateInputItem('cabinClass', null)}
                                ticketInputs={ticketInputs}
                                totalPassengers={totalPass}/>
                        </View>
                    </View>
                    <View>
                        {
                            passengerTypes.map((passenger, i) => (
                                <ListItem
                                    key={i}
                                    title={passenger.name}
                                    titleStyle={{fontFamily:'alpha-regular', fontSize:16, color:ColorDefinitions.darkText.shade4}}
                                    subtitle={passenger.age_desc}
                                    subtitleStyle={{fontFamily:'alpha-regular', fontSize:13, color:ColorDefinitions.darkText.shade4}}
                                    bottomDivider
                                    rightElement={()=>(<View style={{flexDirection: 'row'}}>
                                        <Ripple
                                            onPress={()=>this._updateInputItem(passenger.code, ticketInputs[passenger.code] - 1)}
                                            rippleOpacity={.8}>
                                            <AntDesign
                                                name="minus"
                                                size={26}
                                                color={ColorDefinitions.darkText.shade4}/>
                                        </Ripple>
                                        <Text style={{fontFamily: 'alpha-regular', fontSize: 24, paddingHorizontal: 10, color: ColorDefinitions.darkText.shade4}}>
                                            {ticketInputs[passenger.code]}
                                        </Text>
                                        <Ripple
                                            onPress={()=>this._updateInputItem(passenger.code, ticketInputs[passenger.code] + 1)}
                                            rippleOpacity={.8}>
                                            <AntDesign
                                                name="plus"
                                                size={26}
                                                color={ColorDefinitions.darkText.shade4}/>
                                        </Ripple>
                                    </View>)}
                                />
                            ))
                        }
                    </View>
                </View>
                <SearchFlightsButton
                    raised={!this.state.showDemoInfo}
                    hasMissingInformation={this._hasMissingInformation()}
                    compileErrors={this._compileErrors}
                    searchFlights={this._searchFlights}/>
            </ScrollView>
        );
    }

    _compileErrors = () =>{
        const message = [];
        const isRoundTrip = this.props.route.name === 'RoundTrip';
        const ticketInputs = this.props.inputs;
        const totalPass = ticketInputs.adults + ticketInputs.children + ticketInputs.infants;
        if(!ticketInputs.origin) message.push(i18n.t('lbl_origin'))
        if(!ticketInputs.destination) message.push(i18n.t('lbl_destination'))
        if(!ticketInputs.cabinClass) message.push(i18n.t('lbl_cabin_class'))
        if(!ticketInputs.departureDate) message.push(i18n.t('lbl_depart_date'))
        if(isRoundTrip && !ticketInputs.returnDate) message.push(i18n.t('lbl_return_date'))
        if(totalPass < 1) message.push('Select Passengers'+'\n')

        this._updateInternalState('missingInformationMessage', message.join('\n'));
        this._updateInternalState('missingInformation', true)
    };

    _hasMissingInformation = () =>{
        const  ticketInputs = this.props.inputs;
        const totalPass = ticketInputs.adults + ticketInputs.children + ticketInputs.infants;
        const isRoundTrip = this.props.route.name === 'RoundTrip';
        if (isRoundTrip)
            return (
                !ticketInputs.origin ||
                !ticketInputs.destination ||
                !ticketInputs.departureDate||
                !ticketInputs.returnDate ||
                !ticketInputs.cabinClass ||
                totalPass < 1
            );
        else
            return (
                !ticketInputs.origin ||
                !ticketInputs.destination ||
                !ticketInputs.departureDate ||
                !ticketInputs.cabinClass ||
                totalPass < 1
            )
    };

    _updateInternalState = (inputKey, inputValue) => {
        this.setState({
            ...this.state,
            [inputKey] : inputValue
        });
    };

    _navToAirportSelection = (key) =>{
        this.props.navigation.navigate('AirportSelection', {AIRPORT_KEY: key});
    };

    _searchFlights = async () => {
        const {navigation} = this.props;
        const ticketInputs = this.props.inputs;
        const searchParams = {
            origin: ticketInputs.origin,
            destination: ticketInputs.destination,
            departureDate: ticketInputs.departureDate,
            returnDate: ticketInputs.returnDate
        };

        await this.props.searchFlights(searchParams);

        if (this.props.route.name === 'RoundTrip'){
            if (this.props.foundFlights.flightsTo && this.props.foundFlights.flightsFrom)
                navigation.navigate('OutboundFlightSelection');
            else {
                this._updateInternalState('noFlights', true)
            }
        } else {
            if (this.props.foundFlights.flightsTo)
                navigation.navigate('OutboundFlightSelection');
            else {
                this._updateInternalState('noFlights', true)
            }
        }
    };

    _updateInputItem = (itemKey, itemValue) => {
        const passengerTypes = this.props.passengerTypes;
        const item = {
            key: itemKey,
            value: (passengerTypes.find(pass=>pass.code === itemKey) && itemValue <=0) ? 0 : itemValue
        };

        this.props.updateTicketInputs(item);
    };
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'space-between'
    },

    locationsSection: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: ColorDefinitions.darkBackgroundColor.shade4
    },

    datesSection:{
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: ColorDefinitions.darkBackgroundColor.shade4
    },

    passengerSection:{
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },

    passengerItem:{
        width: '50%',
        justifyContent: 'center',
        padding: 10,
    },

    cabinItem:{
        width: '50%',
        justifyContent: 'center',
        padding: 10,
    },

    passengersSectionTitle:{
        fontFamily: 'alpha-regular',
        textTransform: 'uppercase',
        fontSize: 10,
        color: ColorDefinitions.darkText.shade4
    },

    passengerInfo:{
        flexDirection: 'row'
    },

    cabinInfo:{
        flexDirection: 'row',
        fontSize: 26,
        color: ColorDefinitions.darkText.shade4,
        fontFamily: 'alpha-regular',
    },

    passengerCount:{
        fontSize: 30,
        paddingHorizontal: 10,
        fontFamily: 'alpha-regular',
        color: ColorDefinitions.darkText.shade4
    }
});

const mapStateToProps = (state) => {
    return {
        isRoundTrip: false,
        visibleItems: state.buyTicketsReducer.visibleItems,
        foundFlights: state.buyTicketsReducer.foundFlights,
        inputs: state.buyTicketsReducer.inputs,
        passengerTypes: state.buyTicketsReducer.passengerTypes,
    }
};

const matchDispatchToProps = dispatch => {
    return {
        updateVisibleItems: (item) => dispatch (buyTicketsUpdateVisibleItems(item)),
        updateTicketInputs: (item) => dispatch (updateTicketInputs(item)),
        searchFlights: (params) => dispatch (searchFlights(params)),
    }
};

export default connect(mapStateToProps, matchDispatchToProps)(LandingScreen);
