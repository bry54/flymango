import React, {Component} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import ColorDefinitions from "../../../constants/ColorDefinitions";
import {connect} from "react-redux";
import FlightsList from "../../../components/buy-ticket/FlightsList";
import {MaterialCommunityIcons} from '@expo/vector-icons';
import moment from "moment";
import {updateTicketInputs, updateTicketParams} from "../../../store/utilities/actionsCollection";
import compartments from "../../../utilities/api/compartments";

/**
 * receives a parameter SETTING_RETURN_FLIGHT
 */
class SearchResultsScreen extends Component {

    componentDidMount() {
        this.props.navigation.addListener(
            'focus', () => {
                const isSettingOutboundFlight = this.props.route.name === 'OutboundFlightSelection';
                if (isSettingOutboundFlight){
                    this.props.updateTicketParams({key: 'inBoundFlight', value: null});
                    this.props.updateTicketParams({key: 'inBoundCabin', value: null});
                }
            }
        );
    }

    render () {
        const isSettingReturnFlight = this.props.route.name === 'InboundFlightSelection';

        return (
            <View style={styles.container}>
                <View style={{flexDirection: 'row'}}>
                    <View style={{
                        alignItems: 'center',
                        backgroundColor: ColorDefinitions.darkBackgroundColor.shade4,
                        paddingHorizontal: 10,
                        paddingVertical: 10,
                        elevation: 5}}>
                        <Text style={{
                            textTransform: 'uppercase',
                            fontFamily: 'alpha-regular',
                            marginVertical: 3,
                            color: ColorDefinitions.lightText.shade0,
                            fontSize: 18,
                            paddingVertical: 8
                        }}>
                            {isSettingReturnFlight ? 'INBOUND':'OUTBOUND'}
                        </Text>

                        {
                            isSettingReturnFlight ?
                                <MaterialCommunityIcons name="airplane-landing" size={26} style={{transform: [{ scaleX: -1 }]}} color={ColorDefinitions.lightText.shade0}/> :
                                <MaterialCommunityIcons size={26} style={{paddingHorizontal: 12, paddingTop: 3, color: ColorDefinitions.lightText.shade0}} name='airplane-takeoff'/>
                        }
                    </View>

                    <View style={{flex: 3, backgroundColor: '#eceff1', paddingHorizontal: 10, paddingVertical: 10}}>
                        <View style={{flexDirection: 'row'}}>
                            <Text style={{fontFamily: 'alpha-regular', textTransform: 'uppercase', paddingVertical: 5, color: ColorDefinitions.darkText.shade4, fontSize: 12}}>
                                {isSettingReturnFlight ? this.props.userInputs.destination.city.name : this.props.userInputs.origin.city.name}
                            </Text>
                            <MaterialCommunityIcons size={16} style={{paddingHorizontal: 12, paddingTop: 3, color: ColorDefinitions.darkText.shade4}} name='arrow-right'/>
                            <Text style={{fontFamily: 'alpha-regular', textTransform: 'uppercase', paddingVertical: 5, fontSize: 12, color: ColorDefinitions.darkText.shade4}}>
                                {isSettingReturnFlight ? this.props.userInputs.origin.city.name : this.props.userInputs.destination.city.name}
                            </Text>
                        </View>

                        <Text style={{fontFamily: 'alpha-regular', textTransform: 'uppercase', paddingVertical: 5, color: ColorDefinitions.darkText.shade4, fontSize: 12}}>
                            {moment(isSettingReturnFlight ? this.props.userInputs.returnDate : this.props.userInputs.departureDate).format('LL')}
                        </Text>
                        <Text style={{fontFamily: 'alpha-regular', textTransform: 'uppercase', paddingVertical: 5, color: ColorDefinitions.darkText.shade4, fontSize: 12}}>
                            {this.props.userInputs.cabinClass ? `${this.props.userInputs.cabinClass.class_desc} class` : 'NA'}
                        </Text>
                    </View>
                </View>

                <FlightsList
                    userInputs={this.props.userInputs}
                    setCabin={this._setCabin}
                    setFlight={this._setFlight}
                    navToNextScreen={this._navToNextScreen}
                    navBack={this._navBack}
                    flights={isSettingReturnFlight ? this.props.flightsFrom : this.props.flightsTo}/>
            </View>
        );
    }

    _navToNextScreen = () => {
        const {navigation} = this.props;
        const isRoundTrip = this.props.userInputs.isRoundTrip;

        if (isRoundTrip && !this.props.ticketParams.inBoundFlight){
            navigation.navigate('InboundFlightSelection');
        } else {
            navigation.navigate('PassengerDetails');
        }
    };

    _navBack = () => {
        const {navigation} = this.props;
        navigation.goBack()
    };

    _setCabin = (cabin) => {
        const isSettingReturnFlight = this.props.route.name === 'InboundFlightSelection';
        this.props.updateTicketInputs({key: 'cabinClass', value: compartments.find(c => c.id ===cabin.item.id)});

        if (isSettingReturnFlight)
            this.props.updateTicketParams({key: 'inBoundCabin', value: cabin});
        else{
            this.props.updateTicketParams({key: 'outBoundCabin', value: cabin});
        }
    };

    _setFlight = (flight) => {
        const isSettingReturnFlight = this.props.route.name === 'InboundFlightSelection';

        if (isSettingReturnFlight)
            this.props.updateTicketParams({key: 'inBoundFlight', value: flight});
        else
            this.props.updateTicketParams({key: 'outBoundFlight', value: flight});
    };
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: ColorDefinitions.lightBackgroundColor.shade0//ColorDefinitions.mainBackground,
    },
});

const mapStateToProps = (state) => {
    return {
        flightsTo: state.buyTicketsReducer.foundFlights.flightsTo,
        flightsFrom: state.buyTicketsReducer.foundFlights.flightsFrom,
        userInputs: state.buyTicketsReducer.inputs,
        ticketParams: state.buyTicketsReducer.ticketParams
    }
};

const matchDispatchToProps = dispatch => {
    return {
        updateTicketParams: (item) => dispatch (updateTicketParams(item)),
        updateTicketInputs: (item) => dispatch (updateTicketInputs(item)),
    }
};

export default connect(mapStateToProps, matchDispatchToProps)(SearchResultsScreen);
