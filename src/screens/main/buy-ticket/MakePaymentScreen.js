import React, {Component} from 'react';
import {FlatList, ScrollView, StyleSheet, Text, View} from 'react-native';
import ColorDefinitions from "../../../constants/ColorDefinitions";
import {connect} from "react-redux";
import {FontAwesome, MaterialCommunityIcons} from "@expo/vector-icons";
import {Avatar, Button, ButtonGroup, Input} from "react-native-elements";
import moment from "moment";
import TypoDefinitions from "../../../constants/TypoDefinitions";
import Dialog from "react-native-dialog";
import {SkypeIndicator} from "react-native-indicators";
import {saveTicket} from "../../../store/utilities/actionsCollection";
import Ripple from "react-native-material-ripple";

class MakePaymentScreen extends Component {
    fakePayment = null;
    state = {
        buttonGroupIndex: 0,
        cvsDialogOpen: false,
        fullName: null,
        cardNumber: null,
        expiryDate: null,
        cardType: null,
        cardCVC: null,

        paymentInitiated: false,
        isAttemptingPayment: false,
        isPaymentSuccess: false
    };

    render () {
        const buttons = ['Saved Cards', 'New Card'];
        const paymentMethods = this.props.user ? this.props.user.payment_methods : [];
        const formIncomplete = !this.state.cardType;

        if (this.state.paymentInitiated)
            return (
                this.state.isAttemptingPayment ? (
                    <View style={{flex: 1, justifyContent:'center', alignItems: 'center'}}>
                        <SkypeIndicator size={60} color={ColorDefinitions.darkBackgroundColor.shade4}/>
                        <Text style={{marginBottom: 200, fontFamily: 'alpha-regular', fontSize: 20, color: ColorDefinitions.darkText.shade4}}>Processing payment, please wait.</Text>
                    </View>
                ) : (
                    this.state.isPaymentSuccess ? (
                        <View style={{flex: 1, justifyContent:'center', alignItems: 'center'}}>
                            <Avatar
                                overlayContainerStyle={{backgroundColor: ColorDefinitions.darkBackgroundColor.shade4}}
                                containerStyle={{elevation: 5}}
                                size={"xlarge"}
                                icon={{name:'check-decagram', type:'material-community', color: ColorDefinitions.accentColor.shade0}}
                                rounded />
                            <Text style={{fontFamily: 'alpha-regular', fontSize: 18, paddingVertical: 20, color: ColorDefinitions.darkText.shade4}}>
                                Payment was successfully completed.
                            </Text>
                            <View style={{borderRadius: 50, width: '60%', elevation: 5, margin: 10, marginTop: 40, backgroundColor: ColorDefinitions.darkBackgroundColor.shade4}}>
                                <Ripple rippleOpacity={.8} onPress={()=>{this._navToHome()}}>
                                    <Text style={{textTransform: 'uppercase', color: ColorDefinitions.accentColor.shade0,fontFamily: 'alpha-regular',fontSize: 17,paddingVertical: 15,textAlign: 'center'}}>
                                        {'Done'}
                                    </Text>
                                </Ripple>
                            </View>
                        </View>
                    ) : (
                        <View style={{flex: 1, justifyContent:'center', alignItems: 'center'}}>
                            <Avatar
                                overlayContainerStyle={{backgroundColor: ColorDefinitions.darkBackgroundColor.shade4}}
                                containerStyle={{elevation: 5}}
                                size={"xlarge"}
                                icon={{name:'alert-decagram', type:'material-community', color: ColorDefinitions.accentColor.shade0}}
                                rounded />

                            <Text style={{fontFamily: 'alpha-regular', fontSize: 18, paddingVertical: 20, color: ColorDefinitions.darkText.shade4, textAlign: 'center'}}> Transaction failed, do you want to try again?</Text>

                            <View style={{borderRadius: 50, width: '60%', elevation: 5, margin: 10, marginTop: 40, backgroundColor: ColorDefinitions.accentColor.shade0}}>
                                <Ripple rippleOpacity={.8} onPress={()=>{this._initiatePayment()}}>
                                    <Text style={{textTransform: 'uppercase', color: ColorDefinitions.darkBackgroundColor.shade4, fontFamily: 'alpha-regular',fontSize: 17,paddingVertical: 15,textAlign: 'center'}}>
                                        {'Retry'}
                                    </Text>
                                </Ripple>
                            </View>

                            <View style={{borderRadius: 50, width: '60%', elevation: 5, margin: 10, marginTop: 40, backgroundColor: ColorDefinitions.darkBackgroundColor.shade4}}>
                                <Ripple rippleOpacity={.8} onPress={()=>{this._navToHome()}}>
                                    <Text style={{textTransform: 'uppercase', color: ColorDefinitions.accentColor.shade0,fontFamily: 'alpha-regular',fontSize: 17,paddingVertical: 15,textAlign: 'center'}}>
                                        {'Cancel'}
                                    </Text>
                                </Ripple>
                            </View>
                        </View>
                    )
                )
            );

        return (
            <View style={styles.container}>
                <Dialog.Container visible={this.state.cvsDialogOpen}>
                    <Dialog.Title style={{fontFamily: 'alpha-regular', color: ColorDefinitions.darkBackgroundColor.shade4, marginBottom: 15}}>Enter Card CVC</Dialog.Title>
                    <Dialog.Input
                        keyboardType='numeric'
                        maxLength={3}
                        onTextChange={(val) => {console.log(this.state.cardCVC); this._updateInputControl('cardCVC', val)}}
                        value={this.state.cardCVC}
                        style={{color: '#000', fontFamily: 'alpha-regular', borderColor: ColorDefinitions.darkBackgroundColor.shade4, borderWidth: Platform.OS === 'ios' ? 0 : 1, borderRadius: Platform.OS === 'ios' ? 0 : 10}}
                    />
                    <Dialog.Button
                        color={'red'}
                        label="Cancel" onPress={() => {this._updateInputControl('cvsDialogOpen', false)}} />
                    <Dialog.Button
                        color={'teal'}
                        label="Proceed" onPress={ async () => {
                        await this._updateInputControl('cvsDialogOpen', false);
                        this._initiatePayment()
                    }}/>
                </Dialog.Container>

                <View style={{elevation: 5, paddingHorizontal: 5, paddingVertical: 5, backgroundColor: ColorDefinitions.colorPrimary}}>
                    <ButtonGroup
                        innerBorderStyle={{color: 'transparent'}}
                        selectedButtonStyle={{backgroundColor: 'transparent', borderBottomWidth: 2, borderBottomColor: ColorDefinitions.darkText.shade2}}
                        textStyle={{textTransform: 'uppercase', fontFamily: 'alpha-regular', fontSize: 15, color: ColorDefinitions.darkText.shade4}}
                        selectedTextStyle={{fontFamily: 'alpha-regular', fontSize: 15 , color: ColorDefinitions.darkText.shade4}}
                        onPress={(index) => this._updateInputControl('buttonGroupIndex', index)}
                        selectedIndex={this.state.buttonGroupIndex}
                        buttons={buttons}
                        containerStyle={{height: 40, backgroundColor: ColorDefinitions.colorPrimary, borderWidth: 0, borderRadius: 0}}
                    />
                </View>

                <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={styles.container}>
                    { this.state.buttonGroupIndex === 0 ?
                        (
                            <FlatList
                                ListEmptyComponent={
                                    <View style={{alignSelf: 'center'}}>
                                        <MaterialCommunityIcons name='information-variant' size={80} color={ColorDefinitions.accentColor.shade0} style={{alignSelf: 'center'}}/>
                                        <Text style={{fontSize: 20, textAlign: 'center', fontFamily: 'alpha-regular'}}>You do not have any saved payment cards.</Text>
                                    </View>
                                }
                                style={{marginTop: 5}}
                                numColumns={1}
                                keyExtractor={(item)=>{JSON.stringify(item)}}
                                showsHorizontalScrollIndicator={false}
                                horizontal={false}
                                data={paymentMethods}
                                renderItem={this.renderItem}/>
                        ):(
                            <View style={{flex: 1, justifyContent: 'space-between'}}>
                                <Input
                                    rightIcon={<MaterialCommunityIcons name={'credit-card'} size={20} color={ColorDefinitions.accentColor.shade0}/>}
                                    value={this.state.cardType}
                                    containerStyle={{ paddingTop: 5}}
                                    inputContainerStyle={{borderColor: ColorDefinitions.accentColor.shade0, borderBottomWidth:2, paddingTop: 0}}
                                    inputStyle={{fontFamily: 'alpha-regular'}}
                                    label='Card Type'
                                    labelStyle={{fontFamily: 'alpha-regular', fontWeight: TypoDefinitions.lightText, fontSize: TypoDefinitions.xSmallFont, color: ColorDefinitions.darkText.shade4}}
                                    placeholder='VISA'
                                    onChangeText={val => this._updateInputControl('cardType', val)}
                                />

                                <Input
                                    rightIcon={<MaterialCommunityIcons name={'rename-box'} size={20} color={ColorDefinitions.accentColor.shade0}/>}
                                    value={this.state.fullName}
                                    containerStyle={{ paddingTop: 5}}
                                    inputContainerStyle={{borderColor: ColorDefinitions.accentColor.shade0, borderBottomWidth:2, paddingTop: 0}}
                                    inputStyle={{fontFamily: 'alpha-regular'}}
                                    label='Name on card'
                                    labelStyle={{fontFamily: 'alpha-regular', fontWeight: TypoDefinitions.lightText, fontSize: TypoDefinitions.xSmallFont, color: ColorDefinitions.darkText.shade4}}
                                    placeholder='John Doe'
                                    onChangeText={val => this._updateInputControl('fullName', val)}
                                />

                                <Input
                                    rightIcon={<MaterialCommunityIcons name={'credit-card'} size={20} color={ColorDefinitions.accentColor.shade0}/>}
                                    keyboardType={'numeric'}
                                    value={this.state.cardNumber}
                                    containerStyle={{ paddingTop: 5}}
                                    inputContainerStyle={{borderColor: ColorDefinitions.accentColor.shade0, borderBottomWidth:2, paddingTop: 0}}
                                    inputStyle={{fontFamily: 'alpha-regular'}}
                                    label='Card Number'
                                    labelStyle={{fontFamily: 'alpha-regular', fontWeight: TypoDefinitions.lightText, fontSize: TypoDefinitions.xSmallFont, color: ColorDefinitions.darkText.shade4}}
                                    placeholder='xxxx-xxxx-xxxx-xxxx'
                                    onChangeText={val => this._updateInputControl('cardNumber', val)}
                                />

                                <Input
                                    rightIcon={<MaterialCommunityIcons name={'calendar'} size={20} color={ColorDefinitions.accentColor.shade0}/>}
                                    keyboardType={'numeric'}
                                    value={this.state.expiryDate}
                                    containerStyle={{ paddingTop: 5}}
                                    inputContainerStyle={{borderColor: ColorDefinitions.accentColor.shade0, borderBottomWidth:2, paddingTop: 0}}
                                    inputStyle={{fontFamily: 'alpha-regular'}}
                                    label='Expiry Date'
                                    labelStyle={{fontFamily: 'alpha-regular', fontWeight: TypoDefinitions.lightText, fontSize: TypoDefinitions.xSmallFont, color: ColorDefinitions.darkText.shade4}}
                                    placeholder='MM/YYYY'
                                    onChangeText={val => this._updateInputControl('expiryDate', val)}
                                />

                                <Input
                                    rightIcon={<MaterialCommunityIcons name={'credit-card'} size={20} color={ColorDefinitions.accentColor.shade0}/>}
                                    keyboardType={'numeric'}
                                    value={this.state.cardCVC}
                                    containerStyle={{ paddingTop: 5}}
                                    inputContainerStyle={{borderColor: ColorDefinitions.accentColor.shade0, borderBottomWidth:2, paddingTop: 0}}
                                    inputStyle={{fontFamily: 'alpha-regular'}}
                                    label='CVC'
                                    labelStyle={{fontFamily: 'alpha-regular', fontWeight: TypoDefinitions.lightText, fontSize: TypoDefinitions.xSmallFont, color: ColorDefinitions.darkText.shade4}}
                                    placeholder='xxx'
                                    onChangeText={val => this._updateInputControl('cardCVC', val)}
                                />

                                {formIncomplete ? (
                                    <Button
                                        containerStyle={{margin: 10, marginTop: 40}}
                                        buttonStyle={{backgroundColor: ColorDefinitions.accentColor.shade0, borderRadius: 0}}
                                        titleStyle={{textTransform: 'capitalize', fontFamily: 'alpha-regular', marginVertical:5, color: ColorDefinitions.darkText.shade4}}
                                        title="Save Card"
                                        raised
                                        disabled = {true}/>
                                ) : (
                                    <View style={{elevation: 5, margin: 10, marginTop: 40, backgroundColor: ColorDefinitions.accentColor.shade0}}>
                                        <Ripple rippleOpacity={.8} onPress={() => this._updateInputControl('buttonGroupIndex', 0)}>
                                            <Text style={{textTransform: 'uppercase', color: ColorDefinitions.darkText.shade4,fontFamily: 'alpha-regular',fontSize: 16,paddingVertical: 16,textAlign: 'center'}}>
                                                {'Save Card'}
                                            </Text>
                                        </Ripple>
                                    </View>
                                )}

                            </View>
                        )
                    }
                </ScrollView>
            </View>
        );
    }

    renderItem = ({item}) =>{
        const cc = item.card_number.replace(/(\d{4})(?=\d)/g, '$1-');
        const ccArr = cc.split('-');
        const first4 = ccArr[0];
        const last4 = ccArr[ccArr.length-1];

        return (
            <Ripple
                rippleOpacity={.8}
                onPress={() => this._updateInputControl('cvsDialogOpen', true)}>
                <View style={{padding: 10, margin: 5, backgroundColor: ColorDefinitions.darkBackgroundColor.shade4, elevation: 2, borderRadius: 5}}>
                    <View style={{alignSelf: 'flex-end'}}>
                        <FontAwesome
                            name={
                                (() => {
                                    if (item.type === 'visa') return 'cc-visa';
                                    else if (item.type.includes('mastercard')) return 'cc-mastercard';
                                    else if (item.type.includes('jcb')) return 'cc-jcb';
                                    else if (item.type.includes('discover')) return 'cc-discover';
                                    else return 'credit-card';
                                })()}
                            size={30} style={{justifyContent: 'flex-end', alignItem: 'flex-end', color: ColorDefinitions.lightText.shade0 }}/>
                    </View>

                    <View style={{paddingVertical:5}}>
                        <Text style={{color: ColorDefinitions.accentColor.shade0, textAlign: 'left', textTransform: 'uppercase', fontFamily: 'alpha-regular', fontSize: 12}}>
                            Card Number
                        </Text>
                        <View style={{flexDirection: 'row'}}>
                            <Text style={{color: ColorDefinitions.lightText.shade0, textAlign: 'left', textTransform: 'uppercase', fontFamily: 'alpha-regular', fontSize: 26}}>
                                {first4}{" "}
                            </Text>
                            {[1,2,3,4].map( num =><MaterialCommunityIcons key={num} name={'asterisk'} color={ColorDefinitions.lightText.shade0} size={18} style={{paddingTop: 6}}/>)}
                            <Text style={{color: ColorDefinitions.lightText.shade0, textAlign: 'left', textTransform: 'uppercase', fontFamily: 'alpha-regular', fontSize: 26}}>
                                {" "}
                            </Text>
                            {[1,2,3,4].map( num =><MaterialCommunityIcons key={num} name={'asterisk'} color={ColorDefinitions.lightText.shade0} size={18} style={{paddingTop: 6}}/>)}
                            <Text style={{color: ColorDefinitions.lightText.shade0, textAlign: 'left', textTransform: 'uppercase', fontFamily: 'alpha-regular', fontSize: 26}}>
                                {" "}
                            </Text>
                            <Text style={{color: ColorDefinitions.lightText.shade0, textAlign: 'left', textTransform: 'uppercase', fontFamily: 'alpha-regular', fontSize: 26}}>
                                {last4}
                            </Text>
                        </View>
                    </View>

                    <View style={{flexDirection: 'row', justifyContent: 'space-between', paddingVertical: 5}}>
                        <View>
                            <Text style={{color: ColorDefinitions.accentColor.shade0, textAlign: 'left', textTransform: 'uppercase', fontFamily: 'alpha-regular', paddingBottom: 3, fontSize: 12}}>
                                Card Holder's Name
                            </Text>
                            <Text style={{color: ColorDefinitions.lightText.shade0, textAlign: 'left', textTransform: 'uppercase', fontFamily: 'alpha-regular', paddingBottom: 0, fontSize: 14}}>
                                {item.holder_name}
                            </Text>
                        </View>

                        <View>
                            <Text style={{color: ColorDefinitions.accentColor.shade0, textAlign: 'left', textTransform: 'uppercase', fontFamily: 'alpha-regular', paddingBottom: 3, fontSize: 12}}>
                                Expiry Date
                            </Text>
                            <Text style={{color: ColorDefinitions.lightText.shade0, textAlign: 'left', textTransform: 'uppercase', fontFamily: 'alpha-regular', paddingBottom: 0, fontSize: 16}}>
                                {moment(item.expiry_date, 'DD.MM.YYYY').format('MM/YYYY')}
                            </Text>
                        </View>
                    </View>
                </View>
            </Ripple>
        )
    };

    _initiatePayment = () => {
        this._updateInputControl('paymentInitiated', true);
        this._updateInputControl('isAttemptingPayment', true);

        this.fakePayment = setInterval(() => {
            this._updatePaymentState()
        }, 5000);
    };

    _updatePaymentState = () =>{
        clearInterval(this.fakePayment);
        const result = Math.random() >= 0.5;
        if (result){
            this.props.saveTicket(this.props.ticketParams);
        }
        this._updateInputControl('isPaymentSuccess', result);
        this._updateInputControl('isAttemptingPayment', false);
    };

    _navToHome = () =>{
        const {navigation} = this.props;
        navigation.popToTop();
        navigation.navigate('Home')
    };

    _updateInputControl = (inputKey, inputValue) => {
        this.setState({
            ...this.state,
            [inputKey] : inputValue
        });
    };
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: ColorDefinitions.mainBackground,
    },
});

const mapStateToProps = (state) => {
    return {
        user: state.authReducer.loggedInUser,
        ticketParams: state.buyTicketsReducer.ticketParams,
        ticketInputs: state.buyTicketsReducer.inputs
    }
};

const matchDispatchToProps = dispatch => {
    return {
        saveTicket: (ticket) => dispatch(saveTicket(ticket))
    }
};

export default connect(mapStateToProps, matchDispatchToProps)(MakePaymentScreen);
