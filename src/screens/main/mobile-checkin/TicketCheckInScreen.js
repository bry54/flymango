import React, {Component} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import ColorDefinitions from "../../../constants/ColorDefinitions";
import {connect} from "react-redux";
import TypoDefinitions from "../../../constants/TypoDefinitions";
import {Input} from "react-native-elements";
import {MaterialCommunityIcons} from "@expo/vector-icons";
import Ripple from "react-native-material-ripple";

class TicketCheckInScreen extends Component {
    render () {
        return (
            <View style={styles.container}>
                <View>
                    <Input
                        rightIcon={<MaterialCommunityIcons name={'ticket-confirmation'} size={20} color={ColorDefinitions.accentColor.shade0}/>}
                        containerStyle={{marginVertical: 10}}
                        inputContainerStyle={{borderColor: ColorDefinitions.accentColor.shade0, borderBottomWidth: 2}}
                        inputStyle={{fontFamily: 'alpha-regular'}}
                        label='PNR or e-ticket number'
                        labelStyle={{
                            fontFamily: 'alpha-regular',
                            fontWeight: TypoDefinitions.lightText,
                            fontSize: TypoDefinitions.xSmallFont,
                            color: ColorDefinitions.darkText.shade4
                        }}
                        placeholder='PNR or e-ticket number'
                    />

                    <Input
                        rightIcon={<MaterialCommunityIcons name={'rename-box'} size={20} color={ColorDefinitions.accentColor.shade0}/>}
                        containerStyle={{marginVertical: 10}}
                        inputContainerStyle={{borderColor: ColorDefinitions.accentColor.shade0, borderBottomWidth: 2, marginVertical: 10}}
                        inputStyle={{fontFamily: 'alpha-regular'}}
                        label='Surname'
                        labelStyle={{
                            fontFamily: 'alpha-regular',
                            fontWeight: TypoDefinitions.lightText,
                            fontSize: TypoDefinitions.xSmallFont,
                            color: ColorDefinitions.darkText.shade4
                        }}
                        placeholder='Surname'
                    />
                </View>

                <View style={{elevation: 0, marginVertical: 0, backgroundColor: ColorDefinitions.accentColor.shade0}}>
                    <Ripple rippleOpacity={.8} onPress={() => this._navToCheckinDetail()}>
                        <Text style={{textTransform: 'uppercase', color: ColorDefinitions.darkText.shade4,fontFamily: 'alpha-regular',fontSize: 16,paddingVertical: 16,textAlign: 'center'}}>
                            {"Search"}
                        </Text>
                    </Ripple>
                </View>
            </View>
        );
    }

    _navToCheckinDetail = () => {
        const {navigation} = this.props;
        navigation.navigate('Dummy')
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'space-between',
        backgroundColor: ColorDefinitions.mainBackground,
    },
});

const mapStateToProps = (state) => {
    return {

    }
};

const matchDispatchToProps = dispatch => {
    return {

    }
};

export default connect(mapStateToProps, matchDispatchToProps)(TicketCheckInScreen);
