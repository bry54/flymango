import React, {Component} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import ColorDefinitions from "../../../constants/ColorDefinitions";
import {connect} from "react-redux";
import {Avatar} from "react-native-elements";

class SavedFlightsScreen extends Component {

    render () {
        if (this.props.user)
            return (
                <View style={{flex: 1, alignItems: 'center', paddingVertical: 50}}>
                    <Avatar
                        overlayContainerStyle={{backgroundColor: ColorDefinitions.darkBackgroundColor.shade4}}
                        containerStyle={{elevation: 5}}
                        size={"xlarge"}
                        icon={{name:'language-java', type:'material-community', color: ColorDefinitions.accentColor.shade0}}
                        rounded />

                    <Text style={{paddingVertical: 40, paddingHorizontal: 10,fontFamily: 'alpha-regular', fontSize: 18, textAlign: 'center'}}>
                        Under construction.
                    </Text>
                    <Text style={{fontFamily: 'handwriting', fontSize: 24, textAlign: 'center', paddingVertical: 20}}>Brian Paidamoyo Sithole</Text>
                </View>
            );
        else
            return (
                <View style={{flex: 1, alignItems: 'center', paddingVertical: 50}}>
                    <Avatar
                        overlayContainerStyle={{backgroundColor: ColorDefinitions.darkBackgroundColor.shade4}}
                        containerStyle={{elevation: 5}}
                        size={"xlarge"}
                        icon={{name:'alert-decagram', type:'material-community', color: ColorDefinitions.accentColor.shade0}}
                        rounded />
                    <Text style={{paddingVertical: 40, paddingHorizontal: 10,fontFamily: 'alpha-regular', fontSize: 18, textAlign: 'center'}}>
                        Login to access your saved flights.
                    </Text>
                </View>
            )

    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: ColorDefinitions.mainBackground,
        justifyContent: 'center' //remove this line when creating content
    },
});

const mapStateToProps = (state) => {
    return {
        user: state.authReducer.loggedInUser
    }
};

const matchDispatchToProps = dispatch => {
    return {

    }
};

export default connect(mapStateToProps, matchDispatchToProps)(SavedFlightsScreen);
