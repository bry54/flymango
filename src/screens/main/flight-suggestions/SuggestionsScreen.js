import React, {Component} from 'react';
import {ImageBackground, StyleSheet, View} from 'react-native';
import ColorDefinitions from "../../../constants/ColorDefinitions";
import {connect} from "react-redux";
import {ListItem, PricingCard} from "react-native-elements";
import AppIntroSlider from "react-native-app-intro-slider";
import {recoItems} from "../../../utilities/MockUpData";
import i18n from "../../../localizations/i18n";

class SuggestionsScreen extends Component {

    render () {
        return (
            <View style={styles.container}>
                <View style={{backgroundColor: ColorDefinitions.darkBackgroundColor.shade4, elevation: 5}}>
                    <ListItem
                        containerStyle={{backgroundColor: ColorDefinitions.darkBackgroundColor.shade4}}
                        title={'Here is a curated list of tourist destinations we think you might be interested in, travel more with us and we can give destinations tailored for you...'}
                        titleStyle={{
                            fontFamily: 'alpha-regular',
                            fontSize: 16,
                            color: ColorDefinitions.lightText.shade0,
                            textTransform: 'none'
                        }}
                    />
                </View>
                <AppIntroSlider
                    showPrevButton={false}
                    showNextButton={false}
                    showDoneButton={false}
                    dotStyle={{backgroundColor: 'gray'}}
                    activeDotStyle={{backgroundColor: ColorDefinitions.accentColor.shade0}}
                    renderItem={this._renderShowcaseItem}
                    slides={recoItems}
                    onDone={() => {}}/>
            </View>
        );
    }

    _renderShowcaseItem = ({ item }) => {
        return (
            <ImageBackground source={item.image} style={styles.specialsContainer}>
                <View style={{marginBottom: 50}}>
                    <PricingCard
                        onButtonPress={() => this._navToBuyTicket()}
                        containerStyle={{borderWidth: 0, backgroundColor: 'rgba(31, 19, 78, 0.8)' }}
                        color={ColorDefinitions.accentColor.shade0}
                        title={item.title}
                        titleStyle={styles.title}
                        price={i18n.t('lbl_starting_at', { price: item.fee })}
                        pricingStyle={styles.fee}
                        info={[item.text]}
                        infoStyle={styles.text}
                        button={{
                            title: 'Search Ticket Now !!!',
                            titleStyle: { fontFamily: 'alpha-regular', color: ColorDefinitions.darkText.shade4, textTransform: 'capitalize'},
                        }}
                    />
                </View>
            </ImageBackground>
        );
    };

    _navToBuyTicket = () =>{
        const {navigation} = this.props;
        navigation.navigate('BuyTicket');
    };
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        height: '100%',
        backgroundColor: ColorDefinitions.mainBackground,
    },
    specialsContainer: {
        height: '100%',
        justifyContent: 'flex-end',
        paddingHorizontal: 16,
    },

    fee: {fontWeight: 'normal',
        color: 'rgba(255, 255, 255, 1.0)',
        backgroundColor: 'transparent',
        textAlign: 'left',
        fontSize: 23,
        fontFamily: 'alpha-regular',
        marginBottom: 5,
    },

    text: {
        fontWeight: 'normal',
        color: 'rgba(255, 255, 255, 1.0)',
        backgroundColor: 'transparent',
        textAlign: 'left',
        fontSize: 18,
        fontFamily: 'alpha-regular',
        marginBottom: 10,
    },

    title: {
        fontWeight: 'normal',
        color: 'rgba(255, 255, 255, 1.0)',
        fontSize: 26,
        backgroundColor: 'transparent',
        textAlign: 'center',
        marginBottom: 16,
        fontFamily: 'alpha-regular'
    },
});

const mapStateToProps = (state) => {
    return {

    }
};

const matchDispatchToProps = dispatch => {
    return {

    }
};

export default connect(mapStateToProps, matchDispatchToProps)(SuggestionsScreen);
