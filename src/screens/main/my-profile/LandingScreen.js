import React, {Component} from 'react';
import {StyleSheet, View} from 'react-native';
import ColorDefinitions from "../../../constants/ColorDefinitions";
import {connect} from "react-redux";
import SectionHeader from "../../../components/my-profile/SectionHeader";
import {ListItem} from "react-native-elements";
import {attemptSignOut} from "../../../store/actions/auth";
import Ripple from "react-native-material-ripple";

class LandingScreen extends Component {

    render () {
        const {navigation} = this.props;
        const profileSections = [
            {title: 'Personal Information', subTitle: 'Update personal identification information.'},
            {title: 'Flight Preferences',  subTitle: 'Set your flight booking preferences.'},
            {title: 'Payment Information', subTitle: 'Set up your methods for flight payment.'},
            {title: 'My Favorite Cities',  subTitle: 'You havent added any cities to your list.'},
            {title: 'Sign Out', subTitle: 'Bye', key: 'LOGOUT'},
        ];

        return (
            <View style={styles.container}>
                <SectionHeader
                    navigation={navigation}
                    user={this.props.user}/>

                {
                    profileSections.map((item, index) =>(
                        <Ripple
                            key={index}
                            onPress={() => this._expandSection(item)}
                            rippleOpacity={.8}>
                            <ListItem
                                title={item.title}
                                titleStyle={{fontSize: 17,fontFamily: 'alpha-regular', color: ColorDefinitions.darkText.shade4}}
                                subtitle={item.subTitle}
                                subtitleStyle={{fontFamily: 'alpha-regular'}}
                                bottomDivider
                                chevron/>
                        </Ripple>
                    ))
                }
            </View>
        );
    }

    _expandSection = (section) =>{
        const {navigation} = this.props;
        if (section.key === 'LOGOUT'){
            this.props.attemptLogout();
            navigation.navigate('Home')
        } else{
            navigation.navigate('Dummy')
        }
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: ColorDefinitions.mainBackground,
    },
});

const mapStateToProps = (state) => {
    return {
        user: state.authReducer.loggedInUser
    }
};

const matchDispatchToProps = dispatch => {
    return {
        attemptLogout: () => dispatch(attemptSignOut())
    }
};

export default connect(mapStateToProps, matchDispatchToProps)(LandingScreen);
