import React, {Component} from 'react';
import {StyleSheet} from 'react-native';
import ColorDefinitions from "../../../constants/ColorDefinitions";
import {connect} from "react-redux";
import {WebView} from "react-native-webview";
import {SkypeIndicator} from "react-native-indicators";

class LandingScreen extends Component {

    render () {
        return (
            <WebView
                startInLoadingState={true}
                renderLoading={()=>(<SkypeIndicator color={ColorDefinitions.colorPrimary}/>)}
                source={{ uri: 'https://www.flymango.com/#carCriteria' }}
                style={{ marginTop: 0 }}
            />
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: ColorDefinitions.mainBackground,
    },
});

const mapStateToProps = (state) => {
    return {

    }
};

const matchDispatchToProps = dispatch => {
    return {

    }
};

export default connect(mapStateToProps, matchDispatchToProps)(LandingScreen);
