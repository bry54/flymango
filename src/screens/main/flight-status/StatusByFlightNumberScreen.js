import React, {Component} from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import ColorDefinitions from "../../../constants/ColorDefinitions";
import {connect} from "react-redux";
import TypoDefinitions from "../../../constants/TypoDefinitions";
import {Input} from "react-native-elements";
import {getFlightStatusByFlightNumber,} from "../../../store/utilities/actionsCollection";
import moment from 'moment';
import {MaterialCommunityIcons} from '@expo/vector-icons';
import DateTimePickerModal from "react-native-modal-datetime-picker";
import {Appearance} from "react-native-appearance";
import SnackBar from "react-native-snackbar-component";
import ActionButton from "../../../components/utilities/ActionButton";
import {SCLAlert, SCLAlertButton} from "react-native-scl-alert";
import Ripple from "react-native-material-ripple";

class StatusByFlightNumberScreen extends Component {
    state ={
        departureDatePickerVisibility: false,
        departureDate: null, //'2020-02-28 14:22:55.511Z',//new Date(),
        flightNumber: null, //'MNG-550254',
        showDemoInfo: true,
        missingInformation: false,
        missingInformationMessage: ''
    };

    render () {
        const colorScheme = Appearance.getColorScheme();

        return (
            <View style={styles.container}>
                <SCLAlert
                    containerStyle={{height: 250}}
                    overlayStyle={{backgroundColor: ColorDefinitions.darkBackgroundColor.rgbaShade5}}
                    show={this.state.missingInformation}
                    onRequestClose={() => this._updateInputItem('noFlights', false)}
                    theme="danger"
                    title="Missing Information"
                    titleStyle={{ fontFamily: 'alpha-regular' }}
                    subtitle={this.state.missingInformationMessage}
                    subtitleStyle={{ fontFamily: 'alpha-regular' }}>

                    <SCLAlertButton
                        textStyle={{ fontFamily: 'alpha-regular', fontWeight: '400' }}
                        theme="info"
                        onPress={() =>
                            this._updateInputItem('missingInformation', false)
                        }>
                        Okay
                    </SCLAlertButton>
                </SCLAlert>

                <SnackBar
                    backgroundColor='rgba( 14,  6, 50, .8)'
                    messageStyle={{ }}
                    actionStyle={{ }}
                    visible={this.state.showDemoInfo}
                    textMessage={`DEMO FLIGHT SEARCH ${'\n'}${'\n'}Date   :  2020/02/28 ${'\n'}Flight :  MNG-550254`}
                    actionHandler={()=>{this._updateInputItem('showDemoInfo', false)}}
                    actionText="Dismiss"/>

                <View>
                    <DateTimePickerModal
                        isDarkModeEnabled={colorScheme === 'dark'}
                        isVisible={this.state.departureDatePickerVisibility}
                        mode="date"
                        onConfirm={async (date) => {
                            this._updateInputItem('departureDatePickerVisibility', false)
                            await this._updateInputItem('departureDate', date);
                        }}
                        onCancel={() => this._updateInputItem('departureDatePickerVisibility', false)}
                    />

                    <Ripple
                        activeOpacity={.5}
                        onPress={() => {this._updateInputItem('departureDatePickerVisibility', true)}}>
                        <Input
                            rightIcon={<MaterialCommunityIcons name={'calendar-clock'} size={20} color={ColorDefinitions.accentColor.shade0}/>}
                            disabled={true}
                            value={this.state.departureDate ? moment(this.state.departureDate).format('DD/MM/YYYY') : null}
                            containerStyle={{marginVertical:10}}
                            inputContainerStyle={{borderColor: ColorDefinitions.accentColor.shade0, borderBottomWidth:2}}
                            inputStyle={{fontFamily: 'alpha-regular'}}
                            label='Flight Date'
                            labelStyle={{fontFamily: 'alpha-regular', fontWeight: TypoDefinitions.lightText, fontSize: TypoDefinitions.xSmallFont, color: ColorDefinitions.darkText.shade4}}
                            placeholder='DD/MM/YYYY'/>
                    </Ripple>

                    <Input
                        keyboardType={'number-pad'}
                        leftIcon={<Text style={{fontFamily: 'alpha-regular', fontSize: 16, marginLeft:-10}}>MNG - </Text>}
                        rightIcon={<MaterialCommunityIcons name={'ticket-confirmation'} size={20} color={ColorDefinitions.accentColor.shade0}/>}
                        onChangeText={(val => this._updateInputItem('flightNumber', val))}
                        value={this.state.flightNumber}
                        containerStyle={{marginVertical:10}}
                        inputContainerStyle={{borderColor: ColorDefinitions.accentColor.shade0, borderBottomWidth:2}}
                        inputStyle={{fontFamily: 'alpha-regular'}}
                        label='Flight Number'
                        labelStyle={{fontFamily: 'alpha-regular', fontWeight: TypoDefinitions.lightText, fontSize: TypoDefinitions.xSmallFont, color: ColorDefinitions.darkText.shade4}}
                        placeholder='######'/>
                </View>

                <ActionButton
                    raise={!this.state.showDemoInfo}
                    hasMissingInformation={this._hasMissingInformation()}
                    compileErrors={this._compileErrors}
                    buttonAction={()=>{this._getFlightStatus()}}
                    buttonTitle='Search'/>
            </View>
        );
    }

    _hasMissingInformation = () =>{
        return (
            !this.state.departureDate ||
            !this.state.flightNumber
        )
    };

    _compileErrors = () =>{
        const message = [];
        if(!this.state.departureDate) message.push('Select Departure Date');
        if(!this.state.flightNumber) message.push('Enter Flight Number');

        this._updateInputItem('missingInformationMessage', message.join('\n'));
        this._updateInputItem('missingInformation', true)
    };

    _updateInputItem = (itemKey, itemValue) =>{
        this.setState({
            ...this.state,
            [itemKey] : itemValue
        })
    };

    _getFlightStatus = async () => {
        const {navigation} = this.props;
        const params = {
            flightNumber: 'MNG-'+this.state.flightNumber.replace(/\s+/g, ''),
            departureDate: this.state.departureDate
        };

        await this.props.getFlightStatus(params);
        navigation.navigate('SearchResults')
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'space-between',
        backgroundColor: ColorDefinitions.mainBackground,
    },
});

const mapStateToProps = (state) => {
    return {

    }
};

const matchDispatchToProps = dispatch => {
    return {
        getFlightStatus: (params) => dispatch (getFlightStatusByFlightNumber(params)),
    }
};

export default connect(mapStateToProps, matchDispatchToProps)(StatusByFlightNumberScreen);
