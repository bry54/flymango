import React, {Component} from 'react';
import {FlatList, StyleSheet, Text, View} from 'react-native';
import ColorDefinitions from "../../../constants/ColorDefinitions";
import {connect} from "react-redux";
import {Avatar, Badge, ListItem} from "react-native-elements";
import images from '../../../assets/images'
import {MaterialCommunityIcons} from "@expo/vector-icons";
import moment from "moment";

class SearchResultsScreen extends Component {

    render () {
        return (
            <View style={styles.container}>
                <FlatList
                    ListEmptyComponent={()=>(
                        <View style={{flex: 1, alignItems: 'center', paddingVertical: 50}}>
                            <Avatar
                                overlayContainerStyle={{backgroundColor: ColorDefinitions.darkBackgroundColor.shade4}}
                                containerStyle={{elevation: 5}}
                                size={"xlarge"}
                                icon={{name:'airplane-off', type:'material-community', color: ColorDefinitions.accentColor.shade0}}
                                rounded />
                            <Text style={{paddingVertical: 40, fontFamily: 'alpha-regular', fontSize: 18, textAlign: 'center'}}>
                                No scheduled flights found for the specified date
                            </Text>
                        </View>
                    )}
                    keyExtractor={(item, index) => index}
                    data={this.props.flights}
                    renderItem={this.renderItem}
                />
            </View>
        );
    }

    _navToDetailedFlightStatus = (detail) =>{
        const {navigation} = this.props;
        navigation.navigate('DetailedFlightStatus', {DETAIL: detail})
    };

    renderItem = ({ item }) => (
        <ListItem
            onPress={() => this._navToDetailedFlightStatus(item)}
            containerStyle={{paddingHorizontal: 5}}
            title={
                <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                    <View style={{alignItems: 'flex-start'}}>
                        <MaterialCommunityIcons name="airplane-takeoff" size={18} color={ColorDefinitions.darkText.shade4}/>
                        <Text style={{fontSize:12, color:ColorDefinitions.darkText.shade4, fontFamily: 'alpha-regular', textTransform: 'uppercase'}}>
                            {item.flightObj.flight.departure.airport.airport_code}
                        </Text>
                        <Text style={{fontSize:12, fontFamily: 'alpha-regular', textDecorationLine: 'none'}}>
                            {moment(item.departure.scheduled_utc_time).format('HH:mm')}
                        </Text>
                    </View>

                    <View style={{
                        backgroundColor: ColorDefinitions.darkText.shade4,
                        height: 1,
                        flex: 1,
                        alignSelf: 'center',
                        marginHorizontal: 5
                    }}/>

                    <View style={{alignItems: 'flex-start'}}>
                        <MaterialCommunityIcons name="airplane-landing" size={18} color={ColorDefinitions.darkText.shade4}/>
                        <Text style={{fontSize:12, color:ColorDefinitions.darkText.shade4, fontFamily: 'alpha-regular', textTransform: 'uppercase'}}>
                            {item.flightObj.flight.arrival.airport.airport_code}
                        </Text>
                        <Text style={{fontSize:12, fontFamily: 'alpha-regular', textDecorationLine: 'none'}}>
                            {moment(item.arrival.scheduled_utc_time).format('HH:mm')}
                        </Text>
                    </View>
                </View>
            }
            leftAvatar={
                <View style={{flexDirection: 'row'}}>
                    <Avatar rounded source={images.planes._mango_logo_01}/>
                    <View style={{paddingHorizontal: 10, alignItems: 'flex-start'}}>
                        <Text style={{fontSize: 13, fontFamily: 'alpha-regular', paddingBottom: 2}}>
                            {item.flightObj ? item.flightObj.marketing_carrier.flight_number : 'MNG-1679'}
                        </Text>
                        <Badge
                            value={item.flightObj.flight_time_status.definition}
                            status={(
                                () => {
                                    const Cancelled =  1;
                                    const Departed =  2;
                                    const Landed =  3;
                                    const Rerouted =  4;
                                    const NoStatus =  5;

                                    return "primary"
                                })
                            ()}
                            badgeStyle={{borderRadius: 2}}
                            textStyle={{fontFamily: 'alpha-regular'}}
                        />
                    </View>
                </View>
            }
            bottomDivider
            chevron={<MaterialCommunityIcons name='chevron-right' size={18}/>}
        />
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: ColorDefinitions.mainBackground,
    },
});

const mapStateToProps = (state) => {
    return {
        flights: state.flightStatusReducer.flights
    }
};

const matchDispatchToProps = dispatch => {
    return {

    }
};

export default connect(mapStateToProps, matchDispatchToProps)(SearchResultsScreen);
