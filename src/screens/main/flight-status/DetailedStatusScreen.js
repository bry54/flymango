import React, {Component} from 'react';
import {ScrollView, StyleSheet, Text, View} from 'react-native';
import ColorDefinitions from "../../../constants/ColorDefinitions";
import {connect} from "react-redux";
import {MaterialCommunityIcons} from "@expo/vector-icons";
import moment from "moment";
import {Divider} from "react-native-elements";

class DetailedStatusScreen extends Component {

    render () {
        const detail = this.props.route.params.DETAIL;
        return (
            <View style={styles.container}>
                <View style={{flexDirection: 'row', backgroundColor: ColorDefinitions.darkBackgroundColor.shade4, justifyContent: 'space-between'}}>
                    <View style={{alignItems: 'flex-start', paddingHorizontal: 10, paddingVertical: 10, elevation: 5}}>
                        <Text style={{textTransform: 'uppercase', fontFamily: 'alpha-regular', marginVertical: 3, color: ColorDefinitions.accentColor.shade0, fontSize: 14, paddingVertical: 8}}>
                            {'Flight Number'}
                        </Text>
                        <Text style={{textTransform: 'uppercase', fontFamily: 'alpha-regular', marginVertical: 5, color: ColorDefinitions.lightText.shade0, fontSize: 30}}>
                            {detail.flightObj ? detail.flightObj.marketing_carrier.flight_number : 'N/A'}
                        </Text>
                    </View>

                    <View style={{alignItems: 'flex-start', paddingHorizontal: 10, paddingVertical: 10, elevation: 5}}>
                        <Text style={{textTransform: 'uppercase', fontFamily: 'alpha-regular', marginVertical: 3, color: ColorDefinitions.accentColor.shade0, fontSize: 14, paddingVertical: 8}}>
                            {'Date'}
                        </Text>
                        <View style={{flexDirection: 'row'}} >
                            <Text style={{fontSize: 36, color: ColorDefinitions.lightText.shade0, fontFamily: 'alpha-regular', paddingRight: 6}}>
                                {
                                    moment(detail.departure.scheduled_utc_time).format('DD')
                                }
                            </Text>
                            <View style={{flexDirection: 'column', marginTop: 4}}>
                                <Text style={{color: ColorDefinitions.lightText.shade0, fontFamily: 'alpha-regular',}}>
                                    {
                                        moment(detail.departure.scheduled_utc_time).format('MMM')
                                    }
                                </Text>
                                <Text style={{color: ColorDefinitions.lightText.shade0, fontFamily: 'alpha-regular',}}>
                                    {
                                        moment(detail.departure.scheduled_utc_time).format('YYYY')
                                    }
                                </Text>
                            </View>
                        </View>
                    </View>
                </View>

                <View style={{flexDirection: 'row', justifyContent: 'flex-start'}}>
                    <View style={{paddingHorizontal: 25, elevation: 0,
                        backgroundColor: (
                            () => {
                                const early =  1;
                                const next =  2;
                                const onTime =  3;
                                const delayed =  4;
                                const noStatus =  5;

                                if (detail.flightObj.flight_time_status.id === early) return "green"
                                if (detail.flightObj.flight_time_status.id === next) return "yellow"
                                if (detail.flightObj.flight_time_status.id === onTime) return "green"
                                if (detail.flightObj.flight_time_status.id === delayed) return "red"
                                if (detail.flightObj.flight_time_status.id === noStatus) return "red"
                            }) (),
                        width: '25%'}}>
                        <MaterialCommunityIcons name='progress-clock' size={30} style={{paddingVertical: 15}} color={ColorDefinitions.lightText.shade0}/>
                    </View>

                    <View style={{paddingHorizontal: 10, paddingVertical: 20, elevation: 0, backgroundColor: '#ccc', width: '75%'}}>
                        <Text style={{textTransform: 'uppercase', fontFamily: 'alpha-regular',
                            color: (
                                () => {
                                    const early =  1;
                                    const next =  2;
                                    const onTime =  3;
                                    const delayed =  4;
                                    const noStatus =  5;

                                    if (detail.flightObj.flight_time_status.id === early) return "green"
                                    if (detail.flightObj.flight_time_status.id === next) return "yellow"
                                    if (detail.flightObj.flight_time_status.id === onTime) return "green"
                                    if (detail.flightObj.flight_time_status.id === delayed) return "red"
                                    if (detail.flightObj.flight_time_status.id === noStatus) return "red"
                                }) (),
                            fontSize: 20, textAlign: 'center'}}>
                            {detail.flightObj.flight_time_status.definition}
                        </Text>
                    </View>
                </View>

                <View>
                    <View style={{flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal: 10, paddingVertical: 10}}>
                        <View>
                            <Text style={{fontSize:30, fontFamily: 'alpha-regular', paddingVertical: 2, color: ColorDefinitions.accentColor.shade0}}>
                                {detail.flightObj.flight.departure.airport.airport_code}
                            </Text>
                            <Text style={{fontSize:13, fontFamily: 'alpha-regular', paddingVertical: 2}}>
                                {detail.flightObj.flight.departure.airport.name.replace('International','Intl')}
                            </Text>
                            <Text style={{fontSize:13, fontFamily: 'alpha-regular', paddingVertical: 2}}>
                                Scheduled: {moment(detail.departure.scheduled_utc_time).format('HH:mm')}
                            </Text>
                            <Text style={{fontSize:13, fontFamily: 'alpha-regular', paddingVertical: 2}}>
                                Actual: {moment(detail.departure.actual_utc_time).format('HH:mm')}
                            </Text>
                        </View>

                        <MaterialCommunityIcons name='airplane-takeoff' size={30} style={{paddingVertical: 5}} color={ColorDefinitions.accentColor.shade0}/>

                        <View>
                            <Text style={{fontSize:30, fontFamily: 'alpha-regular', paddingVertical: 2, color: ColorDefinitions.accentColor.shade0}}>
                                {detail.flightObj.flight.arrival.airport.airport_code}
                            </Text>
                            <Text style={{fontSize:13, fontFamily: 'alpha-regular', paddingVertical: 2}}>
                                {detail.flightObj.flight.arrival.airport.name.replace('International','Intl')}
                            </Text>
                            <Text style={{fontSize:13, fontFamily: 'alpha-regular', paddingVertical: 2}}>
                                Scheduled: {moment(detail.arrival.scheduled_utc_time).format('HH:mm')}
                            </Text>
                            <Text style={{fontSize:13, fontFamily: 'alpha-regular', paddingVertical: 2}}>
                                Actual: {moment(detail.arrival.actual_utc_time).format('HH:mm')}</Text>
                        </View>
                    </View>
                    <Divider style={{backgroundColor: ColorDefinitions.darkText.shade4, marginVertical: 10}}/>
                </View>

                <ScrollView>
                    <View>
                        <Text style={{paddingHorizontal: 10, textTransform: 'uppercase'}}>Departure Information</Text>
                        <View style={{
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            paddingHorizontal: 20,
                            paddingVertical: 10
                        }}>
                            <View style={{}}>
                                <Text style={{
                                    textTransform: 'capitalize',
                                    fontSize: 16,
                                    fontFamily: 'alpha-regular',
                                    paddingVertical: 2,
                                    color: ColorDefinitions.darkText.shade4
                                }}>Terminal</Text>
                                <Text
                                    style={{fontSize: 14, fontFamily: 'alpha-regular', paddingVertical: 2, color: 'grey'}}>
                                    {detail.flightObj.flight.departure.terminal.name}
                                </Text>
                            </View>

                            <View style={{}}>
                                <Text style={{
                                    textTransform: 'capitalize',
                                    fontSize: 16,
                                    fontFamily: 'alpha-regular',
                                    paddingVertical: 2,
                                    color: ColorDefinitions.darkText.shade4
                                }}>Gate Number</Text>
                                <Text style={{fontSize: 14, fontFamily: 'alpha-regular', paddingVertical: 2, color: 'grey'}}>
                                    {detail.flightObj.flight.departure.terminal.gate}
                                </Text>
                            </View>
                        </View></View>

                    <View>
                        <Text style={{paddingHorizontal: 10, textTransform: 'uppercase'}}>Arrival Information</Text>
                        <View style={{
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            paddingHorizontal: 20,
                            paddingVertical: 10
                        }}>
                            <View style={{}}>
                                <Text style={{
                                    textTransform: 'capitalize',
                                    fontSize: 16,
                                    fontFamily: 'alpha-regular',
                                    paddingVertical: 2,
                                    color: ColorDefinitions.darkText.shade4
                                }}>Terminal</Text>
                                <Text style={{fontSize: 14, fontFamily: 'alpha-regular', paddingVertical: 2, color: 'grey'}}>
                                    {detail.flightObj.flight.arrival.terminal.name}
                                </Text>
                            </View>

                            <View style={{}}>
                                <Text style={{
                                    textTransform: 'capitalize',
                                    fontSize: 16,
                                    fontFamily: 'alpha-regular',
                                    paddingVertical: 2,
                                    color: ColorDefinitions.darkText.shade4
                                }}>Gate Number</Text>
                                <Text
                                    style={{fontSize: 14, fontFamily: 'alpha-regular', paddingVertical: 2, color: 'grey'}}>
                                    {detail.flightObj.flight.arrival.gate ? detail.flightObj.flight.arrival.gate : 'N/A'}
                                </Text>
                            </View>
                        </View></View>

                    <View style={{flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal: 20, paddingVertical: 10}}>
                        <View style={{}}>
                            <Text style={{textTransform: 'capitalize',fontSize:16, fontFamily: 'alpha-regular', paddingVertical: 2, color: ColorDefinitions.darkText.shade4}}>Flight Distance</Text>
                            <Text style={{fontSize:14, fontFamily: 'alpha-regular', paddingVertical: 2, color: 'grey'}}>
                                10000 KM
                            </Text>
                        </View>

                        <View style={{}}>
                            <Text style={{textTransform: 'capitalize',fontSize:16, fontFamily: 'alpha-regular', paddingVertical: 2, color: ColorDefinitions.darkText.shade4}}>Flight Duration</Text>
                            <Text style={{fontSize:14, fontFamily: 'alpha-regular', paddingVertical: 2, color: 'grey'}}>
                                {this._getFlightDuration(detail.departure.actual_utc_time, detail.arrival.actual_utc_time)}
                            </Text>
                        </View>
                    </View>

                    <View style={{flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal: 20, paddingVertical: 10}}>
                        <View style={{}}>
                            <Text style={{textTransform: 'capitalize',fontSize:16, fontFamily: 'alpha-regular', paddingVertical: 2, color: ColorDefinitions.darkText.shade4}}>Operated By</Text>
                            <Text style={{fontSize:14, fontFamily: 'alpha-regular', paddingVertical: 2, color: 'grey'}}>
                                {detail.flightObj.marketing_carrier.organization.name.en}
                            </Text>
                        </View>
                        <View style={{}}>
                            <Text style={{textTransform: 'capitalize',fontSize:16, fontFamily: 'alpha-regular', paddingVertical: 2, color: ColorDefinitions.darkText.shade4}}>Aircraft Type</Text>
                            <Text style={{fontSize:14, fontFamily: 'alpha-regular', paddingVertical: 2, color: 'grey'}}>
                                {detail.flightObj.equipment.aircraft.name.en + ' ' +detail.flightObj.equipment.aircraft.aircraft_code}
                            </Text>
                        </View>
                    </View>
                </ScrollView>
            </View>
        );
    }

    _getFlightDuration =(departure, arrival)=>{
        const arrTime = moment(arrival, 'YYYY-MM-DD HH:mm:ss');
        const departTime = moment(departure, 'YYYY-MM-DD HH:mm:ss');
        const diff = arrTime.diff(departTime);
        const duration = moment.duration(diff);

        const hrs = duration.hours();
        const mins = duration.minutes();

        return `${hrs}hrs ${mins}mins`
    };
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
});

const mapStateToProps = (state) => {
    return {

    }
};

const matchDispatchToProps = dispatch => {
    return {

    }
};

export default connect(mapStateToProps, matchDispatchToProps)(DetailedStatusScreen);
