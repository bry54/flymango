import React, {Component} from 'react';
import {StyleSheet, TouchableOpacity, View} from 'react-native';
import ColorDefinitions from "../../../constants/ColorDefinitions";
import {connect} from "react-redux";
import {getFlightStatusByRoute} from "../../../store/utilities/actionsCollection";
import moment from 'moment';
import ActionButton from "../../../components/utilities/ActionButton";
import AirportsModal from "../../../components/utilities/AirportsModal";
import {Appearance} from "react-native-appearance";
import DateTimePickerModal from "react-native-modal-datetime-picker";
import {Input} from "react-native-elements";
import TypoDefinitions from "../../../constants/TypoDefinitions";
import SnackBar from "react-native-snackbar-component";
import {MaterialCommunityIcons} from "@expo/vector-icons";
import {SCLAlert, SCLAlertButton} from "react-native-scl-alert";
import Ripple from "react-native-material-ripple";

class StatusByRouteScreen extends Component {
    state ={
        departureDatePickerVisibility: false,
        originAirportsVisibility: false,
        destinationAirportsVisibility: false,
        departureDate: null,
        flightNumber: '',
        origin: null,
        destination: null,
        airportSearch: '',
        showDemoInfo: true,
        missingInformation: false,
        missingInformationMessage: ''
    };

    render () {
        const colorScheme = Appearance.getColorScheme();
        const airports = this.props.airports;

        return (
            <View style={styles.container}>
                <SCLAlert
                    overlayStyle={{backgroundColor: ColorDefinitions.darkBackgroundColor.rgbaShade5}}
                    show={this.state.missingInformation}
                    onRequestClose={() => this._updateInputItem('noFlights', false)}
                    theme="danger"
                    title="Missing Information"
                    titleStyle={{ fontFamily: 'alpha-regular' }}
                    subtitle={this.state.missingInformationMessage}
                    subtitleStyle={{ fontFamily: 'alpha-regular' }}>

                    <SCLAlertButton
                        textStyle={{ fontFamily: 'alpha-regular', fontWeight: '400' }}
                        theme="info"
                        onPress={() =>
                            this._updateInputItem('missingInformation', false)
                        }>
                        Okay
                    </SCLAlertButton>
                </SCLAlert>
                <SnackBar
                    backgroundColor='rgba( 14,  6, 50, .8)'
                    messageStyle={{ }}
                    actionStyle={{ }}
                    visible={this.state.showDemoInfo}
                    textMessage={`DEMO FLIGHT SEARCH ${'\n'}${'\n'}Date   :  28/02/2020 ${'\n'}From  :  BNF ${'\n'}To       :  HLA`}
                    actionHandler={()=>{this._updateInputItem('showDemoInfo', false)}}
                    actionText="Dismiss"/>
                <View>
                    <DateTimePickerModal
                        isDarkModeEnabled={colorScheme === 'dark'}
                        isVisible={this.state.departureDatePickerVisibility}
                        mode="date"
                        onConfirm={async (date) => {
                            this._updateInputItem('departureDatePickerVisibility', false)
                            await this._updateInputItem('departureDate', date);
                        }}
                        onCancel={() => this._updateInputItem('departureDatePickerVisibility', false)}
                    />

                    <AirportsModal
                        airportSearch={this.state.airportSearch}
                        updateItemVisibility={() => this._updateInputItem('destinationAirportsVisibility', false)}
                        updateInputItem={this._updateInputItem}
                        airports={ airports.filter( item => {
                            return item.airport_code.toLowerCase().includes(this.state.airportSearch.toLowerCase()) ||
                                item.name.toLowerCase().includes(this.state.airportSearch.toLowerCase()) ||
                                item.city.name.toLowerCase().includes(this.state.airportSearch.toLowerCase()) ||
                                item.city.code.toLowerCase().includes(this.state.airportSearch.toLowerCase()) ||
                                item.province.name.toLowerCase().includes(this.state.airportSearch.toLowerCase())||
                                item.province.code.toLowerCase().includes(this.state.airportSearch.toLowerCase())
                        })}
                        modalVisible={this.state.destinationAirportsVisibility}
                        airportMode={'destination'} />

                    <AirportsModal
                        airportSearch={this.state.airportSearch}
                        updateItemVisibility={() => this._updateInputItem('originAirportsVisibility', false)}
                        updateInputItem={this._updateInputItem}
                        airports={ airports.filter( item => {
                            return item.airport_code.toLowerCase().includes(this.state.airportSearch.toLowerCase()) ||
                                item.name.toLowerCase().includes(this.state.airportSearch.toLowerCase()) ||
                                item.city.name.toLowerCase().includes(this.state.airportSearch.toLowerCase()) ||
                                item.city.code.toLowerCase().includes(this.state.airportSearch.toLowerCase()) ||
                                item.province.name.toLowerCase().includes(this.state.airportSearch.toLowerCase())||
                                item.province.code.toLowerCase().includes(this.state.airportSearch.toLowerCase())
                        })}
                        modalVisible={this.state.originAirportsVisibility}
                        airportMode={'origin'} />

                    <Ripple
                        activeOpacity={.9}
                        onPress={() => {this._updateInputItem('departureDatePickerVisibility', true)}}>
                        <Input
                            rightIcon={<MaterialCommunityIcons name={'calendar'} size={20} color={ColorDefinitions.accentColor.shade0}/>}
                            disabled={true}
                            value={this.state.departureDate ? moment(this.state.departureDate).format('DD/MM/YYYY') : null}
                            containerStyle={{marginVertical:10}}
                            inputContainerStyle={{borderColor: ColorDefinitions.accentColor.shade0, borderBottomWidth:2}}
                            inputStyle={{fontFamily: 'alpha-regular'}}
                            label='Flight Date'
                            labelStyle={{fontFamily: 'alpha-regular', fontWeight: TypoDefinitions.lightText, fontSize: TypoDefinitions.xSmallFont, color: ColorDefinitions.darkText.shade4}}
                            placeholder='DD/MM/YYYY'/>
                    </Ripple>

                    <Ripple
                        activeOpacity={.9}
                        onPress={() => {this._updateInputItem('originAirportsVisibility', true)}}>
                        <Input
                            rightIcon={<MaterialCommunityIcons name={'airport'} size={20} color={ColorDefinitions.accentColor.shade0}/>}
                            disabled={true}
                            value={this.state.origin ? this.state.origin.name : null}
                            containerStyle={{marginVertical:10}}
                            inputContainerStyle={{borderColor: ColorDefinitions.accentColor.shade0, borderBottomWidth:2}}
                            inputStyle={{fontFamily: 'alpha-regular'}}
                            label='Origin (From)'
                            labelStyle={{fontFamily: 'alpha-regular', fontWeight: TypoDefinitions.lightText, fontSize: TypoDefinitions.xSmallFont, color: ColorDefinitions.darkText.shade4}}
                            placeholder='Select Origin Airport (From)'/>
                    </Ripple>

                    <Ripple
                        activeOpacity={.9}
                        onPress={() => {this._updateInputItem('destinationAirportsVisibility', true)}}>
                        <Input
                            rightIcon={<MaterialCommunityIcons name={'airport'} size={20} color={ColorDefinitions.accentColor.shade0}/>}
                            disabled={true}
                            value={this.state.destination ? this.state.destination.name : null}
                            containerStyle={{marginVertical:10}}
                            inputContainerStyle={{borderColor: ColorDefinitions.accentColor.shade0, borderBottomWidth:2}}
                            inputStyle={{fontFamily: 'alpha-regular'}}
                            label='Destination (To)'
                            labelStyle={{fontFamily: 'alpha-regular', fontWeight: TypoDefinitions.lightText, fontSize: TypoDefinitions.xSmallFont, color: ColorDefinitions.darkText.shade4}}
                            placeholder='Select Destination Airport (To)'/>
                    </Ripple>
                </View>
                <ActionButton
                    hasMissingInformation={this._hasMissingInformation()}
                    compileErrors={this._compileErrors}
                    buttonAction={()=>{this._getFlightStatus()}}
                    buttonTitle='Search'/>
            </View>
        );
    }

    _hasMissingInformation = () =>{
        return (
            !this.state.departureDate ||
            !this.state.origin ||
            !this.state.destination
        )
    };

    _compileErrors = () =>{
        const message = [];
        if(!this.state.departureDate) message.push('Select Departure Date');
        if(!this.state.origin) message.push('Select Origin');
        if(!this.state.destination) message.push('Select Destination');

        this._updateInputItem('missingInformationMessage', message.join('\n'));
        this._updateInputItem('missingInformation', true);
    };

    _updateInputItem = (itemKey, itemValue) =>{
        this.setState({
            ...this.state,
            [itemKey] : itemValue
        })
    };

    _getFlightStatus = async () => {
        const {navigation} = this.props;
        const params = {
            origin: this.state.origin,
            destination: this.state.destination,
            departureDate: this.state.departureDate
        };

        await this.props.getFlightStatus(params);
        navigation.navigate('SearchResults')
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'space-between',
        backgroundColor: ColorDefinitions.mainBackground,
    },
});

const mapStateToProps = (state) => {
    return {
        airports: state.buyTicketsReducer.airports
    }
};

const matchDispatchToProps = dispatch => {
    return {
        getFlightStatus: (params) => dispatch (getFlightStatusByRoute(params)),
    }
};

export default connect(mapStateToProps, matchDispatchToProps)(StatusByRouteScreen);
