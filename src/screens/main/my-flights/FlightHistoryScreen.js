import React, {Component} from 'react';
import {ScrollView, StyleSheet, Text, View} from 'react-native';
import ColorDefinitions from "../../../constants/ColorDefinitions";
import {connect} from "react-redux";
import {Avatar, Input, ListItem} from "react-native-elements";
import {MaterialCommunityIcons} from "@expo/vector-icons";
import moment from "moment";
import TypoDefinitions from "../../../constants/TypoDefinitions";
import Ripple from "react-native-material-ripple";

class FlightHistoryScreen extends Component {
    state = {
        flightSearch: ''
    };

    componentDidMount() {

    }

    render () {
        const flights = this.props.myFlights.historicalFlights;
        const filteredFlights = flights.filter( item => {
            return item.flight.departure.airport.city.name.toLowerCase().includes(this.state.flightSearch.toLowerCase()) ||
                item.flight.arrival.airport.city.name.toLowerCase().includes(this.state.flightSearch.toLowerCase()) ||
                item.flight.departure.airport.airport_code.toLowerCase().includes(this.state.flightSearch.toLowerCase()) ||
                item.flight.arrival.airport.airport_code.toLowerCase().includes(this.state.flightSearch.toLowerCase())
        });

        return (
            <View style={styles.container}>
                { (!flights.length) ?
                    (
                        <View style={{flex: 1, alignItems: 'center', paddingVertical: 50}}>
                            <Avatar
                                overlayContainerStyle={{backgroundColor: ColorDefinitions.darkBackgroundColor.shade4}}
                                containerStyle={{elevation: 5}}
                                size={"xlarge"}
                                icon={{name:'flag-checkered', type:'material-community', color: ColorDefinitions.accentColor.shade0}}
                                rounded />
                            <Text style={{paddingVertical: 40, paddingHorizontal: 10,fontFamily: 'alpha-regular', fontSize: 18, textAlign: 'center'}}>
                                You haven't been in the skies with us yet, start accumulating some Hours and Miles with the best domestic airline in South Africa.
                            </Text>
                        </View>
                    ) : (
                        <View style={{marginTop: 5, flex: 1}}>
                            <Input
                                rightIcon={
                                    this.state.flightSearch ? (
                                        <Ripple onPress={()=>this.updateInputItem('flightSearch', '')}>
                                            <MaterialCommunityIcons name={'close'} size={25} color={ColorDefinitions.accentColor.shade0}/>
                                        </Ripple>
                                    ) : (
                                        <MaterialCommunityIcons name={'database-search'} size={20} color={ColorDefinitions.accentColor.shade0}/>
                                    )
                                }
                                onChangeText={ (text)=>this.updateInputItem('flightSearch', text)}
                                value={this.state.flightSearch}
                                containerStyle={{ marginBottom: 0 }}
                                inputContainerStyle={{borderColor: ColorDefinitions.accentColor.shade0, borderBottomWidth:0, paddingVertical: 2.5}}
                                labelStyle={{fontFamily: 'alpha-regular', fontWeight: TypoDefinitions.lightText, fontSize: TypoDefinitions.xSmallFont, color: ColorDefinitions.darkText.shade4}}
                                inputStyle={{fontFamily: 'alpha-regular'}}
                                placeholder='Quick Search A Trip' />

                            <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={{ }}>
                                { filteredFlights.map( flight => (
                                    this.renderItem(flight)
                                ))}
                            </ScrollView>
                        </View>
                    )}
                <View style={{elevation:5,paddingHorizontal: 10, paddingVertical: 10, backgroundColor: ColorDefinitions.darkBackgroundColor.shade4, flexDirection: 'row', justifyContent: 'space-between'}}>
                    <View>
                        <Text style={{textAlign: 'left', fontFamily: 'alpha-regular', paddingVertical: 2, fontSize: 15, color: ColorDefinitions.accentColor.shade0}}>TOTAL DISTANCE</Text>
                        <Text style={{textAlign: 'left', fontFamily: 'alpha-regular', paddingVertical: 2, fontSize: 15, color: ColorDefinitions.lightText.shade0}}>
                            {this._getTotalDistance() + ' KM'}
                        </Text>
                    </View>

                    <View>
                        <Text style={{textAlign: 'left', fontFamily: 'alpha-regular', paddingVertical: 2, fontSize: 15, color: ColorDefinitions.accentColor.shade0}}>TOTAL HOURS</Text>
                        <Text style={{textAlign: 'left', fontFamily: 'alpha-regular', paddingVertical: 2, fontSize: 15, color: ColorDefinitions.lightText.shade0}}>
                            {this._getTotalDuration() + ' Hrs'}
                        </Text>
                    </View>
                </View>
            </View>
        );
    }

    renderItem = (item) => {
        return (
            <Ripple rippleOpacity={.8} onPress={ () => { }} key={JSON.stringify(item)}>
                <ListItem
                    leftAvatar={{
                        size: "medium",
                        title: item.flight.arrival.airport.airport_code,
                        titleStyle: {fontFamily: 'alpha-regular', color: ColorDefinitions.accentColor.shade0, fontSize: 20},
                        //icon: {name:'paper-plane', type:'font-awesome', color: ColorDefinitions.accentColor.shade0},
                        overlayContainerStyle: {backgroundColor: ColorDefinitions.darkBackgroundColor.shade4},
                        rounded: true
                    }}
                    title={
                        <View style={{ paddingVertical: 5, flexDirection: 'row', justifyContent: 'flex-start'}}>
                            <Text style={{paddingVertical: 3, fontFamily: 'alpha-regular', color: ColorDefinitions.darkText.shade4}}>
                                {item.flight.departure.airport.city.name.toUpperCase()}
                            </Text>
                            <MaterialCommunityIcons style={{paddingHorizontal: 20}} name={'airplane-takeoff'} size={20} color= {ColorDefinitions.darkText.shade4}/>
                            <Text style={{paddingVertical: 3, fontFamily: 'alpha-regular', color: ColorDefinitions.darkText.shade4, flexShrink: 1}}>
                                {item.flight.arrival.airport.city.name.toUpperCase()}
                            </Text>
                        </View>
                    }
                    subtitle={
                        <View>
                            <Text style={{fontFamily: 'alpha-regular', color: ColorDefinitions.darkText.shade4}}>
                                {moment(item.flight.departure.scheduled_time).format('dddd DD MMMM YYYY')}
                            </Text>
                            {/*<View style={{ paddingVertical: 5, flexDirection: 'row', justifyContent: 'space-between'}}>
                                <Text style={{fontFamily: 'alpha-regular', color: ColorDefinitions.darkText.shade4}}>{item.distance}
                                    {(Math.floor(Math.random() * (3500 - 500) ) + 500)} KM
                                </Text>
                                <Text style={{fontFamily: 'alpha-regular', color: ColorDefinitions.darkText.shade4}}>{item.duration}
                                    {(Math.floor(Math.random() * (2 - 4) ) + 4)} Hrs
                                </Text>
                            </View>*/}
                        </View>
                    }
                    subtitleStyle={{fontFamily: 'alpha-regular'}}
                    containerStyle={{elevation: 0, marginBottom: 2, backgroundColor: ColorDefinitions.accentColor.shade0,}}
                    bottomDivider={false}
                />
            </Ripple>
        )
    };

    updateInputItem = (itemKey, itemValue) =>{
        this.setState({
            ...this.state,
            [itemKey] : itemValue
        })
    };

    _getTotalDuration = () =>{
        const flights = this.props.myFlights.historicalFlights;
        let total = 0;
        flights.forEach(f => total += (Math.floor(Math.random() * (4 - 2) ) + 2));
        return total
    };

    _getTotalDistance = () =>{
        const flights = this.props.myFlights.historicalFlights;
        let total = 0;
        flights.forEach(f => total += (Math.floor(Math.random() * (3500 - 500) ) + 500));
        return total;
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: ColorDefinitions.mainBackground,
    },
});

const mapStateToProps = (state) => {
    return {
        myFlights: state.buyTicketsReducer.flattenedTickets,
    }
};

const matchDispatchToProps = dispatch => {
    return {

    }
};

export default connect(mapStateToProps, matchDispatchToProps)(FlightHistoryScreen);
