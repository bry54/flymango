import React, {Component} from 'react';
import {ScrollView, StyleSheet, Text, View} from 'react-native';
import ColorDefinitions from "../../../constants/ColorDefinitions";
import {connect} from "react-redux";
import {Avatar, ListItem} from "react-native-elements";
import {MaterialCommunityIcons, MaterialIcons} from "@expo/vector-icons";
import moment from "moment";
import {Accordion} from "native-base";

class UpcomingFlightsScreen extends Component {
    swipeable = null;
    state = {
        isSwiping: false
    };

    render () {
        const swipeActions = [
            <View style={{flex: 1, justifyContent: 'space-evenly'}}>
            <MaterialCommunityIcons name='information' size={25} color='blue'/>
            <MaterialCommunityIcons name='trash-can' size={25} color='red'/>
            </View>
        ];

        const flights = this.props.myFlights;
        if (!flights.length)
            return (
                <View style={{flex: 1, alignItems: 'center', paddingVertical: 50}}>
                    <Avatar
                        overlayContainerStyle={{backgroundColor: ColorDefinitions.darkBackgroundColor.shade4}}
                        containerStyle={{elevation: 5}}
                        size={"xlarge"}
                        icon={{name:'view-list', type:'material-community', color: ColorDefinitions.accentColor.shade0}}
                        rounded />
                    <Text style={{paddingVertical: 40, paddingHorizontal: 10,fontFamily: 'alpha-regular', fontSize: 18, textAlign: 'center'}}>
                        You dont have any upcoming flights.
                    </Text>
                </View>
            );

        return (
            <ScrollView style={styles.container} showsVerticalScrollIndicator={false} scrollEnabled={!this.state.isSwiping}>

                { flights.map( flight => {
                    const today = moment().startOf("day");
                    const departDate = moment(flight.outBoundFlight.flight.departure.scheduled_time).startOf('day');
                    let toAndFro = [];

                    if (departDate.isSameOrAfter(today)) {
                        toAndFro.push({
                            title: 'OUTBOUND',
                            outBoundFlight: flight.outBoundFlight,
                            outBoundCabin: flight.outBoundCabin,
                            passengers: flight.passengers
                        });
                    }

                    if (flight.inBoundFlight) {
                        const returnDate = moment(flight.inBoundFlight.flight.departure.scheduled_time).startOf('day');
                        if (returnDate.isSameOrAfter(today)) {
                            toAndFro.push({
                                title: 'INBOUND',
                                inBoundFlight: flight.inBoundFlight,
                                inBoundCabin: flight.inBoundCabin,
                                passengers: flight.passengers
                            })
                        }
                    }

                    return (
                        toAndFro.length ?
                            <ListItem
                                key={JSON.stringify(flight)}
                                containerStyle={{backgroundColor: ColorDefinitions.darkBackgroundColor.shade4, padding: 0, marginBottom: 10, elevation: 3, marginHorizontal: 5, marginTop: 5, borderRadius: 5}}
                                title={
                                    <View style={{flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal:10, paddingVertical: 10, backgroundColor: 'transparent'}}>
                                        <View style={{}}>
                                            <Text style={{fontFamily: 'alpha-regular', paddingVertical:5, color: ColorDefinitions.accentColor.shade0}}>
                                                {flight.outBoundFlight.flight.departure.airport.airport_code}
                                            </Text>
                                            <Text style={{fontFamily: 'alpha-regular', paddingVertical:5, color: ColorDefinitions.lightText.shade0}}>
                                                {flight.outBoundFlight.flight.departure.airport.city.name}
                                            </Text>
                                        </View>
                                        <MaterialCommunityIcons
                                            name={flight.inBoundFlight ? 'swap-horizontal' : 'arrow-right-bold'} size={26} style={{paddingVertical: 12}} color={ColorDefinitions.accentColor.shade0}/>
                                        <View style={{}}>
                                            <Text style={{fontFamily: 'alpha-regular', paddingVertical:5, color: ColorDefinitions.accentColor.shade0}}>
                                                {flight.outBoundFlight.flight.arrival.airport.airport_code}
                                            </Text>
                                            <Text style={{fontFamily: 'alpha-regular', paddingVertical:5, color: ColorDefinitions.lightText.shade0}}>
                                                {flight.outBoundFlight.flight.arrival.airport.city.name}
                                            </Text>
                                        </View>
                                    </View>
                                }
                                subtitle={
                                    <Accordion
                                        style={{borderRadius: 0}}
                                        animation={true}
                                        dataArray={toAndFro}
                                        renderHeader={(item, expanded)=> this._renderHeader(item, expanded)}
                                        renderContent={(item) => this._renderContent(item)}
                                    />
                                }
                                bottomDivider={true}
                            /> : null
                    )
                })}
            </ScrollView>
        );
    }

    _renderHeader(item, expanded) {
        return (
            <View style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                backgroundColor: ColorDefinitions.accentColor.shade0,
                elevation: 2}}>
                <View style={{flexDirection: 'row'}}>
                    <Text style={{padding: 10, textTransform: 'uppercase', fontFamily: 'alpha-regular', fontSize: 15, color: ColorDefinitions.darkText.shade4}}>
                        {item.title}
                    </Text>
                    <MaterialCommunityIcons
                        name={item.title === 'OUTBOUND' ? 'airplane-takeoff' : 'airplane-landing'}
                        size={20}
                        color={ColorDefinitions.darkText.shade4}
                        style={{margin: 8, transform: item.title === 'OUTBOUND' ? [{ scaleX: 1  }] : [{ scaleX: -1 }] }}/>

                    <Text style={{padding: 10, textTransform: 'uppercase', fontFamily: 'alpha-regular', fontSize: 15, color: ColorDefinitions.darkText.shade4}}>
                        {item.title === 'OUTBOUND' ? item.outBoundFlight.marketing_carrier.flight_number : item.inBoundFlight.marketing_carrier.flight_number}
                    </Text>
                </View>
                <View style={{alignItems: 'center'}}>
                    {expanded ?
                        <MaterialIcons name="expand-less" size={25} color={ColorDefinitions.darkText.shade4} style={{paddingVertical: 10}}/> :
                        <MaterialIcons name="expand-more" size={25} color={ColorDefinitions.darkText.shade4} style={{paddingVertical: 10}}/>}
                </View>
            </View>
        )
    }

    _renderContent(item) {
        return (
            <View style={{flexDirection: 'row', justifyContent: 'space-between', margin: 5}}>
                <View style={{backgroundColor: 'transparent', paddingHorizontal: 15, paddingVertical: 10}}>
                    <Text style={{color: ColorDefinitions.accentColor.shade0, fontFamily: 'alpha-regular'}}>Departure Date</Text>
                    <Text style={{color: ColorDefinitions.lightText.shade0, fontFamily: 'alpha-regular', fontSize: 15}}>
                        {moment(item.title === 'OUTBOUND' ? item.outBoundFlight.flight.departure.scheduled_time: item.inBoundFlight.flight.departure.scheduled_time).format('DD.MM.YYYY')}
                    </Text>
                    <Text style={{color: ColorDefinitions.lightText.shade0, fontFamily: 'alpha-regular', fontSize: 15}}>
                        @{moment(item.title === 'OUTBOUND' ?
                        item.outBoundFlight.flight.departure.scheduled_time :
                        item.inBoundFlight.flight.departure.scheduled_time)
                        .format('HH:mm')}
                    </Text>
                </View>
                <View style={{backgroundColor: 'transparent', paddingHorizontal: 15, paddingVertical: 10}}>
                    <Text style={{color: ColorDefinitions.accentColor.shade0, fontFamily: 'alpha-regular'}}>Arrival Date</Text>
                    <Text style={{color: ColorDefinitions.lightText.shade0, fontFamily: 'alpha-regular', fontSize: 15}}>
                        {moment(item.title === 'OUTBOUND' ? item.outBoundFlight.flight.arrival.scheduled_time : item.inBoundFlight.flight.arrival.scheduled_time).format('DD.MM.YYYY')}
                    </Text>
                    <Text style={{color: ColorDefinitions.lightText.shade0, fontFamily: 'alpha-regular', fontSize: 15}}>
                        @{moment(item.title === 'OUTBOUND' ? item.outBoundFlight.flight.arrival.scheduled_time : item.inBoundFlight.flight.arrival.scheduled_time).format('HH:mm')}
                    </Text>
                </View>
                <View style={{backgroundColor: 'transparent', paddingHorizontal: 15, paddingVertical: 10}}>
                    <Text style={{color: ColorDefinitions.accentColor.shade0, fontFamily: 'alpha-regular'}}>Terminal</Text>
                    <Text style={{color: ColorDefinitions.lightText.shade0, fontFamily: 'alpha-regular', fontSize: 15}}>
                        Terminal: {item.title === 'OUTBOUND' ? item.outBoundFlight.flight.departure.terminal.name : item.inBoundFlight.flight.departure.terminal.name}
                    </Text>
                    <Text style={{color: ColorDefinitions.lightText.shade0, fontFamily: 'alpha-regular', fontSize: 15}}>
                        Gate: {item.title === 'OUTBOUND' ? item.outBoundFlight.flight.departure.terminal.gate : item.inBoundFlight.flight.departure.terminal.gate}
                    </Text>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: ColorDefinitions.mainBackground,
    },
});

const mapStateToProps = (state) => {
    return {
        myFlights: state.buyTicketsReducer.savedTickets,
    }
};

const matchDispatchToProps = dispatch => {
    return {

    }
};

export default connect(mapStateToProps, matchDispatchToProps)(UpcomingFlightsScreen);
