import React, {Component} from 'react';
import {ScrollView, StyleSheet, Switch, Text, View} from 'react-native';
import ColorDefinitions from "../../../constants/ColorDefinitions";
import {connect} from "react-redux";
import {Divider, ListItem} from "react-native-elements";
import {AntDesign} from "@expo/vector-icons";
import SectionHeader from "../../../components/settings/SectionHeader";
import SaveButton from "../../../components/settings/SaveButton";

class PrivacyScreen extends Component {
    state ={
        receivePromo: false,
        functionalCookies: true,
        marketingCookies: true,
    };

    render () {

        return (
            <ScrollView
                showsHorizontalScrollIndicator={false}
                showsVerticalScrollIndicator={false}
                contentContainerStyle={styles.container}>
                <View>
                    <SectionHeader
                        sectionTitle='Privacy Settings'
                        sectionDescription='Use this section to controls and limit what information can be anonymously collected from your device.'/>

                    <View style={{marginHorizontal: 10, marginVertical: 10}}>
                        <View style={{flexDirection: 'row', justifyContent: 'space-between', marginVertical: 10}}>
                            <View>
                                <Text style={{
                                    paddingVertical: 2,
                                    fontFamily: 'alpha-regular',
                                    color: ColorDefinitions.darkText.shade4,
                                    fontSize: 16
                                }}>Performance Cookies</Text>
                                <Text style={{paddingVertical: 2, fontFamily: 'alpha-regular', color: 'grey'}}>You will
                                    receive promotional information</Text>
                            </View>
                            <Switch
                                value={this.state.receivePromo}
                                onValueChange={(value) => {
                                    this._updateInputControl('receivePromo', value)
                                }}/>
                        </View>
                        <Divider/>

                        <View style={{flexDirection: 'row', justifyContent: 'space-between', marginVertical: 10}}>
                            <View>
                                <Text style={{
                                    paddingVertical: 2,
                                    fontFamily: 'alpha-regular',
                                    color: ColorDefinitions.darkText.shade4,
                                    fontSize: 16
                                }}>Functional Cookies</Text>
                                <Text style={{paddingVertical: 2, fontFamily: 'alpha-regular', color: 'grey'}}>You will
                                    receive promotional information</Text>
                            </View>
                            <Switch
                                value={this.state.functionalCookies}
                                onValueChange={(value) => {
                                    this._updateInputControl('functionalCookies', value)
                                }}/>
                        </View>
                        <Divider/>

                        <View style={{flexDirection: 'row', justifyContent: 'space-between', marginVertical: 10}}>
                            <View>
                                <Text style={{
                                    paddingVertical: 2,
                                    fontFamily: 'alpha-regular',
                                    color: ColorDefinitions.darkText.shade4,
                                    fontSize: 16
                                }}>Marketing Cookies</Text>
                                <Text style={{paddingVertical: 2, fontFamily: 'alpha-regular', color: 'grey'}}>You will
                                    receive promotional information</Text>
                            </View>
                            <Switch
                                value={this.state.marketingCookies}
                                onValueChange={(value) => {
                                    this._updateInputControl('marketingCookies', value)
                                }}/>
                        </View>
                        <Divider/>
                    </View>
                    <ListItem
                        onPress={() => {

                        }}
                        title={'Read Full Terms & Conditions'}
                        titleStyle={{
                            fontFamily: 'alpha-regular',
                            color: ColorDefinitions.darkText.shade4,
                            paddingVertical: 3,
                            fontSize: 18
                        }}
                        subtitle={'This will open device settings'}
                        subtitleStyle={{fontFamily: 'alpha-regular'}}
                        rightIcon={() => (<AntDesign name="right" size={18} color={ColorDefinitions.darkText.shade4}/>)}
                        bottomDivider
                    />
                </View>
                <SaveButton
                    buttonAction={() => { this.props.navigation.goBack() }}
                    buttonTitle={'Save'}/>
            </ScrollView>
        );
    }
    _updateInputControl = (inputKey, inputValue) => {
        //set global language here...
        this.setState({
            ...this.state,
            [inputKey] : inputValue
        });
    };
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'space-between',
        backgroundColor: ColorDefinitions.mainBackground,
    },
});

const mapStateToProps = (state) => {
    return {

    }
};

const matchDispatchToProps = dispatch => {
    return {

    }
};

export default connect(mapStateToProps, matchDispatchToProps)(PrivacyScreen);
