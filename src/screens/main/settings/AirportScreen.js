import React, {Component} from 'react';
import {StyleSheet, View} from 'react-native';
import ColorDefinitions from "../../../constants/ColorDefinitions";
import {connect} from "react-redux";
import AirportsModal from "../../../components/utilities/AirportsModal";
import {buyTicketsUpdateVisibleItems, updateTicketInputs} from "../../../store/utilities/actionsCollection";
import SectionHeader from "../../../components/settings/SectionHeader";
import SaveButton from "../../../components/settings/SaveButton";

class AirportScreen extends Component {
    state={
        airportsVisibility: false,
        airportSearch: '',
        default: null,
    };

    render () {
        const airports = this.props.airports;

        return (
            <View style={styles.container}>
                <AirportsModal
                    airportSearch={this.state.airportSearch}
                    updateItemVisibility={() => this._updateInputItem('airportsVisibility', false)}
                    updateInputItem={this._updateInputItem}
                    airports={ airports.filter( item => {
                        return item.airport_code.toLowerCase().includes(this.state.airportSearch.toLowerCase()) ||
                            item.name.toLowerCase().includes(this.state.airportSearch.toLowerCase()) ||
                            item.city.name.toLowerCase().includes(this.state.airportSearch.toLowerCase()) ||
                            item.city.code.toLowerCase().includes(this.state.airportSearch.toLowerCase()) ||
                            item.province.name.toLowerCase().includes(this.state.airportSearch.toLowerCase())||
                            item.province.code.toLowerCase().includes(this.state.airportSearch.toLowerCase())
                    })}
                    modalVisible={this.state.airportsVisibility}
                    airportMode={'default'} />

                <SectionHeader
                    sectionTitle='Default Airport'
                    sectionDescription='Selected airport will be used as the default origin airport when searching flights. You can still change origin airport'/>

                <SaveButton
                    buttonAction={() => {this._updateInputItem('airportsVisibility', true)}}
                    buttonTitle={this.state.default ? this.state.default.airport_code + ' - '+ this.state.default.city.name +', '+this.state.default.province.name: "Select Your Default Home Airport"}/>
            </View>
        );
    }

    _updateInputItem = (itemKey, itemValue) =>{
        this.setState({
            ...this.state,
            [itemKey] : itemValue
        })
    };
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        //justifyContent: 'space-between',
        backgroundColor: ColorDefinitions.mainBackground,
    },
});

const mapStateToProps = (state) => {
    return {
        visibleItems: state.buyTicketsReducer.visibleItems,
        inputs: state.buyTicketsReducer.inputs,
        passengerTypes: state.buyTicketsReducer.passengerTypes,
        airports: state.buyTicketsReducer.airports
    }
};

const matchDispatchToProps = dispatch => {
    return {
        updateVisibleItems: (item) => dispatch (buyTicketsUpdateVisibleItems(item)),
        updateTicketInputs: (item) => dispatch (updateTicketInputs(item)),
    }
};

export default connect(mapStateToProps, matchDispatchToProps)(AirportScreen);
