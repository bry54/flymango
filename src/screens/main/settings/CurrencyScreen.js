import React, {Component} from 'react';
import {StyleSheet, View} from 'react-native';
import ColorDefinitions from "../../../constants/ColorDefinitions";
import {connect} from "react-redux";
import SectionHeader from "../../../components/settings/SectionHeader";
import SaveButton from "../../../components/settings/SaveButton";
import i18n from "../../../localizations/i18n";
import {Dropdown} from "react-native-material-dropdown";
import {updateGlobalControls} from "../../../store/actions/global";

class CurrencyScreen extends Component {
    render () {
        const currencies = i18n.t('currencies');

        return (
            <View style={styles.container}>
                <View>
                    <SectionHeader
                    sectionTitle='Currency'
                    sectionDescription='Selected currency will be used as the base currency for all fares/prices'/>

                    <View style={{padding: 10}}>
                        <Dropdown
                            value={this.props.currency}
                            inputContainerStyle={{borderBottomWidth: 2}}
                            onChangeText={(value) => this._updateInputControl('currency', value)}
                            style={{fontSize: 18,fontFamily: 'alpha-regular', color: ColorDefinitions.accentColor.shade0 }}
                            labelFontSize={14}
                            labelTextStyle={{ fontFamily: 'alpha-regular', color: ColorDefinitions.darkText.shade4 }}
                            itemTextStyle={{ fontFamily: 'alpha-regular' }}
                            baseColor='rgba(255, 165, 0, 0.9)'
                            label={i18n.t('currency_label')}
                            data={currencies}
                            valueExtractor={(item) => item.value}
                            labelExtractor={(item) => item.label}/>
                    </View>
                </View>

                <SaveButton
                    buttonAction={() => { this.props.navigation.goBack() }}
                    buttonTitle={'Save'}/>
            </View>
        );
    }

    _updateInputControl = (inputKey, inputValue) => {
        this.props.updateGlobalControl({key: inputKey, value: inputValue});
    };
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'space-between',
        backgroundColor: ColorDefinitions.mainBackground,
    },

    pickerHolder: {
        borderBottomColor: ColorDefinitions.accentColor.shade0,
        borderBottomWidth: 2,
        overflow: 'hidden'
    },
});

const mapStateToProps = (state) => {
    return {
        currency: state.globalReducer.currency
    }
};

const matchDispatchToProps = dispatch => {
    return {
        updateGlobalControl: (controlObj) => dispatch(updateGlobalControls(controlObj))
    }
};

export default connect(mapStateToProps, matchDispatchToProps)(CurrencyScreen);
