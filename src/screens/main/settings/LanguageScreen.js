import React, {Component} from 'react';
import { StyleSheet, View } from 'react-native';
import ColorDefinitions from "../../../constants/ColorDefinitions";
import {connect} from "react-redux";
import SectionHeader from "../../../components/settings/SectionHeader";
import SaveButton from "../../../components/settings/SaveButton";
import i18n from "../../../localizations/i18n";
import {updateGlobalControls} from "../../../store/actions/global";
import {Dropdown} from "react-native-material-dropdown";

class LanguageScreen extends Component {

    render () {
        const languages = i18n.t('languages');
        const regions = i18n.t('regions');

        return (
            <View style={styles.container}>
                <View>
                    <SectionHeader
                    sectionTitle='Application Localization'
                    sectionDescription='Select your country and language you want to use in-app'/>

                    <View style={{padding: 10}}>
                        <Dropdown
                            value={this.props.region}
                            inputContainerStyle={{borderBottomWidth: 2}}
                            onChangeText={(value) => this._updateInputControl('region', value)}
                            style={{fontSize: 18,fontFamily: 'alpha-regular', color: ColorDefinitions.accentColor.shade0 }}
                            labelFontSize={14}
                            labelTextStyle={{ fontFamily: 'alpha-regular', color: ColorDefinitions.darkText.shade4 }}
                            itemTextStyle={{ fontFamily: 'alpha-regular' }}
                            baseColor='rgba(255, 165, 0, 0.9)'
                            label={i18n.t('region_label')}
                            data={regions}
                            valueExtractor={(item) => item.value}
                            labelExtractor={(item) => item.label}/>
                    </View>

                    <View style={{padding: 10}}>
                        <Dropdown
                            value={this.props.language}
                            inputContainerStyle={{borderBottomWidth: 2}}
                            onChangeText={(value) => this._updateInputControl('language', value)}
                            style={{fontSize: 18,fontFamily: 'alpha-regular', color: ColorDefinitions.accentColor.shade0 }}
                            labelFontSize={14}
                            labelTextStyle={{ fontFamily: 'alpha-regular', color: ColorDefinitions.darkText.shade4 }}
                            itemTextStyle={{ fontFamily: 'alpha-regular' }}
                            baseColor='rgba(255, 165, 0, 0.9)'
                            label={i18n.t('language_label')}
                            data={languages}
                            valueExtractor={(item) => item.value}
                            labelExtractor={(item) => item.label}/>
                    </View>
                </View>

                <SaveButton
                    buttonAction={() => { this.props.navigation.goBack() }}
                    buttonTitle={'Save'}/>
            </View>
        );
    }

    _updateInputControl = (inputKey, inputValue) => {
        this.props.updateGlobalControl({key: inputKey, value: inputValue});
    };
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'space-between',
        backgroundColor: ColorDefinitions.mainBackground,
    },

    pickerHolder: {
        borderBottomColor: ColorDefinitions.accentColor.shade0,
        borderBottomWidth: 2,
        overflow: 'hidden'
    },
});

const mapStateToProps = (state) => {
    return {
        language: state.globalReducer.language,
        region: state.globalReducer.region,
    }
};

const matchDispatchToProps = dispatch => {
    return {
        updateGlobalControl: (controlObj) => dispatch(updateGlobalControls(controlObj))
    }
};

export default connect(mapStateToProps, matchDispatchToProps)(LanguageScreen);
