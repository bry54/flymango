import React, {Component} from 'react';
import {FlatList, StyleSheet, View} from 'react-native';
import ColorDefinitions from "../../../constants/ColorDefinitions";
import {connect} from "react-redux";
import {ListItem} from "react-native-elements";
import {AntDesign} from "@expo/vector-icons";
import Ripple from "react-native-material-ripple";

class LandingScreen extends Component {
    render () {
        const settings = [
            {title: 'Location Services', route: 'LocationSettings'},
            {title: 'Default Airport', route: 'AirportSettings'},
            {title: 'Notifications', route: 'NotificationsSettings'},
            {title: 'Default Language', route: 'LanguageSettings'},
            {title: 'Default Currency', route: 'CurrencySettings'},
            {title: 'Privacy Settings', route: 'PrivacySettings'},
            {title: 'Measurement Unit', route: 'WeatherUnits'},
        ];

        return (
            <View style={styles.container}>
                <FlatList
                    keyExtractor={(item) => JSON.stringify(item)}
                    data={settings}
                    renderItem={ (item) => this.renderItem(item)} />
            </View>
        );
    }

    renderItem = ( {item} ) => {
        const {navigation} = this.props;
        return (
            <Ripple rippleOpacity={.8} onPress={() => {navigation.navigate(item.route)}}>
                <ListItem
                    title={item.title}
                    titleStyle={{fontFamily: 'alpha-regular', color: ColorDefinitions.darkText.shade4, paddingVertical: 3, fontSize: 16}}
                    subtitle={item.subtitle}
                    subtitleStyle={{fontFamily: 'alpha-regular'}}
                    rightIcon={()=>(<AntDesign name="right" size={18} color={ColorDefinitions.darkText.shade1} />)}
                    bottomDivider
                />
            </Ripple>
        )
    };
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: ColorDefinitions.mainBackground,
    },
});

const mapStateToProps = (state) => {
    return {

    }
};

const matchDispatchToProps = dispatch => {
    return {

    }
};

export default connect(mapStateToProps, matchDispatchToProps)(LandingScreen);
