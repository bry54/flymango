import React, {Component} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import ColorDefinitions from "../../../constants/ColorDefinitions";
import {connect} from "react-redux";
import {Divider} from "react-native-elements";
import LabeledSwitchButton from "../../../components/utilities/LabeledSwitchButton";
import SectionHeader from "../../../components/settings/SectionHeader";
import SaveButton from "../../../components/settings/SaveButton";

class weatherUnitsScreen extends Component {
    state = {
        temperature: 2,
        distance: 2,
        precipitation: 2,
    };

    render () {
        return (
            <View style={styles.container}>
                <View>
                    <SectionHeader
                        sectionTitle='Weather Display Settings'
                        sectionDescription='Set your preferred weather measurements units.'/>

                    <View style={{marginHorizontal: 10, marginVertical: 10}}>
                        <View style={{flexDirection: 'row', justifyContent: 'space-between', marginVertical: 10}}>
                            <View>
                                <Text style={{
                                    paddingVertical: 2,
                                    fontFamily: 'alpha-regular',
                                    color: ColorDefinitions.darkText.shade4,
                                    fontSize: 16
                                }}>Temperature Units</Text>
                                <Text style={{paddingVertical: 2, fontFamily: 'alpha-regular', color: 'grey'}}>Select
                                    to receive push notification</Text>
                            </View>
                            {this.unitSwitch({
                                key: 'temperature',
                                metricLabel: '°C',
                                metricCode: 'degrees',
                                empiricalLabel: '°F',
                                empiricalCode: 'fahrenheit',
                                code: 'WEATHER'
                            }, this.state.temperature)}
                        </View>

                        <Divider/>

                        <View style={{flexDirection: 'row', justifyContent: 'space-between', marginVertical: 10}}>
                            <View>
                                <Text style={{
                                    paddingVertical: 2,
                                    fontFamily: 'alpha-regular',
                                    color: ColorDefinitions.darkText.shade4,
                                    fontSize: 16
                                }}>Distance Unit</Text>
                                <Text style={{paddingVertical: 2, fontFamily: 'alpha-regular', color: 'grey'}}>Select
                                    to receive push notification</Text>
                            </View>
                            {this.unitSwitch({
                                key: 'distance',
                                metricLabel: 'km',
                                metricCode: 'km',
                                empiricalLabel: 'mi',
                                empiricalCode: 'mi',
                                code: 'SPEED'
                            }, this.state.distance)}
                        </View>

                        <Divider/>

                        <View style={{flexDirection: 'row', justifyContent: 'space-between', marginVertical: 10}}>
                            <View>
                                <Text style={{
                                    paddingVertical: 2,
                                    fontFamily: 'alpha-regular',
                                    color: ColorDefinitions.darkText.shade4,
                                    fontSize: 16
                                }}>Precipitation Units</Text>
                                <Text style={{paddingVertical: 2, fontFamily: 'alpha-regular', color: 'grey'}}>Select
                                    to receive push notification</Text>
                            </View>
                            {this.unitSwitch({
                                key: 'precipitation',
                                metricLabel: 'mm',
                                metricCode: 'mm',
                                empiricalLabel: 'in',
                                empiricalCode: 'in',
                                code: 'WEATHER'
                            }, this.state.precipitation)}
                        </View>
                    </View>
                </View>

                <SaveButton
                    buttonAction={() => { this.props.navigation.goBack() }}
                    buttonTitle={'Save'}/>
            </View>
        );
    }

    _updateInputControl =  (inputKey, inputValue) => {
        //set global language here...
        this.setState({
            ...this.state,
            [inputKey] : inputValue
        });
    };

    unitSwitch = (unit, selected) =>{
        return ( <LabeledSwitchButton
                onValueChange={(val) => {this._updateInputControl(unit.key, val)}}      // this is necessary for this component
                text1 = {unit.metricLabel}                       // optional: first text in switch button --- default ON
                text2 = {unit.empiricalLabel}                       // optional: second text in switch button --- default OFF
                switchWidth = {100}                 // optional: switch width --- default 44
                switchHeight = {40}                 // optional: switch height --- default 100
                switchdirection = 'rtl'             // optional: switch button direction ( ltr and rtl ) --- default ltr
                switchBorderRadius = {0}          // optional: switch border radius --- default oval
                switchSpeedChange = {500}           // optional: button change speed --- default 100
                switchBorderColor = 'transparent'       // optional: switch border color --- default #d4d4d4
                switchBackgroundColor = 'transparent'      // optional: switch background color --- default #fff
                btnBorderColor = 'transparent'          // optional: button border color --- default #00a4b9
                btnBackgroundColor = {ColorDefinitions.accentColor.shade0}      // optional: button background color --- default #00bcd4
                fontColor = '#b1b1b1'               // optional: text font color --- default #b1b1b1
                activeFontColor = { ColorDefinitions.darkText.shade4}           // optional: active font color --- default #fff
                selectedItem={selected}
            />
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'space-between',
        backgroundColor: ColorDefinitions.mainBackground,
    },
});

const mapStateToProps = (state) => {
    return {

    }
};

const matchDispatchToProps = dispatch => {
    return {

    }
};

export default connect(mapStateToProps, matchDispatchToProps)(weatherUnitsScreen);
