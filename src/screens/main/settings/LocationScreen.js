import React, {Component} from 'react';
import {StyleSheet, View} from 'react-native';
import ColorDefinitions from "../../../constants/ColorDefinitions";
import {connect} from "react-redux";
import {ListItem} from "react-native-elements";
import {AntDesign} from "@expo/vector-icons";
import SectionHeader from "../../../components/settings/SectionHeader";
import SaveButton from "../../../components/settings/SaveButton";

class LocationScreen extends Component {
    render () {
        return (
            <View style={styles.container}>
                <View>
                    <SectionHeader
                    sectionTitle='Turned ON'
                    sectionDescription='We only use you location to show special offers based on your location.'/>

                    <ListItem
                        onPress={() => {

                        }}
                        title={'Change Device Settings'}
                        titleStyle={{fontFamily: 'alpha-regular', color: ColorDefinitions.darkText.shade4, paddingVertical: 3, fontSize: 18}}
                        subtitle={'This will open device settings'}
                        subtitleStyle={{fontFamily: 'alpha-regular'}}
                        rightIcon={() => (<AntDesign name="right" size={18} color={ColorDefinitions.darkText.shade4}/>)}
                        bottomDivider
                    />
                </View>

                <SaveButton
                    buttonAction={() => { this.props.navigation.goBack() }}
                    buttonTitle={'Save'}/>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'space-between',
        backgroundColor: ColorDefinitions.mainBackground,
    },
});

const mapStateToProps = (state) => {
    return {

    }
};

const matchDispatchToProps = dispatch => {
    return {

    }
};

export default connect(mapStateToProps, matchDispatchToProps)(LocationScreen);
