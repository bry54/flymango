import React, {Component} from 'react';
import {ScrollView, StyleSheet, Switch, Text, View} from 'react-native';
import ColorDefinitions from "../../../constants/ColorDefinitions";
import {connect} from "react-redux";
import {Divider} from "react-native-elements";
import SectionHeader from "../../../components/settings/SectionHeader";
import SaveButton from "../../../components/settings/SaveButton";

class NotificationScreen extends Component {
    state= {
        flightNotice: false,
        generalNotice: false,
        promoNotice: false,
        receiveEmailNotice: false,
        receiveMobileNotice: false,
    };

    render () {
        return (
            <ScrollView
                showsHorizontalScrollIndicator={false}
                showsVerticalScrollIndicator={false}
                style={styles.container}>
                <SectionHeader
                    sectionTitle='Notifications'
                    sectionDescription='Select notifications that apply.'/>

                <View style={{marginHorizontal:10, marginVertical: 10}}>
                    <View style={{flexDirection: 'row', justifyContent: 'space-between', marginVertical: 10}}>
                        <View>
                            <Text style={{paddingVertical: 2, fontFamily: 'alpha-regular', color: ColorDefinitions.darkText.shade4, fontSize: 16}}>Flight Notices</Text>
                            <Text style={{paddingVertical: 2, fontFamily: 'alpha-regular', color: 'grey'}}>You will receive flight notifications from Mango</Text>
                        </View>
                        <Switch
                            value={this.state.flightNotice}
                            onValueChange = {(value) => {this._updateInputControl('flightNotice', value)}}/>
                    </View>

                    <Divider/>

                    <View style={{flexDirection: 'row', justifyContent: 'space-between', marginVertical: 10}}>
                        <View>
                            <Text style={{paddingVertical: 2, fontFamily: 'alpha-regular', color: ColorDefinitions.darkText.shade4, fontSize: 16}}>General Notices</Text>
                            <Text style={{paddingVertical: 2, fontFamily: 'alpha-regular', color: 'grey'}}>You will receive general notifications</Text>
                        </View>
                        <Switch
                            value={this.state.generalNotice}
                            onValueChange = {(value) => {this._updateInputControl('generalNotice', value)}}/>
                    </View>

                    <Divider/>

                    <View style={{flexDirection: 'row', justifyContent: 'space-between', marginVertical: 10}}>
                        <View>
                            <Text style={{paddingVertical: 2, fontFamily: 'alpha-regular', color: ColorDefinitions.darkText.shade4, fontSize: 16}}>Promotion Notices</Text>
                            <Text style={{paddingVertical: 2, fontFamily: 'alpha-regular', color: 'grey'}}>You will receive promotional notifications</Text>
                        </View>
                        <Switch
                            value={this.state.promoNotice}
                            onValueChange = {(value) => {this._updateInputControl('promoNotice', value)}}/>
                    </View>
                </View>
                <Divider/>

                <SectionHeader
                    sectionTitle='Notification Destinations'
                    sectionDescription='Select where you would like to receive notifications.'/>

                <View style={{marginHorizontal:10, marginVertical: 10}}>
                    <View style={{flexDirection: 'row', justifyContent: 'space-between', marginVertical: 10}}>
                        <View>
                            <Text style={{paddingVertical: 2, fontFamily: 'alpha-regular', color: ColorDefinitions.darkText.shade4, fontSize: 16}}>Receive Email Notification</Text>
                            <Text style={{paddingVertical: 2, fontFamily: 'alpha-regular', color: 'grey'}}>Select to receive e-mail notification</Text>
                        </View>
                        <Switch
                            value={this.state.receiveEmailNotice}
                            onValueChange = {(value) => {this._updateInputControl('receiveEmailNotice', value)}}/>
                    </View>

                    <Divider/>

                    <View style={{flexDirection: 'row', justifyContent: 'space-between', marginVertical: 10}}>
                        <View>
                            <Text style={{paddingVertical: 2, fontFamily: 'alpha-regular', color: ColorDefinitions.darkText.shade4, fontSize: 16}}>Receive Mobile Notification</Text>
                            <Text style={{paddingVertical: 2, fontFamily: 'alpha-regular', color: 'grey'}}>Select to receive push notification</Text>
                        </View>
                        <Switch
                            value={this.state.receiveMobileNotice}
                            onValueChange = {(value) => {this._updateInputControl('receiveMobileNotice', value)}}/>
                    </View>
                </View>

                <SaveButton
                    buttonAction={() => { this.props.navigation.goBack() }}
                    buttonTitle={'Save'}/>
            </ScrollView>
        );
    }

    _updateInputControl = (inputKey, inputValue) => {
        //set global language here...
        this.setState({
            ...this.state,
            [inputKey] : inputValue
        });
    };
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: ColorDefinitions.mainBackground,
    },
});

const mapStateToProps = (state) => {
    return {

    }
};

const matchDispatchToProps = dispatch => {
    return {

    }
};

export default connect(mapStateToProps, matchDispatchToProps)(NotificationScreen);
