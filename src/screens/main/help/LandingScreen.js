import React, {Component} from 'react';
import {Linking, ScrollView, StyleSheet, Text, View} from 'react-native';
import ColorDefinitions from "../../../constants/ColorDefinitions";
import {connect} from "react-redux";
import {Divider, ListItem} from "react-native-elements";
import {AntDesign, MaterialCommunityIcons, MaterialIcons} from "@expo/vector-icons";
import SectionHeader from "../../../components/help/SectionHeader";
import Ripple from "react-native-material-ripple";

class LandingScreen extends Component {

    render () {
        const socialChannels = [
            {title: 'Facebook', type:'facebook', route: 'https://www.facebook.com/flymangosa', hasNativeRoute: false},
            {title: 'Twitter', type:'twitter', route: 'https://mobile.twitter.com/flymangosa', hasNativeRoute: false},
            {title: 'Instagram', type:'instagram', route: 'https://www.instagram.com/flymangosa', hasNativeRoute: false}
        ];

        const helpSections = [
            {title: 'Contact Information', route: 'ContactUs', hasNativeRoute: true},
            {title: 'News', route: 'https://www.flymango.com/en/news-room', hasNativeRoute: false},
            {title: 'FAQ', route: 'https://www.flymango.com/en/frequently-asked-questions', hasNativeRoute: false},
        ];

        return (
            <ScrollView
                showsHorizontalScrollIndicator={false}
                showsVerticalScrollIndicator={false}
                style={styles.container}>
                <SectionHeader
                    sectionTitle='Our Social Networks'
                    sectionDescription='Checkout our vibrant social networks and and follow us for updates.'/>

                <View style={{marginVertical: 10}}>{
                    socialChannels.map((item, i) => (
                        <Ripple
                            rippleOpacity={.8}
                            key={i}
                            onPress={() => { this._navToHelpItem(item) }}>
                            <ListItem
                                leftAvatar={() => (<MaterialCommunityIcons
                                    size={24}
                                    name={item.type}
                                    color={ColorDefinitions.accentColor.shade0}
                                    iconStyle={{color: ColorDefinitions.accentColor.shade0, elevation: 2}}
                                />)}
                                containerStyle={{paddingVertical: 5}}
                                title={item.title}
                                titleStyle={{fontFamily: 'alpha-regular', color: ColorDefinitions.darkText.shade4, paddingVertical: 0, fontSize: 16}}
                                subtitle={item.subtitle}
                                subtitleStyle={{fontFamily: 'alpha-regular'}}
                                rightIcon={()=>(<MaterialIcons name="open-in-new" size={16} color={ColorDefinitions.darkText.shade4} />)}
                            />
                        </Ripple>
                    ))
                }</View>

                <Divider style={{backgroundColor: 'grey', padding:.5}}/>

                {
                    helpSections.map((item, i) => (
                        <Ripple
                            key={i}
                            rippleOpacity={.8}
                            onPress={() => this._navToHelpItem(item)}>
                            <ListItem
                                containerStyle={{paddingVertical: 15}}
                                title={item.title}
                                titleStyle={{fontFamily: 'alpha-regular', color: ColorDefinitions.darkText.shade4, paddingVertical: 3, fontSize: 15}}
                                subtitle={item.subtitle}
                                subtitleStyle={{fontFamily: 'alpha-regular'}}
                                rightIcon={()=>(<AntDesign name="right" size={14} color={ColorDefinitions.darkText.shade4} />)}
                                bottomDivider
                            />
                        </Ripple>
                    ))
                }

                <View style={{paddingVertical: 20}}>
                    <Text style={{fontFamily: 'numeric-regular', textAlign: 'center', paddingVertical: 5}}>FlyMango (v1.0.0)</Text>
                    <Text style={{fontFamily: 'numeric-regular', textAlign: 'center', paddingVertical: 5}}>Exclusive To Mango Airlines</Text>
                    <Text style={{fontFamily: 'numeric-regular', textAlign: 'center', paddingVertical: 5}}>Prototype By: Brian Paidamoyo Sithole</Text>
                </View>
            </ScrollView>
        );
    }

    _navToHelpItem = ( item ) => {
        const {navigation} = this.props;
        if (item.hasNativeRoute)
            navigation.navigate(item.route);
        else
            Linking.openURL(item.route);
    };
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: ColorDefinitions.mainBackground,
    },
});

const mapStateToProps = (state) => {
    return {

    }
};

const matchDispatchToProps = dispatch => {
    return {

    }
};

export default connect(mapStateToProps, matchDispatchToProps)(LandingScreen);
