import React, {Component} from 'react';
import {contactUs} from "../../../utilities/MockUpData";
import {ScrollView, StyleSheet, Text, View} from "react-native";
import {ListItem} from "react-native-elements";
import {Entypo} from "@expo/vector-icons";
import ColorDefinitions from "../../../constants/ColorDefinitions";

const Address = (props) => (
    <View style={{width: '100%', paddingHorizontal: 5, marginVertical: 5}}>
        <Text style={{fontFamily: 'alpha-regular', color: ColorDefinitions.darkText.shade4, paddingVertical: 3, fontSize: 16}}>
            {props.title}
        </Text>
        <View style={{flexDirection: 'row', flexWrap: 'wrap', borderLeftWidth:3, }}>
            <Text style={{fontFamily: 'alpha-regular', color: ColorDefinitions.darkText.shade4, paddingVertical: 1, fontSize: 14}}>
                <Entypo name={'dot-single'}/>{props.address.line1}
            </Text>
            <Text style={{fontFamily: 'alpha-regular', color: ColorDefinitions.darkText.shade4, paddingVertical: 1, fontSize: 14}}>
                <Entypo name={'dot-single'}/>{props.address.line2}
            </Text>
            <Text style={{fontFamily: 'alpha-regular', color: ColorDefinitions.darkText.shade4, paddingVertical: 1, fontSize: 14}}>
                <Entypo name={'dot-single'}/>{props.address.line3}
            </Text>
            {props.address.line4 && <Text style={{fontFamily: 'alpha-regular', color: ColorDefinitions.darkText.shade4, paddingVertical: 1, fontSize: 14}}>
                <Entypo name={'dot-single'}/>{props.address.line4}
            </Text>}
        </View>
    </View>
);

export default class ContactInformationScreen extends Component {
    render() {
        return (
            <ScrollView style={styles.container} showsVerticalScrollIndicator={false}>
                <View>
                    <View>
                        <ListItem
                            containerStyle={{margin: 0, paddingVertical: 5, paddingHorizontal: 5}}
                            title={
                                <View style={{flexDirection: 'row'}}>
                                    <Text style={{fontFamily: 'alpha-regular', color: ColorDefinitions.darkText.shade4, paddingVertical: 3, fontSize: 20}}>
                                        Contact Addresses
                                    </Text>
                                </View>
                            }
                            subtitle={
                                <View style={{flexDirection: 'column'}}>
                                    <Address
                                        title='Physical Address'
                                        address={contactUs.main.physical_address}/>

                                    <Address
                                        title='Postal Address'
                                        address={contactUs.main.postal_address}/>
                                </View>
                            }/>
                    </View>

                    <View>
                        <ListItem
                            containerStyle={{margin: 0, paddingVertical: 5, paddingHorizontal: 10}}
                            title={
                                <View style={{flexDirection: 'row'}}>
                                    <Text style={{ fontFamily: 'alpha-regular', color: ColorDefinitions.darkText.shade4, paddingVertical: 3, fontSize: 20}}>
                                        Phones
                                    </Text>
                                </View>
                            }
                            subtitle={
                                <View>{
                                    contactUs.main.phone.map(p => (
                                        <View style={{flexDirection: 'row'}}>
                                            <View style={{width: '50%'}}>
                                                <Text style={{fontFamily: 'alpha-regular', color: ColorDefinitions.darkText.shade4, paddingVertical: 1, fontSize: 14}}>{p.name}</Text>
                                            </View>
                                            <View style={{width: '50%'}}>
                                                {p.phones.map(num=>
                                                    <Text style={{fontFamily: 'alpha-regular', color: ColorDefinitions.darkText.shade4, paddingVertical: 1, fontSize: 14}}>
                                                        {num}
                                                    </Text>
                                                )}
                                            </View>
                                        </View>
                                    ))
                                }</View>
                            }
                        />
                    </View>
                </View>
                {
                    contactUs.others.map(contact =>(
                        <ListItem
                            leftAvatar={{
                                overlayContainerStyle: {backgroundColor: ColorDefinitions.darkBackgroundColor.shade4},
                                rounded: true,
                                icon: { name: contact.icon, type:'material-community',color: ColorDefinitions.accentColor.shade0 }}}
                            topDivider={true}
                            title={contact.name}
                            titleStyle={{fontFamily: 'alpha-regular', color: ColorDefinitions.darkText.shade4, paddingVertical: 1, fontSize: 17}}
                            subtitle={
                                <View>
                                    <Text style={{fontFamily: 'alpha-regular', color: ColorDefinitions.darkText.shade4, paddingVertical: 1, fontSize: 15}}>
                                        {contact.phone}
                                    </Text>
                                    {contact.email &&
                                    <Text style={{textTransform: 'lowercase', fontFamily: 'alpha-regular', color: ColorDefinitions.darkText.shade4, paddingVertical: 1, fontSize: 15}}>
                                        {contact.email}</Text>
                                    }
                                </View>
                            }
                        />
                    ))
                }
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: ColorDefinitions.mainBackground,
    },
});
