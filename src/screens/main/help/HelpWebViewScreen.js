import React, {Component} from 'react';
import {WebView} from 'react-native-webview';
import {SkypeIndicator} from "react-native-indicators";
import ColorDefinitions from "../../../constants/ColorDefinitions";

export default class HelpWebViewScreen extends Component {
    render() {
        return (
            <WebView
                startInLoadingState={true}
                renderLoading={()=>(<SkypeIndicator color={ColorDefinitions.colorPrimary}/>)}
                source={{ uri: this.props.route.params.route }}
                style={{ marginTop: 0 }}
            />
        );
    }
}
