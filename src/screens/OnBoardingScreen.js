import React, {Component} from 'react'
import {Ionicons} from '@expo/vector-icons';
import {ImageBackground, StyleSheet, Text, View} from 'react-native';
import ColorDefinitions from "../constants/ColorDefinitions";
import AppIntroSlider from "react-native-app-intro-slider";
import {updateGlobalControls} from "../store/utilities/actionsCollection";
import {connect} from "react-redux";
import i18n from '../localizations/i18n'
import {Dropdown} from "react-native-material-dropdown";

class OnBoardingScreen extends Component{
    render() {
        const boardingSlides = i18n.t('boarding_slides');

        return (
            <AppIntroSlider
                showPrevButton={true}
                showSkipButton={this._showSkipButton}
                skipLabel={i18n.t('skip_boarding')}
                dotStyle={{backgroundColor: 'gray'}}
                activeDotStyle={{backgroundColor: ColorDefinitions.accentColor.shade0}}
                renderDoneButton={()=>this._renderDoneButton()}
                renderNextButton={()=>this._renderArrowedButton('forward')}
                renderPrevButton={()=>this._renderArrowedButton('back')}
                renderItem={this._renderItem}
                slides={boardingSlides}
                onDone={()=>this._navToAppMenu()}/>
        );
    }

    _showSkipButton = ({ item}) =>{
        console.log('here',item)
        return item.key !== '00'
    };

    _renderItem = ({ item }) => {
        const languages = i18n.t('languages');
        return (
            <ImageBackground source={item.image} style={styles.slideContainer}>
                {item.key === '00' ? (
                <View style={{marginBottom: 100}}>
                    <View style={{marginVertical: 200, marginHorizontal: 10}}>
                        <Dropdown
                            value={this.props.language}
                            inputContainerStyle={{borderBottomWidth: .5}}
                            onChangeText={(value) => this.props.updateGlobalControl({key:'language', value: value})}
                            style={{fontSize: 20,fontFamily: 'alpha-regular', color: ColorDefinitions.darkText.shade4 }}
                            labelFontSize={16}
                            labelTextStyle={{ fontFamily: 'alpha-regular', color: ColorDefinitions.darkText.shade4 }}
                            itemTextStyle={{ fontFamily: 'alpha-regular' }}
                            baseColor={ColorDefinitions.darkText.shade4}
                            label={i18n.t('language_label')}
                            data={languages}
                            valueExtractor={(item) => item.value}
                            labelExtractor={(item) => item.label}/>
                    </View>

                    <Text style={styles.title}>{item.title}</Text>
                    <Text style={styles.text}>{item.text}</Text>
                </View>
                ) : (
                <View style={{marginBottom: 100}}>
                    <Text style={styles.title}>{item.title}</Text>
                    <Text style={styles.text}>{item.text}</Text>
                </View>)}
            </ImageBackground>
        );
    };

    _renderArrowedButton = (direction) => {
        return (
            <View style={styles.buttonCircle}>
                <Ionicons
                    name={direction === 'forward' ? "md-arrow-forward" : "md-arrow-back"}
                    color="rgba(255, 255, 255, .9)"
                    size={24}
                    style={{ backgroundColor: 'transparent' }}
                />
            </View>
        );
    };

    _renderDoneButton = () => {
        return (
            <View style={styles.buttonCircle}>
                <Ionicons
                    name="md-checkmark"
                    color="rgba(255, 255, 255, .9)"
                    size={24}
                    style={{ backgroundColor: 'transparent' }}
                />
            </View>
        );
    };

    _navToAppMenu = () => {
        const {navigation} = this.props;
        const user = this.props.user;
        const useAsGuest = this.props.useAsGuest;

        this.props.updateGlobalControl({key:'viewedOnBoardingScreen', value: true});

        if (user || useAsGuest)
            navigation.navigate('Main');
        else
            navigation.navigate('Auth', { screen: 'StartOptions'});
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: ColorDefinitions.mainBackground,
    },

    slideContainer: {
        height: '100%',
        justifyContent: 'flex-end',
    },

    text: {
        color: 'rgba(255, 255, 255, 0.8)',
        backgroundColor: 'transparent',
        textAlign: 'center',
        paddingHorizontal: 16,
        fontSize: 20,
        lineHeight: 30,
        fontFamily: 'alpha-regular'
    },

    title: {
        fontSize: 36,
        paddingHorizontal: 5,
        color: ColorDefinitions.lightText.shade0,
        backgroundColor: 'transparent',
        textAlign: 'center',
        marginBottom: 16,
        fontFamily: 'alpha-regular'
    },
    buttonCircle: {
        width: 40,
        height: 40,
        backgroundColor: 'rgba(0, 0, 0, .2)',
        borderRadius: 20,
        justifyContent: 'center',
        alignItems: 'center',
    },
});

const mapStateToProps = (state) => {
    return {
        user: state.authReducer.loggedInUser,
        useAsGuest: state.globalReducer.useAsGuest,
        language: state.globalReducer.language,
    }
};

const matchDispatchToProps = dispatch => {
    return {
        updateGlobalControl: (controlObj) => dispatch(updateGlobalControls(controlObj))
    }
};

export default connect(mapStateToProps, matchDispatchToProps)(OnBoardingScreen)
