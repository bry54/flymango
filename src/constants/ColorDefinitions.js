const tintColor = '#b71c1c';

const colorSchemes = {
    tintColor,
    errorBackground: '#ff0000',
    errorText: '#fff',
    warningBackground: '#EAEB5E',
    warningText: '#666804',
    noticeBackground: tintColor,
    noticeText: '#fff',

    alternateBackground: '#F5F5F6',
    mainBackground: '#fff',
    deepAlternateBackground: '#263238',

    colorPrimary:'#F4811F',
    colorPrimaryDark:'#BB5300',
    colorAccent:'#FFA500',

    textPrimary: '#01090D',
    textSecondary: '#F5F5F6',
    textAlternate: '#616161',

    primaryColor: {
        shade0: '#F4811F',
        shade1: '#FFB474',
        shade2: '#FF9F4C',
        shade3: '#C86007',
        shade4: '#9E4900',
    },

    darkPrimaryColor: {
        shade0: '#BB5300',
        shade1: '#F59243',
        shade2: '#E96E0C',
        shade3: '#984300',
        shade4: '#6D3000',
    },

    accentColor: {
        shade0: '#e69500',
        shade1: '#FFC864',
        shade2: '#FFBA39',
        shade3: '#C68000',
        shade4: '#9B6500',
    },

    alternateAccentColor: {
        shade0: '#BDD633',
        shade1: '#E8F985',
        shade2: '#D6EC58',
        shade3: '#9CB317',
        shade4: '#798E03',
    },

    lightBackgroundColor:{
        shade0: '#FFFFFF',
        shade1: '#FEFEFE',
        shade2: '#F5F5F5',
        shade3: '#CECECE',
        shade4: '#A2A0A0',
    },

    darkBackgroundColor:{
        shade0: '#36296A', //shade0: '#263238',
        shade1: '#756AA1', //shade1: '#5A6D76',
        shade2: '#4F4282', //shade2: '#40525B',
        shade3: '#4A148C', //shade3: '#182A33',
        shade4: '#1F134E', //shade4: '#0D2029',
        shade5: '#0E0632', //shade5: '#01090D',

        rgbaShade0: 'rgba( 54, 41,106,1)', //rgbaShade0: 'rgba(38, 50, 56, .5)',
        rgbaShade1: 'rgba(117,106,161,1)', //rgbaShade1: 'rgba(90, 109, 118, 1)',
        rgbaShade2: 'rgba( 79, 66,130,1)', //rgbaShade2: 'rgba(64, 82, 91, 1)',
        rgbaShade3: 'rgba(74, 20, 140,1)',//rgbaShade3: 'rgba(24, 42, 51, .9)',
        rgbaShade4: 'rgba( 31, 19, 78,1)', //rgbaShade4: 'rgba(13, 32, 41, 1)',
        rgbaShade5: 'rgba( 14,  6, 50,.5)', //rgbaShade5: 'rgba(1, 9, 13, 1)',
    },

    lightText:{
        shade0: '#FFFFFF',
        shade1: '#FEFEFE',
        shade2: '#F5F5F5',
        shade3: '#CECECE',
        shade4: '#6c757d',
    },

    darkText:{
        shade1: '#36296A', //shade1: '#263238',
        shade2: '#2f245c', //shade2: '#182A33',
        shade3: '#1c1537', //shade3: '#0D2029',
        shade4: '#0E0632', //shade4: '#000A12',
        shade5: '#090712', //shade5: '#01090D',
        shade6: '#000000', //shade6: '#000000',
    },
};

const theme ={
    colorPrimaryDark: colorSchemes.darkPrimaryColor.shade0,
    coloPrimary: colorSchemes.primaryColor.shade4,
    colorAccent: colorSchemes.accentColor.shade0,
};

export default colorSchemes;
