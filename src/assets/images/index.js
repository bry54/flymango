export default {
    splash: require('./splash.png'),
    planes:{
        _01: require('./plane_01.jpg'),
        _02: require('./plane_02.jpg'),
        _03: require('./plane_03.jpg'),
        _04: require('./plane_04.jpg'),
        _05: require('./plane_05.jpg'),
        _06: require('./plane_06.jpg'),
        _07: require('./plane_07.jpg'),
        _08: require('./plane_08.jpg'),
        _09: require('./plane_09.jpg'),
        _10: require('./plane_10.jpg'),
        _11: require('./plane_11.jpg'),
        _mango_01: require('./plane_12.jpg'),
        _mango_02: require('./plane_13.jpg'),
        _mango_03: require('./plane_14.jpg'),
        _mango_04: require('./plane_15.png'),
        _mango_logo_01: require('./plane_16.png'),
        _mango_logo_02: require('./plane_17.png'),
        _mango_logo_03: require('./plane_18.png'),
    },

    joburg:{
        _01: require('./joburg_01.jpg'),
        _02: require('./joburg_02.jpg'),
        _03: require('./joburg_03.jpg'),
        _04: require('./joburg_04.jpg'),
        _05: require('./joburg_05.jpg'),
        _06: require('./joburg_06.jpg'),
    },

    capetown:{
        _01: require('./capetown_01.jpg'),
        _02: require('./capetown_02.jpg'),
        _03: require('./capetown_03.jpg'),
        _04: require('./capetown_04.jpg'),
    },

    durban:{
        _01: require('./durban_01.jpg'),
        _02: require('./durban_02.jpg'),
        _03: require('./durban_03.jpg'),
        _04: require('./durban_04.jpg'),
    },

    holiday:{
        _01: require('./holiday_01.jpg'),
        _02: require('./holiday_02.jpg'),
        _03: require('./holiday_03.jpg'),
        _04: require('./holiday_04.jpg'),
    },

    travel:{
        _01: require('./travel_01.jpg'),
        _02: require('./travel_02.jpg'),
        _03: require('./travel_03.jpg'),
        _04: require('./travel_04.jpg'),
        _05: require('./travel_05.jpg'),
        _06: require('./travel_06.jpg')
    },

    others:{
        _01: require('./others_01.jpg'),
        _02: require('./others_02.jpg'),
        _03: require('./others_03.jpg')
    }
}

export const boardingImages = [
    require('./plane_01.jpg'),
    require('./plane_04.jpg'),
    require('./travel_01.jpg'),
    require('./travel_04.jpg'),
    require('./travel_05.jpg'),
    require('./travel_06.jpg')
];

export const appImages = [
    require('./plane_02.jpg'),
    require('./plane_10.jpg'),
    require('./plane_11.jpg'),
    require('./joburg_01.jpg'),
    require('./joburg_04.jpg'),
    require('./capetown_01.jpg'),
    require('./durban_04.jpg'),
    require('./holiday_01.jpg'),
    require('./holiday_03.jpg'),
    require('./others_01.jpg'),
    require('./others_02.jpg'),
    require('./others_03.jpg')
];
