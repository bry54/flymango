import images from "../assets/images";

export default {
    app_name: 'Ndiza Mango',
    catch_phrase: 'Jabulela uhlelo olukufanele.',
    string_preparing_app: 'Preparing app for use, please wait. This can take a while depending on your connection.',
    language_label: 'Select Preferred Language',
    languages: [
        {label: 'Engels', value: 'en'},
        {label: 'Afrikaans', value: 'af'},
        {label: 'Zulu', value: 'zl'},
        {label: 'Swahili', value: 'sl'},
    ],
    region_label: 'Select Your Region',
    regions: [
        {label: 'Eastern Cape (RSA)', value: 'ECP'},
        {label: 'Western Cape (RSA)', value: 'WCP'},
        {label: 'Gauteng (RSA)', value: 'GP'},
        {label: 'KwaZulu Natal (RSA)', value: 'KZN'},
        {label: 'Zimbabwe', value: 'ZW'},
        {label: 'Tanzania', value: 'TZ'},
        {label: 'Namibia', value: 'NM'},
    ],
    currency_label: 'Select Display Currency',
    currencies: [
        {label: 'South African Rand (R)', value: 'ZAR'},
        {label: 'Zimbabwean Dollar (ZWD)', value: 'ZWD'},
        {label: 'United States Dollar (USD)', value: 'USD'},
        {label: 'Great British Pound (GBP)', value: 'GBP'},
        {label: 'Euro (EURO)', value: 'EURO'},
    ],
    special_flights: [
        {
            key: '00',
            title: 'EGoli',
            text: 'Yonke iphakethe ehlanganisiwe, ukubuya kufakiwe',
            image: images.joburg._04,
            fee: 'R 800.00'
        }, {
            key: '01',
            title: 'EThekwini',
            text: 'Yonke iphakethe ehlanganisiwe, ukubuya kufakiwe',
            image: images.durban._04,
            fee: 'R 700.00'
        }, {
            key: '02',
            title: 'EKapa',
            text: 'Yonke iphakethe ehlanganisiwe, ukubuya kufakiwe',
            image: images.capetown._01,
            fee: 'R 1 000.00'
        },{
            key: '03',
            title: 'George',
            text: 'Yonke iphakethe ehlanganisiwe, ukubuya kufakiwe',
            image: images.others._02,
            fee: 'R 1 500.00'
        }, {
            key: '04',
            title: 'Harare',
            text: 'Yonke iphakethe ehlanganisiwe, ukubuya kufakiwe',
            image: images.others._03,
            fee: 'R 3 000.00'
        }, {
            key: '05',
            title: 'Zanzibar',
            text: 'Yonke iphakethe ehlanganisiwe, ukubuya kufakiwe',
            image: images.others._01,
            fee: 'R 6 000.00'
        }
    ],
    lbl_starting_at: 'Ukuqala @ {{price}}',
    boarding_slides: [
        {
            key: '00',
            title: 'Siyakwamukela ngaphakathi',
            text: 'Ithimba leMango Airlines liyakwamukela kwisicelo sethu sobungane.',
            image: images.planes._01,
        }, {
            key: '02',
            title: 'Fly Manje, Khokha Kamuva',
            text: 'Izinkokhelo eziguqukayo !!! Khokhisa izindiza zakho kalula kwi-akhawunti yakho ye-Edgars, JET noma ye-CNA Siyabonga',
            image: images.travel._04,
        }, {
            key: '03',
            title: 'Izaphulelo Ezikhethekile',
            text: 'Fly ngeMango, buthelela iMango amamayela emoyeni futhi uthole amaphuzu ukuze ufanelekele izaphulelo ezikhethekile nokunikelwayo.',
            image: images.travel._05,
        }, {
            key: '04',
            title: 'Kuphephile nathi',
            text: 'Ukuphepha kwakho kubalulekile kithi. Ndiza ngokuphepha ezandleni zeqembu lethu elinolwazi nelinobungane.',
            image: images.travel._06,
        }, {
            key: '05',
            title: 'Yenza i-akhawunti',
            text: 'Bhalisa i-akhawunti ngeFly Mango futhi uthole izinsiza eziqondene nomuntu.',
            image: images.travel._01,
        }
    ],
    showcase_slides:[
        {
            key: '00',
            title: 'Limpopo, Game park Visits',
            text: 'Enjoy and experience amazing wildlife',
            image: images.holiday._01,
        }, {
            key: '01',
            title: 'Capetown, Road Trips',
            text: 'Have a family vacation and enjoy long road trips',
            image: images.holiday._03,
        }, {
            key: '02',
            title: 'Johannesburg, Energetic Life',
            text: 'Be in the heart of Jozi and enjoy the fast life',
            image: images.joburg._01,
        }, {
            key: '03',
            title: 'Zanzibar, Tanzanian Archipelago',
            text: 'Meet and enjoy the company of Swahili people',
            image: images.others._01,
        }, {
            key: '04',
            title: 'WaterFront, Relaxing Life',
            text: 'Family-friendly attractions include the towering Cape Wheel ride.',
            image: images.others._02,
        }, {
            key: '05',
            title: 'Victoria Falls, Zimbabwe Safari',
            text: 'Get in close proximity with untamed wild-life',
            image: images.holiday._02,
        }
    ],
    skip_boarding: 'Yeqa',
    title_sign_in: 'Ngena ngemvume',
    title_sign_up: 'Bhalisela',
    title_forgot_password: 'Ukhohliwe Iphasiwedi',
    title_skip_registration: 'Yeqa Ukubhalisa',
    title_demo_login: 'Ukungena ngemvume',

    btn_dismiss: 'Chitha',
    btn_cancel: 'Khansela',
    btn_submit: 'Hambisa',
    btn_sign_in: 'Ngena ngemvume',
    btn_sign_up: 'Bhalisela',
    btn_forgot_password: 'Ukhohliwe Iphasiwedi?',

    lbl_or: 'noma',
    lbl_sign_in_method: 'Indlela yokungena ngemvume',
    lbl_membership_number: 'Inombolo Yobulungu',
    lbl_email_address: 'Ikheli le-imeyili',
    lbl_personal_information: 'Imininingwane yomuntu',
    lbl_first_name: 'Igama Lokuqala',
    lbl_surname: 'Isibongo',
    lbl_date_of_birth: 'Usuku Lokuzalwa',
    lbl_contact_information: 'Imininingwane Yokuxhumana',
    lbl_phone: 'Ucingo',
    lbl_security_information: 'Imininingwane Yezokuphepha',
    lbl_password: 'Iphasiwedi',
    lbl_password_confirm: 'Qinisekisa iphasiwedi',
    lbl_agree_tc: 'Ngiyavuma kuMango T&C',
    lbl_info_correct: 'Ngifaka yonke imininingwane ilungile',

    menu_home: 'Ikhaya',
    menu_flight_status: 'Hlola isimo sendiza',
    menu_aircraft_fleet: 'Hlola isimo sendiza',
    menu_suggestions: 'Izindiza eziphakanyisiwe',
    menu_mango_miles: 'Mango Amamayela',
    menu_buy_ticket: 'Thenga Amathikithi',
    menu_my_flights: 'Izindiza Zami',
    menu_mobile_checkin: 'Ukungena Kwamaselula',
    menu_book_hotel: 'Ihhotela Lebhuku',
    menu_rent_car: 'Qasha iMoto',
    menu_maps: 'I-Airport Amamephu',
    menu_settings: 'Izilungiselelo',
    menu_help: 'Usizo',
    menu_other: 'Okunye',
    menu_additional_services: 'Imisebenzi Engeziwe',
    menu_fly_with_mango: 'Ndiza noMango',

    lbl_guest: 'Isimenywa',
    lbl_greeting: "Sawubona {{user}} !!!",
    lbl_next_flight_destination: "Indiza yakho elandelayo iya ku - {{destination}}",
    lbl_next_flight_date: "Ngo - {{date}}",
    lbl_next_flight_boarding: "Ukugibela @ {{time}}",
    lbl_next_flight_departure: "Ukuhamba @ {{time}}",
    btn_mobile_check_in: 'Ngena Iselula',
    btn_search_flights: 'Ukucinga Izindiza',
    btn_search_now: 'Sesha Amathikithi Manje !!!',
    btn_read_more: 'Funda Okuningi !!!',
    string_buy_from_recent: "Thenga ithikithi lakho lokusesha kwendiza kwakamuva.",
    string_sign_in_register: "Ngena ngemvume noma Bhalisa i-akhawunti bese uqala ukuthenga amathikithi, thola amadili weMango anikezwa amalungu kuphela.",

    title_no_flights: 'Izindiza Azitholakali',
    message_no_flights: 'Azikho izindiza ezihleliwe ngezinsuku ezichaziwe.',
    btn_ok: 'Kulungile',
    title_missing_info: 'Ulwazi Olungekho',
    lbl_origin: 'Imvelaphi',
    lbl_destination: 'Ukuya',
    lbl_select: 'Khetha',
    string_select_origin: 'Khetha Umsuka',
    string_select_destination: 'Khetha Ukuya',
    lbl_return_date: 'Usuku Lokubuya',
    lbl_depart_date: 'Usuku Lokuhamba',
    lbl_cabin_class: 'I-Cabin Class',
    string_no_flights_on_date: 'Azikho izindiza ezihleliwe ezitholakele zadethi',
    string_try_different_date: "Zama Izinsuku Ezihlukile",

}
