import images from "../assets/images";

export default {
    app_name: 'Fly Mango',
    catch_phrase: 'Enjoy the schedule that suits you.',
    languages: [
        {label: 'Engels', value: 'en'},
        {label: 'Afrikaans', value: 'af'},
        {label: 'Zulu', value: 'zl'},
        {label: 'Swahili', value: 'sl'},
    ],
    special_flights: [
        {
            key: '00',
            title: 'Johannesburg',
            text: 'Pakket alles ingesluit, retoerreis',
            image: images.joburg._04,
            fee: 'R 800.00'
        }, {
            key: '01',
            title: 'Durban',
            text: 'Pakket alles ingesluit, retoerreis',
            image: images.durban._04,
            fee: 'R 700.00'
        }, {
            key: '02',
            title: 'Capetown',
            text: 'Pakket alles ingesluit, retoerreis',
            image: images.capetown._01,
            fee: 'R 1 000.00'
        },{
            key: '03',
            title: 'George',
            text: 'Pakket alles ingesluit, retoerreis',
            image: images.others._02,
            fee: 'R 1 500.00'
        }, {
            key: '04',
            title: 'Harare',
            text: 'Pakket alles ingesluit, retoerreis',
            image: images.others._03,
            fee: 'R 3 000.00'
        }, {
            key: '05',
            title: 'Zanzibar',
            text: 'Pakket alles ingesluit, retoerreis',
            image: images.others._01,
            fee: 'R 6 000.00'
        }
    ],
    boarding_slides: [
        {
            key: '00',
            title: 'Welcome aboard',
            text: 'Mango Airlines team welcomes you to our friendly mobile application.',
            image: images.planes._01,
        }, {
            key: '01',
            title: 'Mango on-board WiFi',
            text: 'Enjoy on-board wifi access, stay up-to-date with your business activity using reliable connection',
            image: images.planes._04,
        }, {
            key: '02',
            title: 'Fly Now, Pay Later',
            text: 'Flexible payments!!! Conveniently charge your flights to your Edgars, JET or CNA Thank U account card',
            image: images.travel._04,
        }, {
            key: '03',
            title: 'Special Discounts',
            text: 'Fly with Mango, accumulate Mango miles in the air and earn points to be eligible for special discounts and offers.',
            image: images.travel._05,
        }, {
            key: '04',
            title: 'Safe with Us',
            text: 'Your safety is important to us. Fly safely in the hands of our experienced and friendly team.',
            image: images.travel._06,
        }, {
            key: '05',
            title: 'Create an account',
            text: 'Register an account with Fly Mango and get personalised services.',
            image: images.travel._01,
        }
    ],
    skip_boarding: 'Huppel',
}
