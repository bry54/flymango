import images from "../assets/images";
import React from "react";

export default {
    app_name: 'Fly Mango',
    catch_phrase: 'Enjoy the schedule that suits you.',
    string_preparing_app: 'Preparing app for use, please wait. This can take a while depending on your connection.',
    language_label: 'Select Preferred Language',
    languages: [
        {label: 'English', value: 'en'},
        {label: 'Afrikaans', value: 'af'},
        {label: 'Zulu', value: 'zl'},
        {label: 'Swahili', value: 'sl'},
    ],
    region_label: 'Select Your Region',
    regions: [
        {label: 'Eastern Cape (RSA)', value: 'ECP'},
        {label: 'Western Cape (RSA)', value: 'WCP'},
        {label: 'Gauteng (RSA)', value: 'GP'},
        {label: 'KwaZulu Natal (RSA)', value: 'KZN'},
        {label: 'Zimbabwe', value: 'ZW'},
        {label: 'Tanzania', value: 'TZ'},
        {label: 'Namibia', value: 'NM'},
    ],
    currency_label: 'Select Display Currency',
    currencies: [
        {label: 'South African Rand (R)', value: 'ZAR'},
        {label: 'Zimbabwean Dollar (ZWD)', value: 'ZWD'},
        {label: 'United States Dollar (USD)', value: 'USD'},
        {label: 'Great British Pound (GBP)', value: 'GBP'},
        {label: 'Euro (EURO)', value: 'EURO'},
    ],
    special_flights: [
        {
            key: '00',
            title: 'Johannesburg',
            text: 'Pakket alles ingesluit, retoerreis',
            image: images.joburg._04,
            fee: 'R 800.00'
        }, {
            key: '01',
            title: 'Durban',
            text: 'Pakket alles ingesluit, retoerreis',
            image: images.durban._04,
            fee: 'R 700.00'
        }, {
            key: '02',
            title: 'Capetown',
            text: 'Pakket alles ingesluit, retoerreis',
            image: images.capetown._01,
            fee: 'R 1 000.00'
        },{
            key: '03',
            title: 'George',
            text: 'Pakket alles ingesluit, retoerreis',
            image: images.others._02,
            fee: 'R 1 500.00'
        }, {
            key: '04',
            title: 'Harare',
            text: 'Pakket alles ingesluit, retoerreis',
            image: images.others._03,
            fee: 'R 3 000.00'
        }, {
            key: '05',
            title: 'Zanzibar',
            text: 'Pakket alles ingesluit, retoerreis',
            image: images.others._01,
            fee: 'R 6 000.00'
        }
    ],
    lbl_starting_at: 'Starting @ {{price}}',
    boarding_slides: [
        {
            key: '00',
            title: 'Welcome aboard',
            text: 'Mango Airlines team welcomes you to our friendly mobile application.',
            image: images.planes._01,
        }, {
            key: '01',
            title: 'Mango on-board WiFi',
            text: 'Enjoy on-board wifi access, stay up-to-date with your business activity using reliable connection',
            image: images.planes._04,
        }, {
            key: '02',
            title: 'Fly Now, Pay Later',
            text: 'Flexible payments!!! Conveniently charge your flights to your Edgars, JET or CNA Thank U account card',
            image: images.travel._04,
        }, {
            key: '03',
            title: 'Special Discounts',
            text: 'Fly with Mango, accumulate Mango miles in the air and earn points to be eligible for special discounts and offers.',
            image: images.travel._05,
        }, {
            key: '04',
            title: 'Safe with Us',
            text: 'Your safety is important to us. Fly safely in the hands of our experienced and friendly team.',
            image: images.travel._06,
        }, {
            key: '05',
            title: 'Create an account',
            text: 'Register an account with Fly Mango and get personalised services.',
            image: images.travel._01,
        }
    ],
    showcase_slides:[
        {
            key: '00',
            title: 'Limpopo, Game park Visits',
            text: 'Enjoy and experience amazing wildlife',
            image: images.holiday._01,
        }, {
            key: '01',
            title: 'Capetown, Road Trips',
            text: 'Have a family vacation and enjoy long road trips',
            image: images.holiday._03,
        }, {
            key: '02',
            title: 'Johannesburg, Energetic Life',
            text: 'Be in the heart of Jozi and enjoy the fast life',
            image: images.joburg._01,
        }, {
            key: '03',
            title: 'Zanzibar, Tanzanian Archipelago',
            text: 'Meet and enjoy the company of Swahili people',
            image: images.others._01,
        }, {
            key: '04',
            title: 'WaterFront, Relaxing Life',
            text: 'Family-friendly attractions include the towering Cape Wheel ride.',
            image: images.others._02,
        }, {
            key: '05',
            title: 'Victoria Falls, Zimbabwe Safari',
            text: 'Get in close proximity with untamed wild-life',
            image: images.holiday._02,
        }
    ],
    skip_boarding: 'Skip',
    title_sign_in: 'Sign In',
    title_sign_up: 'Sign Up',
    title_forgot_password: 'Forgot Password',
    title_skip_registration: 'Skip Registration',
    title_demo_login: 'Demo Login',

    btn_dismiss: 'Dismiss',
    btn_cancel: 'Cancel',
    btn_submit: 'Submit',
    btn_sign_in: 'Sign In',
    btn_sign_up: 'Sign Up',
    btn_forgot_password: 'Forgot Password ?',

    lbl_or: 'or',
    lbl_sign_in_method: 'Sign In Method',
    lbl_membership_number: 'Membership Number',
    lbl_email_address: 'Email Address',
    lbl_personal_information: 'Personal Information',
    lbl_first_name: 'First Name',
    lbl_surname: 'Surname',
    lbl_date_of_birth: 'Date Of Birth',
    lbl_contact_information: 'Contact Information',
    lbl_phone: 'Phone',
    lbl_security_information: 'Security Information',
    lbl_password: 'Password',
    lbl_password_confirm: 'Password Confirm',
    lbl_agree_tc: 'I agree to Mango T&C',
    lbl_info_correct: 'I conform all the information is correct',

    menu_home: 'Home',
    menu_flight_status: 'Check Flight Status',
    menu_aircraft_fleet: 'Mango Aircraft Fleet',
    menu_suggestions: 'Suggested Flights',
    menu_mango_miles: 'Mango Miles',
    menu_buy_ticket: 'Buy Tickets',
    menu_my_flights: 'My Flights',
    menu_mobile_checkin: 'Mobile Check-In',
    menu_book_hotel: 'Book Hotel',
    menu_rent_car: 'rent A Car',
    menu_maps: 'Airport Maps',
    menu_settings: 'Settings',
    menu_help: 'Help',
    menu_other: 'Other',
    menu_additional_services: 'Additional Services',
    menu_fly_with_mango: 'Fly With Mango',

    lbl_guest: 'Guest',
    lbl_greeting: "Hello {{user}} !!!",
    lbl_next_flight_destination: "Your next flight to {{destination}}",
    lbl_next_flight_date: "On {{date}}",
    lbl_next_flight_boarding: "Boarding @ {{time}}",
    lbl_next_flight_departure: "Departure @ {{time}}",
    btn_mobile_check_in: 'Mobile Check In',
    btn_search_flights: 'Search Flights',
    btn_search_now: 'Search Tickets Now!!!',
    btn_read_more: 'Read More!!!',
    string_buy_from_recent: "Buy a ticket for you recent flight searches.",
    string_sign_in_register: "Sign In or Register an account and start buying tickets, receive Mango deals exclusive to members only.",

    title_no_flights: 'Flights Not Found',
    message_no_flights: 'There are no flights scheduled on the specified dates.',
    btn_ok: 'Okay',
    title_missing_info: 'Missing Information',
    lbl_origin: 'Origin',
    lbl_destination: 'Destination',
    lbl_select: 'Select',
    string_select_origin: 'Select Origin',
    string_select_destination: 'Select Destination',
    lbl_return_date: 'Return Date',
    lbl_depart_date: 'Departure Date',
    lbl_cabin_class: 'Cabin Class',
    string_no_flights_on_date: 'No scheduled flights found for the specified date',
    string_try_different_date: "Try Different Dates",
}
