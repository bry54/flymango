import images from "../assets/images";

export default {
    app_name: 'Vlieg Mango',
    catch_phrase: 'Geniet die skedule wat u pas.',
    string_preparing_app: 'Preparing app for use, please wait. This can take a while depending on your connection.',
    language_label: 'Kies Voorkeurtaal',
    region_label: 'Select Your Region',
    regions: [
        {label: 'Eastern Cape (RSA)', value: 'ECP'},
        {label: 'Western Cape (RSA)', value: 'WCP'},
        {label: 'Gauteng (RSA)', value: 'GP'},
        {label: 'KwaZulu Natal (RSA)', value: 'KZN'},
        {label: 'Zimbabwe', value: 'ZW'},
        {label: 'Tanzania', value: 'TZ'},
        {label: 'Namibia', value: 'NM'},
    ],
    currency_label: 'Select Display Currency',
    currencies: [
        {label: 'South African Rand (R)', value: 'ZAR'},
        {label: 'Zimbabwean Dollar (ZWD)', value: 'ZWD'},
        {label: 'United States Dollar (USD)', value: 'USD'},
        {label: 'Great British Pound (GBP)', value: 'GBP'},
        {label: 'Euro (EURO)', value: 'EURO'},
    ],
    special_flights: [
        {
            key: '00',
            title: 'Johannesburg',
            text: 'Pakket alles ingesluit, retoerreis',
            image: images.joburg._04,
            fee: 'R 800.00'
        }, {
            key: '01',
            title: 'Durban',
            text: 'Pakket alles ingesluit, retoerreis',
            image: images.durban._04,
            fee: 'R 700.00'
        }, {
            key: '02',
            title: 'Kaapstad',
            text: 'Pakket alles ingesluit, retoerreis',
            image: images.capetown._01,
            fee: 'R 1 000.00'
        },{
            key: '03',
            title: 'George',
            text: 'Pakket alles ingesluit, retoerreis',
            image: images.others._02,
            fee: 'R 1 500.00'
        }, {
            key: '04',
            title: 'Harare',
            text: 'Pakket alles ingesluit, retoerreis',
            image: images.others._03,
            fee: 'R 3 000.00'
        }, {
            key: '05',
            title: 'Zanzibar',
            text: 'Pakket alles ingesluit, retoerreis',
            image: images.others._01,
            fee: 'R 6 000.00'
        }
    ],
    lbl_starting_at: 'Begin @ {{price}}',
    boarding_slides: [
        {
            key: '00',
            title: 'Welkom aan boord',
            text: 'Mango Airlines-span verwelkom u by ons vriendelike mobiele toepassing.',
            image: images.planes._01,
        }, {
            key: '02',
            title: 'Vlieg nou, betaal later',
            text: 'Buigsame betalings !!! Laai u vlugte gerieflik op u Edgars-, JET- of CNA Thank U-rekeningkaart',
            image: images.travel._04,
        }, {
            key: '03',
            title: 'Spesiale afslag',
            text: 'Vlieg met Mango, versamel Mango-kilometers in die lug en verdien punte om in aanmerking te kom vir spesiale afslag en aanbiedings.',
            image: images.travel._05,
        }, {
            key: '04',
            title: 'Veilig by ons',
            text: 'U veiligheid is vir ons belangrik. Vlieg veilig in die hande van ons ervare en vriendelike span.',
            image: images.travel._06,
        }, {
            key: '05',
            title: 'Maak \'n rekening oop',
            text: 'Registreer \'n rekening by Fly Mango en kry persoonlike dienste.',
            image: images.travel._01,
        }
    ],
    showcase_slides:[
        {
            key: '00',
            title: 'Limpopo, Game park Visits',
            text: 'Enjoy and experience amazing wildlife',
            image: images.holiday._01,
        }, {
            key: '01',
            title: 'Capetown, Road Trips',
            text: 'Have a family vacation and enjoy long road trips',
            image: images.holiday._03,
        }, {
            key: '02',
            title: 'Johannesburg, Energetic Life',
            text: 'Be in the heart of Jozi and enjoy the fast life',
            image: images.joburg._01,
        }, {
            key: '03',
            title: 'Zanzibar, Tanzanian Archipelago',
            text: 'Meet and enjoy the company of Swahili people',
            image: images.others._01,
        }, {
            key: '04',
            title: 'WaterFront, Relaxing Life',
            text: 'Family-friendly attractions include the towering Cape Wheel ride.',
            image: images.others._02,
        }, {
            key: '05',
            title: 'Victoria Falls, Zimbabwe Safari',
            text: 'Get in close proximity with untamed wild-life',
            image: images.holiday._02,
        }
    ],
    skip_boarding: 'Huppel',
    title_sign_in: 'Meld aan',
    title_sign_up: 'Aanmelden',
    title_forgot_password: 'Wagwoord vergeet',
    title_skip_registration: 'Slaan registrasie oor',
    title_demo_login: 'Demo Aanteken',

    btn_dismiss: 'Dismiss',
    btn_cancel: 'Kanselleer',
    btn_submit: 'Dien\' in',
    btn_sign_in: 'Meld aan',
    btn_sign_up: 'Meld u aan',
    btn_forgot_password: 'Wagwoord vergeet?',

    lbl_of: 'of',
    lbl_sign_in_method: 'Aanmeldmetode',
    lbl_membership_number: 'Membership Number',
    lbl_email_address: 'e-posadres',
    lbl_personal_information: 'Persoonlike inligting',
    lbl_first_name: 'Voornaam',
    lbl_surname: 'Van',
    lbl_date_of_birth: 'Geboortedatum',
    lbl_contact_information: 'Kontakinligting',
    lbl_phone: 'Telefoon',
    lbl_security_information: 'Veiligheidsinligting',
    lbl_password: 'Wagwoord',
    lbl_password_confirm: 'Bevestig wagwoord',
    lbl_agree_tc: 'Ek stem saam met Mango T&C',
    lbl_info_correct: 'Ek stem ooreen dat al die inligting korrek is',

    menu_home: 'Huis',
    menu_flight_status: 'Gaan vlugstatus na',
    menu_aircraft_fleet: 'Mango-vliegtuigvloot',
    menu_suggestions: 'Voorgestelde vlugte',
    menu_mango_miles: 'Mango Miles',
    menu_buy_ticket: 'Koop Kaartjies',
    menu_my_flights: 'My Vlug',
    menu_mobile_checkin: 'Mobiele inklok',
    menu_book_hotel: 'Bespreek Hotel',
    menu_rent_car: 'Huur \'n motor',
    menu_maps: 'Lughawekaarte',
    menu_settings: 'Instellings',
    menu_help: 'Hulp',
    menu_other: 'Ander',
    menu_additional_services: 'Bykomende dienste',
    menu_fly_with_mango: 'Vlieg met mango',

    lbl_guest: 'g]Gast',
    lbl_greeting: "Hallo {{user}} !!!",
    lbl_next_flight_destination: "Jou volgende vlug na {{destination}}",
    lbl_next_flight_date: "Op {{date}}",
    lbl_next_flight_boarding: "Instap @ {{time}}",
    lbl_next_flight_departure: "Vertrek @ {{time}}",
    btn_mobile_check_in: 'Mobiele inklok',
    btn_search_flights: 'Soek vlugte',
    btn_search_now: 'Soek kaartjies nou !!!',
    btn_read_more: 'Lees meer !!!',
    string_buy_from_recent: "Koop 'n kaartjie vir u onlangse vlugsoektogte.",
    string_sign_in_register: "Meld aan of registreer 'n rekening en begin kaartjies koop, ontvang slegs Mango-aanbiedings eksklusief vir lede.",

    title_no_flights: 'Vlugte nie gevind nie',
    message_no_flights: 'Daar is geen vlugte geskeduleer op die gespesifiseerde datums nie.',
    btn_ok: 'Goed',
    title_missing_info: 'Ontbrekende inligting',
    lbl_origin: 'Oorsprong',
    lbl_destination: 'Bestemming',
    lbl_select: 'Kies',
    string_select_origin: 'Kies oorsprong',
    string_select_destination: 'Kies bestemming',
    lbl_return_date: 'Datum van terugkeer',
    lbl_depart_date: 'Vertrekdatum',
    lbl_cabin_class: 'Cabin Class',
    string_no_flights_on_date: 'Geen geskeduleerde vlugte gevind vir die gespesifiseerde datum nie',
    string_try_different_date: "Probeer verskillende datums",
}
