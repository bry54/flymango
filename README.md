# Fly Mango

Fly Mango <dev> is a prototype mobile app to serve as a project proposal for Mango Airlines.

## Features!

  - Mango Miles
  - See Flight History (Registered users Only)
  - Buy Tickets (Registered Users Only)
  - Mobile Check In
  - Check Flight Status
  - Book Hotels
  - Rent Cars
  - See Supported Airports
  - Settings
  - Help

### Demo Inputs
    users
    Buy Tickets
    Flight Status 

## Installation
To run the demo application you will need to install expo client on your mobile device:

- Visit this URL [Expo](https://nodejs.org/) to install Expo.
- Visit this URL [Fly Mango](https://nodejs.org/) to preview Fly Mango.

### Todos

This is only a demo application, features that require deep technical knowledge about airline reservation is not included.
- Strict Data binding
- Backend implementation

Copyright
----

**This prototype is is protected by copyright and has a non-disclosure agreement**

[//]: # (These are reference links used in the body of this note and get stripped out when the markdown processor does its job. There is no need to format nicely because it shouldn't be seen. Thanks SO - http://stackoverflow.com/questions/4823468/store-comments-in-markdown-syntax)

   [node.js]: <http://nodejs.org>
